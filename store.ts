import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer as preloaderReducer } from 'layout/preloader';
import { reducer as appScrollableReducer } from 'layout';
import { reducer as headerReducer } from '_pages/home/elements/header';
import { reducer as mobileMenuReducer } from 'layout/header/menu/mobile';
import thunk from 'redux-thunk';

const combinedReducers = combineReducers({
  preloader: preloaderReducer,
  appScrollable: appScrollableReducer,
  header: headerReducer,
  mobileMenu: mobileMenuReducer,
});

export type AppState = ReturnType<typeof combinedReducers>;

export function initializeStore() {
  return createStore(
    combinedReducers,
    composeWithDevTools(applyMiddleware(thunk)),
  );
}

export default {};
