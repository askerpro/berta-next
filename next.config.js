const webpack = require('webpack');
const withCSS = require('@zeit/next-css');
const withImages = require('next-images');
const withFonts = require('next-fonts');
module.exports = withFonts(
  withCSS(
    withImages({
      pageExtensions: ['page.tsx', 'tsx', 'ts', 'js', 'jsx'],
      cssLoaderOptions: {
        url: true,
      },
      experimental: { css: true },
      webpack(config) {
        config.node = { fs: 'empty' };
        config.devtool = 'cheap-module-source-map';
        config.plugins = config.plugins || [];
        config.plugins.push(
          new webpack.DefinePlugin({
            'process.env': {
              PRELOADER: JSON.stringify(true),
            },
          }),
        );
        return config;
      },
    }),
  ),
);
