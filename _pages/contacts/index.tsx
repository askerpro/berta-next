import React from 'react';
import { Section } from 'layout/elements';
import Page from 'lib/Page';

import { Typography, Grid, Button } from '@material-ui/core';
import data from 'data/contacts';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { ExpandMore, Phone, Place } from '@material-ui/icons';
import Hidden from '@material-ui/core/Hidden';
import * as E from './elements';

const title = 'Контакты';
const desc = 'Адреса и телефоны офисов продаж.';

export default () => (
  <Page
    head={{
      title,
      description: desc,
    }}
  >
    <>
      <Section.Wrapper>
        <Section.Container>
          <Section.HeaderT title={title} subtitle={desc} h1 />
          <Grid container spacing={5}>
            {data.map(region => (
              <Grid item container spacing={2} xs={12} key={region.name}>
                <Grid item xs={12}>
                  <Typography variant="h4">{region.name}</Typography>
                </Grid>
                <Grid item xs={12}>
                  {region.offices.map(office => (
                    <ExpansionPanel key={office.address}>
                      <E.ExpansionPanelSummary expandIcon={<ExpandMore />}>
                        <E.OfficeAddress>
                          {office.city}
                          {'       -       '}
                          {office.address}
                        </E.OfficeAddress>
                        <Hidden mdDown>
                          <Typography>{office.phone}</Typography>
                        </Hidden>
                      </E.ExpansionPanelSummary>
                      <ExpansionPanelDetails>
                        <Hidden smUp>
                          <Grid container>
                            <Grid item xs={12}>
                              <Typography>
                                <Button href={`tel:${office.phone}`}>
                                  <Phone />
                                  {office.phone}
                                </Button>
                              </Typography>
                              <Grid item xs={12}>
                                <Button href={office.src}>
                                  <Place />
                                  Открыть карту
                                </Button>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Hidden>
                        <Hidden mdDown>
                          <Button href={office.src}>
                            <Place />
                            Открыть карту
                          </Button>
                        </Hidden>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  ))}
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Section.Container>
      </Section.Wrapper>
    </>
  </Page>
);
