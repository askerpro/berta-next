import React from 'react';
import Slider from 'react-slick';
import { Section } from 'layout/elements';
import Promo1 from '_pages/production/profiles/silver-eco-premium/promos/promo1';

const slides = [Promo1, Promo1];

export default () => (
  <Section.Wrapper>
    <Section.Container>
      <Slider infinite arrows dots autoplay={false} adaptiveHeight>
        {slides.map((Slide, i) => (
          <Slide key={i} />
        ))}
      </Slider>
    </Section.Container>
  </Section.Wrapper>
);
