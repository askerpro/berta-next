import styled, { css } from 'layout/theme';
import Grid, { GridProps } from '@material-ui/core/Grid';
import React from 'react';

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  padding: 80px 12px;
`;
export const Inner = styled.div`
  display: grid;
  position: relative;
  grid-gap: 2px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  box-sizing: border-box;
  width: 100%;
  max-width: 1600px;
  margin: 0 auto;
  @media (max-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    padding: 0px;
  }
`;
interface IPromoCardsWrapper extends GridProps {
  visible: boolean;
}
export const PromoCardsWrapper = styled(
  ({ visible, ...other }: IPromoCardsWrapper) => <Grid {...other} />,
)<IPromoCardsWrapper>`
  ${({ visible }) =>
    visible &&
    css`
      ${PromoCardWrapper} {
        opacity: 1;
        transform: translateY(0px);
      }
    `}
`;

interface IPromoCardWrapper {
  index: number;
}
export const PromoCardWrapper = styled(Grid)<IPromoCardWrapper>`
  width: 100%;
  opacity: 0;
  transform: translateY(50px);

  transition-duration: 0.5s;
  transition-delay: ${({ index }) => index * 0.15}s;
`;
