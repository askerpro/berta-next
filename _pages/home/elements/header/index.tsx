import { connect } from 'react-redux';
import { AppState } from 'store';
import Header, { HeaderState } from 'layout/header';

export enum Actions {
  OPEN = 'OPEN_HEADER',
  CLOSE = 'CLOSE_HEADER',
}

export const OpenHeader = {
  type: Actions.OPEN,
};

export const CloseHeader = {
  type: Actions.CLOSE,
};

type ActionType = typeof OpenHeader | typeof CloseHeader;

export const initialState: HeaderState = HeaderState.OPENED;

export const reducer = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case Actions.OPEN:
      return HeaderState.OPENED;

    case Actions.CLOSE:
      return HeaderState.CLOSED;

    default:
      return state;
  }
};

const mapStateToProps = (state: AppState) => {
  return {
    state: state.header,
  };
};

export default connect(
  mapStateToProps,
  {},
)(Header);
