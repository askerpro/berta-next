import React from 'react';
import Page from 'lib/Page';
import PriceSection, { ProductType } from 'components/PriceSection';
import ReviewsSection from 'components/ReviewsSection';
import PromoCardsSection from 'layout/promo-cards-section';
import SilverEcoPromo1 from '_pages/production/profiles/silver-eco-premium/promos/promo1';
import Calc from 'components/Calc';
import { Typography } from '@material-ui/core';
import { Section } from 'layout/elements';

const pageTitle = 'Главная';
const desc = 'Добро пожаловать на официальный сайт.';

export default () => (
  <>
    <Page
      head={{
        title: pageTitle,
        description: desc,
      }}
    >
      <>
        <SilverEcoPromo1 />
        {/* <PriceSection
          productType={ProductType.PROMO}
          title="Пластиковые окна"
          subtitle="Рассчет стоимости. Калькулятор пластиковых окон онлайн."
          calcButton
        /> */}
        <Section.Wrapper>
          <Section.Container>
            <Section.HeaderT title="Расчет стоимости" subtitle="" center />
            <Section.Body>
              <Calc />
            </Section.Body>
          </Section.Container>
        </Section.Wrapper>
        <ReviewsSection title="Отзывы" />
        <PromoCardsSection />
      </>
    </Page>
  </>
);
