import React from 'react';
import { Section, Button, PromoCard } from 'layout/elements';
import Page from 'lib/Page';
import { withRouter, SingletonRouter } from 'next/router';
import SendToOperator from 'lib/send-to-opearotr';
import * as E from 'layout/elements/SectionCard/elements';
import { Grey } from 'layout/theme';
import {
  Typography,
  Grid,
  Select,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Container,
} from '@material-ui/core';

import orderImage from './img/bg.jpg';

export enum Service {
  BUY_PRODUCT,
  REPAIR_OR_SETTING,
  EQUIPMENTS,
  CALL_WORKER,
}

interface IService {
  title: string;
  img: string;
}

const services: IService[] = [
  {
    title: 'Купить окно или дверь',
    img: '/static/img/order/4.jpg',
  },
  {
    title: 'Ремонт окон или настройка',
    img: '/static/img/order/schuco-2.jpg',
  },
  {
    title: 'Установка комплектующих',
    img: '/static/img/order/equipments.jpg',
  },
  {
    title: 'Вызвать мастера на замер',
    img: '/static/img/order/measure.jpg',
  },
];

const title = 'Оформить заказ';
const desc = 'Выберите услугу и заполните все поля';
const url = '/services/repair';
const caption = 'Оформление заказа';
const actionText = 'Принять заказ';

interface State {
  selectedService: number | '';
  userName: string;
  userPhone: string;
  stage: number;
}

interface Props {
  router: SingletonRouter;
}
class Order extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      stage: 0,
      selectedService: '',
      userName: '',
      userPhone: '',
    };
  }

  componentDidMount() {
    const { router } = this.props;
    const { serviceId } = router.query;
    if (serviceId) {
      this.setState({
        selectedService: parseInt(serviceId.toString(), 10),
      });
    }
  }

  isAllInputsValid = () => {
    const { userName, userPhone, selectedService } = this.state;
    return userName !== '' && userPhone !== '' && selectedService !== '';
  };

  submit = () => {
    if (this.isAllInputsValid()) {
      const subject = 'Заказ с сайта';
      const { selectedService, userName, userPhone } = this.state;
      const msg = `
        <p> Тип: <strong>${services[selectedService].title}</strong></p>\r\n
				<p> Имя: <strong>${userName}</strong></p>\r\n
        <p> Телефон: <strong>${userPhone}</strong></p>\r\n
      `;
      SendToOperator(subject, msg, result => {
        if (result.error) {
          alert('Ошибка при отправке запроса');
        } else {
          this.setState({
            stage: 1,
          });
        }
      });
    }
  };

  render() {
    const { selectedService, userName, userPhone, stage } = this.state;

    return (
      <Page
        head={{
          title,
          description: desc,
        }}
      >
        <Grey>
          <>
            <Section.Wrapper fullHeight>
              <Section.Container>
                <E.Card vertical>
                  <>
                    <E.CardMedia
                      image={
                        selectedService === ''
                          ? orderImage
                          : services[selectedService].img
                      }
                      title={title}
                    />

                    <E.CardContent>
                      {stage === 0 && (
                        <Grid container spacing={3}>
                          <Grid item xs={12}>
                            {caption && (
                              <Typography
                                gutterBottom
                                variant="overline"
                                component="p"
                                color="textSecondary"
                              >
                                {caption}
                              </Typography>
                            )}
                            <Typography
                              gutterBottom
                              variant="h3"
                              component="h1"
                            >
                              {title}
                            </Typography>
                            <Typography
                              variant="body1"
                              color="textSecondary"
                              component="p"
                            >
                              {desc}
                            </Typography>
                          </Grid>
                          <Grid item container spacing={1} xs={12}>
                            <Grid item xs={12}>
                              <FormControl fullWidth>
                                <InputLabel>Тип услуги: </InputLabel>
                                <Select
                                  value={selectedService}
                                  onChange={event => {
                                    this.setState({
                                      selectedService: event.target
                                        .value as number,
                                    });
                                  }}
                                >
                                  {services.map((service, i) => (
                                    <MenuItem value={i} key={i}>
                                      {service.title}
                                    </MenuItem>
                                  ))}
                                </Select>
                              </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                              <TextField
                                required
                                label="Ваше имя"
                                value={userName}
                                onChange={event => {
                                  this.setState({
                                    userName: event.target.value,
                                  });
                                }}
                                fullWidth
                              />
                            </Grid>
                            <Grid item xs={12}>
                              <TextField
                                required
                                label="Ваш номер"
                                value={userPhone}
                                onChange={event => {
                                  this.setState({
                                    userPhone: event.target.value,
                                  });
                                }}
                                fullWidth
                              />
                            </Grid>
                          </Grid>
                          <Grid item xs={12}>
                            <Button
                              variant="outlined"
                              size="large"
                              fullWidth
                              onClick={this.submit}
                            >
                              {actionText}
                            </Button>
                          </Grid>
                        </Grid>
                      )}
                      {stage === 1 && (
                        <Container maxWidth="sm">
                          <Grid container spacing={5}>
                            <Grid item>
                              <Typography variant="h4">
                                Заказ принят. Ждите звонка оператора.
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Button
                                color="primary"
                                variant="contained"
                                fullWidth
                                onClick={() => {
                                  this.setState({
                                    stage: 0,
                                  });
                                }}
                              >
                                Сделать еще 1 заказ
                              </Button>
                            </Grid>
                          </Grid>
                        </Container>
                      )}
                    </E.CardContent>
                  </>
                </E.Card>
              </Section.Container>
            </Section.Wrapper>
          </>
        </Grey>
      </Page>
    );
  }
}

export const Promo = () => (
  <PromoCard title={title} desc={desc} href={url} image={orderImage} contain />
);

export default withRouter<Props>(Order);
