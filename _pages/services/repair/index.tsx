import React from 'react';
import { Section, Button, PromoCard } from 'layout/elements';
import Page from 'lib/Page';
import { Typography, CardContent, CardActions, Grid } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import * as E from './elements';
import serviceList from './data';
import promoImg from './img/bg.jpg';

const title = 'Ремонт окон';
const desc = 'Любых окон от любого производителя.';
const url = '/services/repair';
export default () => (
  <Page
    head={{
      title,
      description: desc,
    }}
  >
    <>
      <Section.Wrapper>
        <Section.Container>
          <Section.HeaderT title={title} subtitle={desc} h1 />
          <Grid container spacing={5}>
            {serviceList.map((service, i) => (
              <Grid item key={i} xs={12}>
                <E.Card>
                  <E.CardMedia image={service.img} title={service.title} />
                  <CardContent>
                    <Typography variant="h4">{service.title}</Typography>
                    {service.advantages.map(advantage => (
                      <ListItem disableGutters>
                        <E.AdvantageIcon />
                        <Typography>{advantage}</Typography>
                      </ListItem>
                    ))}
                    <CardActions disableSpacing>
                      <Button
                        variant="contained"
                        href={`/services/order?serviceId=${service.service}`}
                      >
                        {service.actionText}
                      </Button>
                    </CardActions>
                  </CardContent>
                </E.Card>
              </Grid>
            ))}
          </Grid>
        </Section.Container>
      </Section.Wrapper>
    </>
  </Page>
);

export const Promo = () => (
  <PromoCard title={title} desc={desc} href={url} image={promoImg} />
);
