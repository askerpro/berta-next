import { Service } from '_pages/services/order';

export default [
  {
    title: 'Настройка окон',
    advantages: [
      'Окно продувает, в ощущается сквозняк',
      'Нижняя или средняя часть створки провисает и задевает раму',
      'Проблемы с фурнитурой? Болтается ручка?',
      'Ручка не поворачивается или поворачивается с трудом',
    ],
    actionText: 'Заказать настройку окон',
    img: '/static/img/repair-service/setting-up.jpg',
    service: Service.REPAIR_OR_SETTING,
  },
  {
    title: 'Дополнительное открывание',
    advantages: [
      'Нужна дополнительная открывающаяся створка',
      'Неудобно мыть окно',
      'Трудно вешать белье',
      'Установим без лишнего шума и пыли доп. створку за 1 час.',
    ],
    actionText: 'Заказать дополнительное открывание',
    img: '/static/img/repair-service/extra-opening.jpg',
    service: Service.REPAIR_OR_SETTING,
  },
  {
    title: 'Замена стеклопакета',
    advantages: [
      'Поломался стеклопакет',
      'Хотите установить утепленный стеклопакет',
      'Хотите установить теплоотражающий стеклопакет',
      'Хотите установить цветнойтонированный стеклопакет',
    ],
    actionText: 'Заказать замену стеклопакета',
    img: '/static/img/repair-service/glass-repair.jpg',
    service: Service.REPAIR_OR_SETTING,
  },
  {
    title: 'Москитная сетка',
    advantages: [
      'Защита от комаров и насекомых',
      'Защищает от пыли',
      'Позволяет открывать окно в дождь',
      'Обеспечивает безопасность детей и домашних животных',
    ],
    actionText: 'Заказать москитная сетку',
    img: '/static/img/repair-service/anti-moskit.jpg',
    service: Service.EQUIPMENTS,
  },
  {
    title: 'Подоконники / Отливы',
    advantages: [
      'Придает эстетичный и законченный вид',
      'Отлив защищает окно от влаги снаружи',
      'Подоконник изолирует от холодного воздуха зимой',
      'Решает проблему возникновения конденсата',
    ],
    actionText: 'Заказать подоконники / Отливы',
    img: '/static/img/repair-service/podokonnik.jpg',
    service: Service.EQUIPMENTS,
  },
  {
    title: 'Ручки, доводчики',
    advantages: [
      'Ваша оконная ручка сломалась',
      'Не нравится внешний вид ручки',
      'Нужны дополнительные функции (защита от взлома)',
      'Нужна ручка акустик',
    ],
    actionText: 'Заказать ручки, доводчики',
    img: '/static/img/repair-service/handles.jpg',
    service: Service.EQUIPMENTS,
  },
];
