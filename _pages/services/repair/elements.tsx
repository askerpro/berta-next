import styled from 'layout/theme';

import {
  // Typography,
  CardMedia as CardMediaT,
  // CardContent,
  // CardActions,
  Card as CardT,
} from '@material-ui/core';

import Done from '@material-ui/icons/Done';

export const AdvantageIcon = styled(Done)`
  margin-right: 7px;
  vertical-align: middle;
`;
export const CardMedia = styled(CardMediaT)`
  height: 300px;
  max-width: 318px;
  width: 100%;
  background-size: contain;
`;
export const Card = styled(CardT)`
  @media (min-width: 768px) {
    display: flex;
    padding: ${({ theme }) => theme.spacing(4)}px;
    align-items: center;
    max-width: 830px;
  }
`;

export default {};
