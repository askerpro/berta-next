import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Dark } from 'layout/theme';
import SectionImage from './img/header.jpg';

const title = 'Остекление коттеджей';
const subtitle = 'Окна, двери, пристройки';
const shortTitle = 'Входные';
const id = 'Остекление коттеджей';
const actionText = 'Заказать остекление коттеджа';
const desc =
  'Мы обладаем достаточным опытом и возможностями, чтобы остеклить коттеджи любой сложности: от простых оконных конструкций до зимних садов нестандартной формы.';

const View = () => (
  <Dark>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={subtitle}
            vertical
          />
        </Section.Container>
      </Section.Wrapper>
    </>
  </Dark>
);

export default {
  id,
  title: shortTitle,
  view: View,
};
