import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { White } from 'layout/theme';
import SectionImage from './img/windows.jpg';

const title = 'Пластиковые окна';
const subtitle = 'Остекление коттеджей';
const shortTitle = 'Окна';
const id = 'windows';
const actionText = 'Заказать остекление коттеджа';
const desc =
  'Оптимальные климат внутри дома – непростая задача. Помимо отопления, кондиционирования, материалов для отделки в этом участвует и остекление. Мы подберем решения, которые удержат тепло, будут препятствовать перенагреву помещения или создадут уют, закрыв окна от яркого солнца.';

const View = () => (
  <White>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={subtitle}
            vertical
            reversed
          />
        </Section.Container>
      </Section.Wrapper>
    </>
  </White>
);

export default {
  id,
  title: shortTitle,
  view: View,
};
