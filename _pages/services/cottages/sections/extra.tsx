import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Dark } from 'layout/theme';
import SectionImage from './img/extensions.jpg';

const title = 'Остекление пристроек';
const subtitle = 'Теплое и холодное';
const shortTitle = 'Пристройки';
const id = 'extensions';
const actionText = 'Заказать остекление пристроек';
const desc =
  'Поможем, как с простыми конструкциями, так и со сложными, например остеклим помещения с повышенной влажностью или сезонного назначения';

const View = () => (
  <Dark>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={subtitle}
            vertical
          />
        </Section.Container>
      </Section.Wrapper>
    </>
  </Dark>
);

export default {
  id,
  title: shortTitle,
  view: View,
};
