import React from 'react';
import { Section, PromoCard, SectionCard } from 'layout/elements';
import Page, { IHead } from 'lib/Page';
import DoorsSection from '_pages/production/doors/sections/outside';
import DesignedSection from '_pages/production/designed/sections/figured';
import ExtraSection from './sections/extra';
import WindowsSection from './sections/windows';
import SectionImage from './sections/img/header.jpg';

const url = '/services/cottages';
const title = 'Остекление коттеджей';
const subtitle = 'Окна, двери, пристройки';
const shortTitle = title;
const id = 'header';
const actionText = 'Заказать остекление коттеджа';
const desc =
  'Мы обладаем достаточным опытом и возможностями, чтобы остеклить коттеджи любой сложности: от простых оконных конструкций до зимних садов нестандартной формы.';

const view = () => (
  <>
    <Section.Wrapper id={id}>
      <Section.Container>
        <SectionCard
          title={title}
          desc={desc}
          image={SectionImage}
          actionText={actionText}
          caption={subtitle}
          vertical
          reversed
        />
      </Section.Container>
    </Section.Wrapper>
  </>
);

const HeaderSection = {
  title: shortTitle,
  id,
  view,
};

const head: IHead = {
  title,
  description: subtitle,
};

export default () => (
  <Page
    sections={[
      HeaderSection,

      DoorsSection,
      WindowsSection,
      DesignedSection,
      ExtraSection,
    ]}
    head={head}
  />
);

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={SectionImage} />
);
