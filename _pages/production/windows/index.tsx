import React from 'react';
import { Section, PromoCard } from 'layout/elements';
import Typography from '@material-ui/core/Typography';
import { White } from 'layout/theme';
import { PageSections as DesignedWindows } from '_pages/production/designed';
import Page, { IHead } from 'lib/Page';
import Container from '@material-ui/core/Container';
import PriceSection, { ProductType } from 'components/PriceSection';
import promoImg from './img/bg.jpg';

const title = 'Пластиковые окна';
const subtitle = 'Виды пластиковых окон и цены.';
const id = 'header';
const desc =
  'Мы предоставляем широкий перечень работ по остеклению квартир любой сложности. Богатый ассортимент оконных профилей позволяет нам реализовывать качественные, многофункциональные и привлекательные решения в доступном ценовом диапазоне.';
const url = '/production/windows';
const view = () => (
  <>
    <White>
      <Section.Wrapper id={id}>
        <Section.Container>
          <Section.HeaderT title={title} subtitle={subtitle} h1 center />
          <Container maxWidth="md">
            <Typography align="center" variant="body1">
              {desc}
            </Typography>
            <Section.Img src={promoImg} />
          </Container>
        </Section.Container>
      </Section.Wrapper>
    </White>
  </>
);

const HeaderSection = {
  title,
  id,
  view,
};

const head: IHead = {
  title,
  description: subtitle,
};

export default () => (
  <Page sections={[HeaderSection, ...DesignedWindows]} head={head} />
);

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={promoImg} />
);
