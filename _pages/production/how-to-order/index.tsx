import React from 'react';
import { Section, PromoCard, SectionCard, Link, Button } from 'layout/elements';
import PageInfo from 'lib/PageInfo';
import * as E from 'layout/elements/SectionCard/elements';
import { Typography } from '@material-ui/core';
import { Light, White } from 'layout/theme';
import promoImg from './img/bg.jpg';

const url = '/production/how-to-order';
const title = 'Как заказать пластиковые Окна Берта?';
const desc = 'Пошаговая инструкция по заказу пластиковых окон.';

export default () => (
  <>
    <PageInfo title={title} description={desc} />
    <Section.Wrapper>
      <Section.Container>
        <Section.HeaderT title={title} subtitle={desc} h1 maxWidth/>
        <E.Card vertical>
          <E.CardMedia
            image="/static/img/order/measure.jpg"
            title="Вызов мастера"
          />
          <E.CardContent>
            <Typography gutterBottom variant="h4" component="h2">
              1. Вызов мастера.
            </Typography>
            <Typography variant="body1" color="textSecondary" component="p">
              {`С помощью  `}
              <Link
                color="textPrimary"
                underline="always"
                href="/services/order?serviceId=3"
                title="Вызвать мастера"
              >
                формы вызова мастера
              </Link>
              {`  или по телефону горячей линии  `}
              <Link
                color="textPrimary"
                underline="always"
                href="tel:8 800 550 30 25"
                title="Горячая линия"
              >
                8 800 550 30 25
              </Link>
              {`  или  `}
              <Link
                color="textPrimary"
                underline="always"
                href="http://localhost:8080/contacts"
                title="Контакты"
              >
                {`в ближайшем к вам офисе  `}
              </Link>
              {`  вызовите мастера на дом. Он точно замерит размеры оконного проема, проконсультирует вас и рассчитает точную стоимость остекления.  `}
            </Typography>
            <E.CardActions display>
              <Button
                href="/services/order?serviceId=3"
                variant="outlined"
                size="large"
                fullWidth
              >
                Вызвать мастера бесплатно
              </Button>
            </E.CardActions>
          </E.CardContent>
        </E.Card>
      </Section.Container>
    </Section.Wrapper>
    <White>
      <Section.Wrapper>
        <Section.Container>
          <E.Card vertical>
            <E.CardMedia
              image="/static/img/order/contract.jpeg"
              title="Заключение договора"
            />
            <E.CardContent>
              <Typography gutterBottom variant="h4" component="h2">
                2. Заключение договора и производство.
              </Typography>
              <Typography variant="body1" color="textSecondary" component="p">
                Если вас устраивают цена и сроки, вам предложат заключить
                договор и внести оплату. После завод начнет изготовление вашего
                окна.
              </Typography>
            </E.CardContent>
          </E.Card>
        </Section.Container>
      </Section.Wrapper>
    </White>
    <Section.Wrapper>
      <Section.Container>
        <E.Card vertical>
          <E.CardMedia
            image="/static/img/order/montage.jpg"
            title="Доставка и монтаж"
          />
          <E.CardContent>
            <Typography gutterBottom variant="h4" component="h2">
              3. Доставка и монтаж.
            </Typography>
            <Typography variant="body1" color="textSecondary" component="p">
              После изготовления вашего окна, с вами свяжется менеджер и
              назначит время для приезда монтажников. Монтажники привезут и
              установят Ваше окно к назначенному времени. Если необходимо,
              демонтируют старое окно.
            </Typography>
            <E.CardActions display>
              <Button
                href="/services/order?serviceId=3"
                variant="outlined"
                size="large"
                fullWidth
              >
                Вызвать мастера бесплатно
              </Button>
            </E.CardActions>
          </E.CardContent>
        </E.Card>
      </Section.Container>
    </Section.Wrapper>
  </>
);

export const Promo = () => (
  <PromoCard title={title} desc={desc} href={url} image={promoImg} />
);
