import React from 'react';
import { Section, PromoCard } from 'layout/elements';
import profiles from 'data/profiles';
import ProfileCard from 'components/ProfileCard';
import Page from 'lib/Page';
import { Grid } from '@material-ui/core';
import promoImg from './img/intro_mobile.jpg';

const title = 'Профильные системы';
const desc = 'Перечень профильных систем и их характеристик.';
const url = '/production/profiles';
export default () => (
  <Page
    head={{
      title,
      description: desc,
    }}
  >
    <>
      <Section.Wrapper>
        <Section.Container>
          <Section.HeaderT title={title} subtitle={desc} h1 />
          <Section.Body>
            <Grid container spacing={5}>
              {profiles.map((profile, i) => (
                <Grid item xs={12} md={4} key={i}>
                  <ProfileCard data={profile} />
                </Grid>
              ))}
            </Grid>
          </Section.Body>
        </Section.Container>
      </Section.Wrapper>
    </>
  </Page>
);

export const Promo = () => (
  <PromoCard title={title} desc={desc} href={url} image={promoImg} contain />
);
