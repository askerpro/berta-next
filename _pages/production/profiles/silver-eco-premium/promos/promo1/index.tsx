import React from 'react';
import { Typography } from '@material-ui/core';
import { EcoLight } from 'layout/theme';
import { Section, Link, Button } from 'layout/elements';
import { isMobile } from 'lib/device-detect';
import * as E from './elements';

interface State {
  animationState: 0 | 1;
  animationDuration: number;
}

class Intro extends React.Component<{}, State> {
  constructor(props) {
    super(props);
    this.state = {
      animationState: 0,
      animationDuration: null,
    };
  }

  render() {
    const { animationState, animationDuration } = this.state;

    return (
      <EcoLight>
        <Link
          href="/production/profiles/silver-eco-premium"
          underline="none"
          title="Подробнее"
        >
          <E.Section>
            <E.Container>
              <E.ImageContainer>
                {isMobile() && (
                  <E.MobileImage src="/static/animations/intro/intro-25.jpg" />
                )}
                {!isMobile() && (
                  <E.Image
                    duration={animationDuration}
                    state={animationState}
                    onAnimationPlayedEnd={() => {
                      this.setState(
                        {
                          animationDuration: 0,
                          animationState: 0,
                        },
                        () => {
                          setTimeout(() => {
                            this.setState({
                              animationDuration: null,
                              animationState: 1,
                            });
                          }, 2000);
                        },
                      );
                    }}
                    onReady={() => {
                      this.setState({
                        animationState: 1,
                      });
                    }}
                    dataSrc="/static/animations/intro/intro.json"
                  />
                )}

                <E.Privolnov src="/static/img/intro/privolnov.png" />
              </E.ImageContainer>

              <E.Content>
                <E.Header>
                  <Typography
                    variant="h5"
                    style={{ fontWeight: 400 }}
                    component="span"
                  >
                    окна нового поколения
                  </Typography>
                  <Section.Title variant="h2" color="textPrimary">
                    {`Berta Silver `}
                    <Typography variant="inherit" color="secondary">
                      Eco
                    </Typography>
                  </Section.Title>
                  <Typography variant="h5" style={{ fontWeight: 500 }}>
                    {`Антон Привольнов   `}
                    <Typography component="span" variant="h5">
                      Рекомендует
                    </Typography>
                  </Typography>
                  <Section.Links>
                    <Button variant="outlined">Подробнее</Button>
                  </Section.Links>
                </E.Header>
              </E.Content>
            </E.Container>
          </E.Section>
        </Link>
      </EcoLight>
    );
  }
}

export default Intro;
