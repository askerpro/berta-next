import styled, { css } from 'layout/theme';
import { Section as S } from 'layout/elements';
import Typography, { TypographyProps } from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';
import Animation from 'components/Animation';
import { vars as headerVars } from 'layout/header/elements';

export const vars = {
  transitionDuration: 0.7,
};

export const Header = styled(S.Header)`
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    margin-bottom: 0;
  }`}
`;

export const Section = styled(S.Wrapper)`
  margin-top: -${headerVars.height}px;
  height: calc(667px + ${headerVars.height}px);
  background-color: #e4e4e4;
  overflow: hidden;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    height: auto;
  }`}
`;

export const Container = styled(S.Container)`
  position: relative;
  padding-top: ${headerVars.height}px;
  padding-bottom: 0;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    height: auto;
    flex-flow: wrap-reverse;
    padding-top: 85px;
  }`}
`;

export const Image = styled(Animation)`
  height: 100%;
  flex-shrink: 0;
  max-width: 100%;
`;

export const MobileImage = styled.img`
  width: 279px;
  margin-left: -20px;
  max-height: 100%;
  max-width: 100%;
`;

export const ImageContainer = styled.div`
  flex-shrink: 0;
  max-width: 600px;
  display: flex;
  max-height: 100%;
  align-items: flex-end;

  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    max-height: 400px;
    width: 100%;
  }`}
`;

export const Privolnov = styled.img`
  height: 74%;
  width: auto;
  margin-left: -10%;
  z-index: 1;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
        margin-left: -26%;
  }`}
`;

export const Content = styled.div`
  max-width: 600px;
  text-align: center;
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    height: auto;
  }`}
`;
