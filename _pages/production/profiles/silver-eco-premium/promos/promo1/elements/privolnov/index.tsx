import React from 'react';
import { Typography } from '@material-ui/core';
import * as E from './elements';
import CardShadow from '../card-shadow';
import Background from '../background';

interface Props {
  state: 0 | 1;
  animationPlayed: boolean;
}

export default ({ state, animationPlayed }: Props) => {
  return (
    <E.Card state={state}>
      <E.BackgroundWrapper visible={animationPlayed}>
        <Background state={state} />
      </E.BackgroundWrapper>
      <E.Container>
        <E.Image />
        <E.Header>
          <Typography variant="h5" color="textSecondary">
            Эксперт по качеству - Антон Привольнов
          </Typography>
          <Typography variant="h4" color="textPrimary">
            Рекомендует!
          </Typography>
        </E.Header>
      </E.Container>
      <E.ShadowWrapper>
        <CardShadow />
      </E.ShadowWrapper>
    </E.Card>
  );
};
