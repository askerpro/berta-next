import styled, { css } from 'layout/theme';

export const vars = {
  transitionDuration: 0.7,
};

export interface ICard {
  state: 0 | 1;
}

export const Recommend = styled.div`
  transition: 0.4s;
  position: absolute;
  bottom: 0;
  background: linear-gradient(#0000, #000000ba);
  padding: 30px;
  text-align: center;
`;

export const Card = styled.div<ICard>`
  background-color: #dbdbdb;
  width: 50%;
  height: 100vh;
  position: absolute;
  top: 0;
  transform: translateX(${({ state }) => (state ? '0%' : '100%')});
  transition: 1s;
  right: 0;
  overflow: hidden;
`;

export const Container = styled.div`
  margin: 0 auto;
  height: calc(100% - 72px);
  margin-top: 72px;
  display: flex;
  justify-content: center;
  position: relative;
`;

export const Image = styled.div`
  background-image: url(/static/img/intro/privolnov.png);
  background-repeat: no-repeat;
  position: absolute;
  width: 100%;
  height: 80%;
  bottom: 0;
  background-position: 50% 100%;
  background-size: contain;
  transition: 0.4s;
`;

export const Header = styled.div`
  max-width: 600px;
  text-align: center;
  position: absolute;
  top: 50px;
`;

export const ShadowWrapper = styled.div`
  height: 100%;
  width: auto;
  position: absolute;
  top: 0;
  left: 0;
  transform: scaleX(-1);
`;
interface IBackground {
  visible: boolean;
}

export const BackgroundWrapper = styled.div<IBackground>`
  visibility: ${({ visible }) => (visible ? 'visible' : 'hidden')};
  position: absolute;
  height: 100%;
  right: 0;
`;
