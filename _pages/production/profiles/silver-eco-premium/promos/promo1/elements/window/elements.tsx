import styled, { css } from 'layout/theme';
import Animation from 'components/Animation';

export const vars = {
  transitionDuration: 0.7,
};

export interface IContainer {
  state: 0 | 1;
}

interface IIconContent {
  index: number;
}

export const IconContent = styled.div<IIconContent>`
  transition-duration: 0s;
  transition-delay: ${({ index }) => index * 0.2}s;

  text-align: right;
`;

export const Container = styled.div<IContainer>`
  background-color: #dbdbdb;
  width: 50%;
  height: 100vh;
  position: absolute;
  top: 0;
  display: flex;
  justify-content: center;
  transform: translateX(${({ state }) => (state ? '0%' : '-100%')});
  transition: 1s;
  overflow: hidden;
`;

interface IImage {
  visible: boolean;
}

export const Image = styled(Animation)<IImage>`
  height: calc(100% - 72px);
  margin-top: 72px;
  visibility: ${({ visible }) => (visible ? 'visible' : 'hidden')};
`;

export const Content = styled.div`
  height: calc(100% - 72px);
  position: absolute;
  top: 72px;
  left: 15%;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  max-width: 150px;
`;

export const Icon = styled.div`
  width: auto;
  height: 45px;
`;

export const IconText = styled.div`
  text-align: right;
`;

export const ShadowWrapper = styled.div`
  height: 100%;
  width: auto;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 2;
`;

interface IFinalImage {
  visible: boolean;
}

export const FinalImage = styled.div<IFinalImage>`
  background-image: url('/static/animations/intro/intro.final.png');
  position: absolute;
  height: calc(100% - 72px);
  top: 72px;
  width: 100%;
  background-size: contain;
  background-position: 50%;
  background-repeat: no-repeat;
  z-index: ${({ visible }) => (visible ? 1 : -1)};
`;

interface IBackground {
  visible: boolean;
}

export const BackgroundWrapper = styled.div<IBackground>`
  visibility: ${({ visible }) => (visible ? 'visible' : 'hidden')};
  position: absolute;
  height: 100%;
  left: 0;
`;
