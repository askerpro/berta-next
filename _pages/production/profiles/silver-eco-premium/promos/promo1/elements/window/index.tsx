import React from 'react';
import { Heart, Ag, EasyCare } from 'layout/elements/Icon';
import { Settings } from '@material-ui/icons';
import { Typography } from '@material-ui/core';
import * as E from './elements';
import CardShadow from '../card-shadow';

import Background from '../background';

const content = [
  {
    icon: EasyCare,
    text: 'Экологически чистые',
  },
  {
    icon: Heart,
    text: 'Безопасные для здоровья',
  },
  {
    icon: Ag,
    text: 'Антибактериальные свойства',
  },
  {
    icon: Settings,
    text: 'Внушительные характеристики',
  },
];

interface Props {
  onReady(): void;
  onPlayed(): void;
  animationPlayed: boolean;
  state: 1 | 0;
}

export default ({ state, onReady, onPlayed, animationPlayed }: Props) => {
  return (
    <E.Container state={state}>
      <E.BackgroundWrapper visible={animationPlayed}>
        <Background state={state} />
      </E.BackgroundWrapper>
      <E.Image
        state={state ? 1 : 0}
        onReady={() => {
          onReady();
        }}
        onAnimationPlayedEnd={() => {
          onPlayed();
        }}
        visible={!animationPlayed}
        dataSrc="/static/animations/intro/intro.json"
        adjust
      />
      <E.FinalImage visible={animationPlayed} />
      <E.Content>
        {content.map((El, i) => (
          <E.IconContent index={i}>
            <E.Icon as={El.icon} />
            <E.IconText>
              <Typography color="textPrimary" variant="subtitle1">
                {El.text}
              </Typography>
            </E.IconText>
          </E.IconContent>
        ))}
      </E.Content>
      <E.ShadowWrapper>
        <CardShadow />
      </E.ShadowWrapper>
    </E.Container>
  );
};
