import React from 'react';
import styled from 'layout/theme';

const Svg = styled.svg`
  height: 100%;
  width: auto;
`;

export default () => (
  <Svg viewBox="0 0 100.484 705.747">
    <filter id="innershadow" width="100%" height="200%">
      <feGaussianBlur in="SourceAlpha" stdDeviation="2" result="blur" />
      <feOffset dy="1" dx="1" />
      <feComposite
        in2="SourceAlpha"
        operator="arithmetic"
        k2="-1"
        k3="1"
        result="shadowDiff"
      />

      <feFlood floodColor="#444444" floodOpacity="0.5" />
      <feComposite in2="shadowDiff" operator="in" />
      <feComposite in2="SourceGraphic" operator="over" result="firstfilter" />

      <feGaussianBlur in="firstfilter" stdDeviation="3" result="blur2" />
      <feOffset dy="-1" dx="-1" />
      <feComposite
        in2="firstfilter"
        operator="arithmetic"
        k2="-1"
        k3="1"
        result="shadowDiff"
      />

      <feFlood floodColor="#444444" floodOpacity="0.5" />
      <feComposite in2="shadowDiff" operator="in" />
      <feComposite in2="firstfilter" operator="over" />
    </filter>
    <g transform="scale(1 -1)">
      <g transform="translate(0 -705.747)">
        <path
          d="M 90.119,0 L 90.236,285.904 C 90.236,290.372 86.608,294 82.14,294 L 8.211,294 C 3.679,294 0,297.679 0,302.211 L 0,404.054 C 0,408.586 3.679,412.265 8.211,412.265 L 82.14,412.265 C 86.608,412.265 90.236,415.893 90.236,420.361 L 90.119,705.747 L 131.367,705.747 L 131.367,0 L 90.119,0 Z"
          fill="#ffffff"
          filter="url(#innershadow)"
        />
      </g>
    </g>
  </Svg>
);
