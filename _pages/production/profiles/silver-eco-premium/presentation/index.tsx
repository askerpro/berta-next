/* eslint-disable react/no-direct-mutation-state */
import React from 'react';
import Page from 'lib/Page';
import { EcoDark } from 'layout/theme';
import { PageNav } from 'layout/elements';
import VisibilitySensor from 'components/VisibilitySensor';
import { isMobile } from 'lib/device-detect';
import { Typography } from '@material-ui/core';
import IntroSection from './sections/intro';
import Profile, { details as profileDetails } from './sections/profile';
import Veka, { details as productionDetails } from './sections/veka';
import Glass, { details as glassDetails } from './sections/glass';
import Furniture, { details as furnitureDetails } from './sections/furniture';
import Assembly, { details as assemblyDetails } from './sections/assembly';
import PrivolnovEnd from './sections/privolnov';
import Calc, { details as calcDetails } from './sections/calc';

const title = 'Berta Silver Eco';
const description =
  'Инновационный, экологически чистый и безопасный для здоровья продукт с содержанием серебра по уникальной запатентованной технологии Silver Eco.  Единственная в России оконная система с содержанием серебра, которая к тому же обладает антибактериальным и антигрибковым эффектом.';

const head = {
  title,
  description,
};

interface State {}

class ProfilePresentation extends React.Component<{}, State> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillUnmount() {
    window.onscroll = null;
    window.onhashchange = null;
  }

  render() {
    return (
      <EcoDark>
        <>
          <PageNav
            title={
              // eslint-disable-next-line react/jsx-wrap-multilines
              <Typography variant="inherit" component="span">
                Berta Silver
                <Typography
                  variant="inherit"
                  component="span"
                  color="secondary"
                >
                  {` Eco`}
                </Typography>
              </Typography>
            }
            activeSection={-1}
            sections={[
              profileDetails,
              productionDetails,
              glassDetails,
              furnitureDetails,
              assemblyDetails,
            ]}
            action={{
              label: calcDetails.title,
              href: `#${calcDetails.id}`,
            }}
          />
          <IntroSection />
          <Profile />
          <Veka />
          <Furniture />
          <Glass />
          <Assembly />
          <PrivolnovEnd />
          <Calc />
        </>
      </EcoDark>
    );
  }
}

export default () => (
  <Page head={head}>
    <ProfilePresentation />
  </Page>
);
