import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography, Hidden } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

const videoSrc =
  '/static/img/presentations/silver-eco/privolnov/privolnov_end.mp4';

const totalDuration = 1.5;

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  mediaRef = React.createRef<HTMLVideoElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  shouldComponentUpdate(nextProps, nextState: State) {
    const { progress } = this.state;
    if (
      nextState.progress !== progress &&
      nextState.progress >= 0.5 &&
      nextState.progress <= 1
    ) {
      this.mediaRef.current.play();
    } else {
      this.mediaRef.current.pause();
    }
    return true;
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: '-100vh',
          }}
          progress={progress}
        >
          <E.Container>
            <E.MediaContainer>
              <E.Video src={videoSrc} muted autoPlay ref={this.mediaRef} />
              <E.VideoContainerCover
                style={{
                  opacity: getLocalProgress(-0.2, 0.5, progress, 0.3, 1) * 0.7,
                }}
              />
            </E.MediaContainer>

            <CE.Content visible style={{ maxWidth: '700px' }}>
              <E.ST
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0.5}
                duration={2}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  ДА! Если это "Berta Silver Eco".
                </Typography>

                <Typography variant="h4" color="textPrimary" gutterBottom>
                  Я проверил и убедился в этом лично!
                </Typography>
                <Typography variant="body1" color="textPrimary">
                  (С) Эксперт по качеству - Антон Привольнов.
                </Typography>
              </E.ST>
            </CE.Content>
          </E.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
