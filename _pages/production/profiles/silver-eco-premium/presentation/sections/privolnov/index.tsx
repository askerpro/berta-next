import React from 'react';
import Intro from './intro';
import More from './more';

export default () => {
  return (
    <>
      <Intro />
      <More />
    </>
  );
};
