import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography, Hidden } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

const poster =
  '/static/img/presentations/silver-eco/privolnov/privolnov_end.jpg';

const totalDuration = 1;

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
          }}
          progress={progress}
        >
          <E.Container>
            <E.MediaContainer>
              <E.Img src={poster} />
              <E.VideoContainerCover
                style={{
                  opacity: getLocalProgress(-0.2, 0.5, progress, 0.3, 1) * 0.7,
                }}
              />
            </E.MediaContainer>

            <CE.Content visible style={{ maxWidth: '700px' }}>
              <E.ST
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0}
                duration={1}
              >
                <Typography variant="h3" color="textPrimary">
                  Так могут ли пластиковые окна быть экологически чистыми и
                  безопасными?
                </Typography>
              </E.ST>
            </CE.Content>
          </E.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
