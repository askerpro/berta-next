import styled from 'layout/theme';

export const Wrapper = styled.div`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    max-width: 100%;
    will-change: opacity, transform;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
     margin-top: 30px;
  }`}
`;

export default {};
