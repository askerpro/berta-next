import React from 'react';
import { Hidden } from '@material-ui/core';
import * as E from './elements';

interface Props {
  sectionProgress: number;
  position: number;
  duration: number;
  sectionDuration: number;
  className?: string;
}

class SectionText extends React.Component<Props, {}> {
  getLocalProgress = () => {
    const { sectionProgress, sectionDuration, position, duration } = this.props;
    return (sectionProgress * sectionDuration - position) / duration;
  };

  getParams() {
    const progress = this.getLocalProgress();
    const styles = {
      opacity: 1,
      transform: `translateY(${-(80 * progress - 40)}px)`,
    };

    if (progress >= 0.9) {
      styles.opacity = 1 - (progress - 0.8) / 0.2;
    }
    if (progress <= 0.1) {
      styles.opacity = progress / 0.3;
    }
    return styles;
  }

  render() {
    const { children, className, sectionProgress } = this.props;
    return (
      <>
        {/* <div
          style={{
            zIndex: 1,
            padding: '20px',
            background: '#fff',
            color: '#000',
          }}
        >
          {`${this.getLocalProgress()}/${sectionProgress}`}
        </div> */}
        <Hidden mdDown>
          <E.Wrapper className={className} style={this.getParams()}>
            {children}
          </E.Wrapper>
        </Hidden>
        <Hidden smUp>
          <E.Wrapper className={className}>{children}</E.Wrapper>
        </Hidden>
      </>
    );
  }
}

export default SectionText;
