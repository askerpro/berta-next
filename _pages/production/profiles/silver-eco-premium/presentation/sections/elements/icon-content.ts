import styled from 'layout/theme';

interface IIconContent {
  index: number;
  visible: boolean;
}

export const IconContent = styled.div<IIconContent>`
  transition-duration: 0s;
  transition-delay: ${({ index }) => index * 0.1}s;
  opacity: ${({ visible }) => (visible ? 1 : 0)};
  text-align: right;
`;

export const IconContentWrapper = styled.div`
  padding: 47px 0;
  position: absolute;
  height: 100%;
  margin-right: -50px;
  top: 0;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  max-width: 150px;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
        max-width: 75px;
    }`}
`;

export const Icon = styled.img`
  width: auto;
  height: 45px;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
        height: 24px;
    }`}
`;

export const IconText = styled.div`
  text-align: right;
  font-size: 15px;
`;
