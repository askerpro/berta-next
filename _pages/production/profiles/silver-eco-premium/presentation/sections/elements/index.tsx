import Animation from 'components/Animation';
import styled, { css } from 'layout/theme';
import { Section as S } from 'layout/elements';
import { vars } from 'layout/header/elements';

interface ISection {
  progress?: number;
}
export const Section = styled(S.Wrapper)<ISection>`
  overflow: unset;
  z-index: -1;
  margin-bottom: 12px;
  ${({ progress }) =>
    progress >= 0 &&
    progress <= 1 &&
    css`
      z-index: 1;
    `}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    height: auto !important;
    margin-top: unset !important;
    z-index: unset !important;
    overflow: hidden;
  }`}
`;

export const Container = styled(S.Container)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    height: calc(100vh - ${vars.height}px);
    position: sticky;
    top: ${vars.height}px;
    display: flex;
    justify-content: flex-end;
  }`}
`;

export const Image = styled(Animation)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    height: 100%;
    top: 0;
    right: 50%;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    margin-bottom: 30px;
  }`}
`;

interface IContent {
  visible: boolean;
}

export const Content = styled.div<IContent>`
  ${({ theme, visible }) => `${theme.breakpoints.up('sm')} {    
    z-index: 1;
    width: 100%;
    max-width: 40%;
    position: relative;
    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    transition: 1s;
  }`}
`;
