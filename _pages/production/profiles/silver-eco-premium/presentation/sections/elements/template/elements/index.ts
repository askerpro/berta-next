import Animation from 'components/Animation';
import styled, { css } from 'layout/theme';
import { Section as S } from 'layout/elements';
import { vars } from 'layout/header/elements';

export const Wrapper = styled(S.Wrapper)`
  overflow: unset;
  background-color: unset;
  margin-bottom: 0;
`;

export const Container = styled(S.Container)`
  height: calc(100vh - ${vars.height}px);
  position: sticky;
  top: ${vars.height}px;
  display: flex;
  justify-content: flex-end;
`;

export const Image = styled(Animation)`
  opacity: 0;
  height: 100%;
  ${({ state }) =>
    state &&
    css`
      opacity: 1;
    `}
`;

export const Content = styled.div`
  width: 40%;
  position: relative;
  height: 100%;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  @media (max-width: 768px) {
    max-width: unset;
  }
`;
