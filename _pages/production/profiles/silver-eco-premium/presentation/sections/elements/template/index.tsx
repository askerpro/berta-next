import React from 'react';
import * as E from './elements';
import SectionText from '../text';

export const getLocalProgress = (
  position: number,
  duration: number,
  globalProgress: number,
  minValue?: number,
  maxValue?: number,
) => {
  let value = (globalProgress - position) / duration;
  if (minValue !== undefined && value < minValue) value = minValue;
  if (maxValue !== undefined && value > maxValue) value = maxValue;
  return value;
};

export const getSectionProgress = (ref: React.RefObject<HTMLDivElement>) => {
  if (ref.current)
    return (
      (document.body.scrollTop + 60 - ref.current.getBoundingClientRect().top) /
      (ref.current.getBoundingClientRect().height -
        document.documentElement.clientHeight)
    );
  return null;
};

export interface ISectionText {
  position: number;
  content: React.FC;
  duration: number;
}

interface Props {
  totalDuration: number;
  title: string;
  id: string;
  imageSrc: string;
  texts: ISectionText[];
}

interface State {
  mounted: boolean;
}

export class Content extends React.Component<Props, State> {
  ref = React.createRef<HTMLDivElement>();

  sectionsProgress: Array<{
    progress: number;
  }>;

  constructor(props: Props) {
    super(props);
    const { texts } = this.props;
    this.sectionsProgress = new Array(texts.length);
    this.sectionsProgress.fill({
      progress: 0,
    });
    this.state = {
      mounted: false,
    };
  }

  componentDidMount() {
    this.setState({
      mounted: true,
    });
  }

  getSectionProgress = () => {
    const { mounted } = this.state;
    if (mounted) {
      return (
        (document.body.scrollTop -
          this.ref.current.getBoundingClientRect().top) /
        (this.ref.current.getBoundingClientRect().height -
          document.documentElement.clientHeight)
      );
    }

    return 0;
  };

  render() {
    const { id, texts, totalDuration, imageSrc } = this.props;
    const progress = this.getSectionProgress();
    return (
      <E.Wrapper
        ref={this.ref}
        style={{
          height: `calc(${totalDuration} * 900px + 110vh)`,
          marginTop: '-100vh',
        }}
        id={id}
      >
        <E.Container>
          <E.Image state={progress >= 0 ? 1 : 0} dataSrc={imageSrc} />
          <E.Content>
            {texts.map((Section, i) => (
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={Section.position}
                duration={Section.duration}
                key={i}
              >
                <Section.content />
              </SectionText>
            ))}
          </E.Content>
        </E.Container>
      </E.Wrapper>
    );
  }
}

export default Content;
