import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '../elements';
import SectionText from '../elements/text';
import { getSectionProgress, getLocalProgress } from '../elements/template';
import * as E from './elements';
import Line from './line';

export const title = 'Профиль';
const id = 'profile';
const imageSrc =
  '/static/animations/presentations/silver-eco/profile/params/data.json';

const totalDuration = 3;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);
    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: 'calc(-100vh)',
          }}
          progress={progress}
          id={id}
        >
          <CE.Container>
            <CE.Image
              state={progress >= 0 ? 1 : 0}
              onAnimationPlayedEnd={() => {
                this.setState({
                  animationPlayed: true,
                });
              }}
              dataSrc={imageSrc}
            >
              {/* <E.LinesWrapper>
                <Line
                  top="61.9%"
                  left={31}
                  visible={animationPlayed}
                  duration={0}
                  position={0}
                  progress={progress * totalDuration}
                  opacityPosition={0.75}
                  title="1.5mm толщина армирования"
                />
                <Line
                  top="65.3%"
                  left={33.3}
                  visible={progress * totalDuration >= 1}
                  duration={0.1}
                  position={1}
                  progress={progress * totalDuration}
                  opacityPosition={1.75}
                  title="5 воздушных камер"
                />
                <Line
                  top="65.3%"
                  left={34.3}
                  visible={progress * totalDuration >= 2}
                  duration={0.1}
                  position={2}
                  progress={progress * totalDuration}
                  opacityPosition={4}
                  title="3mm толщина внешних стенок"
                />
              </E.LinesWrapper> */}
            </CE.Image>
            <CE.Content visible={animationPlayed}>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={-0.5}
                duration={1.5}
              >
                <Typography color="textPrimary" variant="h4">
                  1.5mm толщина армирования
                </Typography>
                <Typography color="textPrimary">
                  Такая толщина обеспечивает условия для противодействия
                  эксплуатационным нагрузкам, крепежа фурнитуры и монтажа
                  конструкции. В профиль с монтажной глубиной 70мм есть
                  возможность установки 1\2 камерного стеклопакета, с
                  максимальной шириной в 32мм.
                </Typography>
              </SectionText>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={1}
                duration={1}
              >
                <Typography color="textPrimary" variant="h4">
                  5 воздушных камер
                </Typography>
                <Typography color="textPrimary">
                  5 воздушных камер позволяет окну сохранять тепло-звуко
                  изоляцию на высоком уровне, что гарантирует, что в доме всегда
                  будет тишина и комфорт.
                </Typography>
              </SectionText>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={2}
                duration={1.5}
              >
                <Typography color="textPrimary" variant="h4">
                  3мм толщина стенок
                </Typography>
                <Typography color="textPrimary">
                  Толщина стенок в 3мм обеспечивает прочность сварных углов
                  соединений, надежность крепления фурнитуры, ударостойкость и
                  прочность поверхности. Все вышеперечисленные характеристики
                  влияют на долговечность изделия.
                </Typography>
              </SectionText>
            </CE.Content>
          </CE.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
