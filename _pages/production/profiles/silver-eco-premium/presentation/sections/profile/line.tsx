import React from 'react';
import * as E from './elements';
import { getLocalProgress } from '../elements/template';

interface Props {
  top: string;
  left: number;
  position: number;
  duration: number;
  progress: number;
  visible: boolean;
  opacityPosition: number;
  title: string;
}

export default ({
  position,
  duration,
  left,
  top,
  progress,
  visible,
  opacityPosition,
  title,
}: Props) => {
  const lineProgress = getLocalProgress(position, duration, progress, 0, 1);
  const opacityProgress = getLocalProgress(
    opacityPosition,
    0.25,
    progress,
    0,
    1,
  );
  return (
    <div>
      <E.Line
        style={{
          top,
          left: visible ? `${300 - lineProgress * (300 - left)}%` : '300%',
          opacity: 1 - opacityProgress,
        }}
      >
        {visible && <E.LinePointer />}
      </E.Line>

      <E.LineText
        variant="h4"
        style={{
          visibility: visible ? 'visible' : 'hidden',
          top: `calc(${top} + 20px)`,
          opacity:
            progress < opacityPosition ? lineProgress : 1 - opacityProgress,
        }}
      >
        {title}
      </E.LineText>
    </div>
  );
};
