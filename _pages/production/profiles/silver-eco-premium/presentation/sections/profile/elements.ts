import styled from 'layout/theme';
import { Typography } from '@material-ui/core';
import SectionText from '../elements/text';

export const LinesWrapper = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: calc(-1220px / 2);
  z-index: 1;
  overflow: hidden;
`;

export const Line = styled.div`
  left: 100%;
  right: 0;
  height: 2px;
  background-color: #578e55;
  transition: 0.7s;
  position: absolute;
`;

export const LinePointer = styled.div`
  position: absolute;
  height: 10px;
  width: 10px;
  border-radius: 50%;
  background-color: #578e55;
  border: 2px solid;
  top: -5px;
  left: 0px;
`;

export const LineText = styled(Typography)`
  position: absolute;
  right: calc(0px - 1280px / 2 + 40px);
  width: 486px;
`;

export const paramsST = styled(SectionText)`
  top: 30%;
`;
