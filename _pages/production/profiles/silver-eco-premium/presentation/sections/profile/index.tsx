import React from 'react';
import Intro, { id, title } from './intro';
import Params from './params';

export default () => {
  return (
    <>
      <Intro />
      <Params />
    </>
  );
};

export const details = { id, title };
