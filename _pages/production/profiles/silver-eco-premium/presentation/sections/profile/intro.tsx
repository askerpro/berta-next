import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '../elements';
import SectionText from '../elements/text';
import { getSectionProgress } from '../elements/template';

export const title = 'Профиль';
export const id = 'profile';
const imageSrc =
  '/static/animations/presentations/silver-eco/profile/more/data.json';

const totalDuration = 1;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);
    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: 'calc(-100vh)',
          }}
          progress={progress}
          id={id}
        >
          <CE.Container>
            <CE.Image
              state={progress >= 0 ? 1 : 0}
              onAnimationPlayedEnd={() => {
                this.setState({
                  animationPlayed: true,
                });
              }}
              dataSrc={imageSrc}
            />

            <CE.Content visible={animationPlayed}>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={-0.5}
                duration={1.5}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  Гладкий глянцевый профиль
                </Typography>
                <Typography color="textPrimary">
                  Отсутствие мелких шероховатостей на поверхности профиля
                  позволяет ему больше оставаться не загрязненным, ведь именно в
                  этих невидимых глазу шероховатостях остаются частички пыли,
                  которые невозможно вывести при обычной мойке готового окна,
                  из-за которых внешний вид окна перестает быть привлекательным.
                </Typography>
              </SectionText>
            </CE.Content>
          </CE.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
