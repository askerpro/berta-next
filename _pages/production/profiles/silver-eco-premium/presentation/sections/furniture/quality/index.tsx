import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import SectionText from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/text';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import { Progress } from 'layout/elements';
import * as E from '../elements';

export const title = 'Профиль';
const id = 'profile';
const imageSrc =
  '/static/animations/presentations/silver-eco/furniture/quality/data.json';

const totalDuration = 1;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);

    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: 'calc(-100vh)',
          }}
          progress={progress}
          id={id}
        >
          <E.Container>
            <E.Image
              state={progress >= 0 ? 1 : 0}
              onAnimationPlayedEnd={() => {
                this.setState({
                  animationPlayed: true,
                });
              }}
              dataSrc={imageSrc}
            />

            <CE.Content visible={animationPlayed}>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0}
                duration={1}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  Высокий ресурс прочности
                </Typography>
                <Typography component="span" color="textPrimary">
                  Эта фурнитура отличается высокой долговечностью и имеет
                  потенциальный ресурс 100 000 циклов открываний (Когда ГОСТ
                  составляет 20 000 открываний).
                </Typography>
                <Progress title="ГОСТ (10 000 открываний)" value={20} />
                <Progress
                  title="Фурнитура Silver ECO (100 000 открываний)"
                  value={animationPlayed ? 100 : 0}
                  heroValue="+500%"
                />
              </SectionText>
            </CE.Content>
          </E.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
