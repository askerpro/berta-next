import React from 'react';
import Intro, { id, title } from './intro';
import More from './more';
import Quality from './quality';
import HandleIntro from './handleIntro';
import HandleMore from './handleMore';

export default () => {
  return (
    <>
      <Intro />
      <More />
      <Quality />
      <HandleIntro />
      <HandleMore />
    </>
  );
};

export const details = { id, title };
