import styled, { css } from 'layout/theme';
import { Image as ImageT, Container as ContainerT } from '../elements';

export const Image = styled(ImageT)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    right: unset;
    left: 50%;
  }`}
`;

export const Container = styled(ContainerT)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    justify-content: flex-start;
  }`}
`;
