import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import SectionText from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/text';
import { getSectionProgress } from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from '../elements';

export const title = 'Фурнитура';
export const id = 'furniture';
const imageSrc =
  '/static/animations/presentations/silver-eco/furniture/intro/data.json';

const totalDuration = 0.5;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);

    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            zIndex: 1,
          }}
          progress={progress}
          id={id}
        >
          <E.Container>
            <E.Image
              state={progress >= 0 ? 1 : 0}
              onAnimationPlayedEnd={() => {
                this.setState({
                  animationPlayed: true,
                });
              }}
              dataSrc={imageSrc}
            />

            <CE.Content visible={animationPlayed}>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={-0.5}
                duration={1}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  Надежная фурнитура
                </Typography>
                <Typography component="span" color="textPrimary">
                  Фурнитура имеет значение не меньшее, чем сам профиль. Каким бы
                  энергосберегающим и эстетичным не был профиль - если ручки не
                  закрываются, механизм заедает и створка не двигается, то очень
                  скоро вы пожалете, что сэкономили на окне. Фурнитура Silver
                  ECO в отличие от стальной, не подвержена коррозии и не
                  промерзает, что полностью исключает образование
                  дополнительного конденсата на элементах фурнитуры, а по своей
                  прочности практически ничем не уступает стали.
                </Typography>
              </SectionText>
            </CE.Content>
          </E.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
