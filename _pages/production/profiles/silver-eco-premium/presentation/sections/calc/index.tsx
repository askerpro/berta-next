import React from 'react';
import { EcoLight } from 'layout/theme';
import { Typography } from '@material-ui/core';

import { getSectionProgress } from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import { Section } from 'layout/elements';
import Calc from 'components/Calc';

export const title = 'Стоимость';
const id = 'calc';

export const details = { id, title };

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoLight>
        <Section.Wrapper ref={this.ref} id={id} noMargin>
          <Section.Container>
            <Section.HeaderT
              title="Рассчитать стоимость"
              subtitle="Рассчитать стоимость окна по вашим размерам."
            />
            <Section.Body>
              <Calc />
              <Section.Container>
                <Typography>
                  Обращаясь в нашу компанию, клиенты могут быть уверены в том,
                  что заказанные окна и двери будут в точности соответствовать
                  их представлениям о качественных окнах.
                </Typography>
              </Section.Container>
            </Section.Body>
          </Section.Container>
        </Section.Wrapper>
      </EcoLight>
    );
  }
}

export default Intro;
