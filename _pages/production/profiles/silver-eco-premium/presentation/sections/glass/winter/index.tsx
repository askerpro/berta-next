import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import SectionText from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/text';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

export const title = 'Профиль';
const id = 'profile';
const imageSrc =
  '/static/animations/presentations/silver-eco/glass/winter/data-0.jpg';

const totalDuration = 1.5;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  snowFlakes1 = new Array(30).fill(null);

  snowFlakes2 = new Array(30).fill(null);

  snowFlakes3 = new Array(30).fill(null);

  constructor(props) {
    super(props);
    this.snowFlakes1 = this.snowFlakes1.map(snowFlake => ({
      animationDelay: `${Math.random() * 10}s, ${Math.random() * 3}s`,
      animationDuration: '10s, 3s',
      left: `${Math.random() * 100}%`,
      fontSize: `${Math.random() + 0.5}rem`,
      filter: 'blur(1px)',
    }));

    this.snowFlakes2 = this.snowFlakes2.map(snowFlake => ({
      animationDelay: `${Math.random() * 14}s, ${Math.random() * 4}s`,
      animationDuration: '14s, 4s',
      left: `${Math.random() * 100}%`,
      fontSize: `${Math.random() + 0.5}rem`,
      filter: `blur(2px)`,
    }));

    this.snowFlakes3 = this.snowFlakes2.map(snowFlake => ({
      animationDelay: `${Math.random() * 18}s, ${Math.random() * 6}s`,
      animationDuration: '18s, 6s',
      left: `${Math.random() * 100}%`,
      fontSize: `${Math.random() + 0.5}rem`,
      filter: `blur(4px)`,
    }));

    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: 'calc(-100vh)',
          }}
          progress={progress}
          id={id}
        >
          <CE.Container>
            <E.ImageWrapper>
              <E.snowContainer
                style={{
                  opacity:
                    progress >= 0.8
                      ? 1 - getLocalProgress(0.8, 0.2, progress, 0, 1)
                      : 1,
                }}
              >
                {this.snowFlakes1.map((flake, i) => {
                  return (
                    <E.snowFlake key={i} style={flake}>
                      o
                    </E.snowFlake>
                  );
                })}
                {this.snowFlakes2.map((flake, i) => {
                  return (
                    <E.snowFlake key={i * 2} style={flake}>
                      o
                    </E.snowFlake>
                  );
                })}

                {this.snowFlakes3.map((flake, i) => {
                  return (
                    <E.snowFlake key={i * 3} style={flake}>
                      o
                    </E.snowFlake>
                  );
                })}
              </E.snowContainer>
              <E.Image src={imageSrc} />
            </E.ImageWrapper>

            <CE.Content visible>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0.5}
                duration={1}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  Бережет тепло зимой
                </Typography>
                <Typography component="span" color="textPrimary">
                  Между стенками этого стеклопакета находится специальный газ
                  вместо воздуха, который позволяет удерживать в помещении на
                  95% больше тепла. Это помогает снизить затраты на отопление и
                  обеспечивает комфорт в самые холодные заморозки.
                </Typography>
              </SectionText>
            </CE.Content>
          </CE.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
