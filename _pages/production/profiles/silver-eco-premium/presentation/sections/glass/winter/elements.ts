import styled, { css } from 'layout/theme';
import { keyframes } from 'styled-components';

export const ImageWrapper = styled.div`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    height: 100%;
    top: 0;
    left: 22%;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    height: 500px;
    position: relative;
    transform: translateX(35%);
  }`}
`;

export const Image = styled.img`
  height: 100%;
  width: auto;
`;

interface ISnowConainer {
  opacity: number;
}

export const snowContainer = styled.div`
  width: 900px;
  height: 100%;
  position: absolute;
  right: 69%;
  top: 0;
  z-index: 1;
`;

const snowFall = keyframes`
  from {
      top: 0%;
  }
  to {
      top: 100%;
  }
`;
const snowShake = keyframes`
  0%{
      transform: translateX(0px);
  }

  50% {
      transform: translateX(-30px);
  }

  100% {
      transform: translateX(0px);
  }
`;

export const snowFlake = styled.div`
  font-size: 1rem;
  line-height: 50%;
  border-radius: 50%;
  top: -10%;
  position: absolute;
  transform: translateY(0vh);
  user-select: none;
  cursor: default;
  background-color: #fff;
  animation-name: ${snowFall}, ${snowShake};
  animation-duration: 10s, 3s;
  animation-timing-function: linear, ease-in-out;
  animation-iteration-count: infinite, infinite;
  animation-play-state: running, running;
`;
