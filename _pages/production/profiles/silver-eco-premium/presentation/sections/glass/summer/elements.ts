import styled, { css } from 'layout/theme';
import { keyframes } from 'styled-components';

export const ImageWrapper = styled.div`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    height: 100%;
    top: 0;
    left: 22%;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    height: 500px;
    position: relative;
    transform: translateX(35%);
  }`}
`;

export const Image = styled.img`
  height: 100%;
  width: auto;
`;

export const RaysWrapper = styled.div`
  position: absolute;
  left: -89%;
  top: 0;
  right: 53.7%;
  bottom: 0;
  overflow: hidden;
`;

interface IRay {
  active: boolean;
  index: number;
}

export const DirectRay = styled.div<IRay>`
  height: 2px;
  background-color: #fff700c7;
  width: 650px;
  filter: blur(1px);
  position: absolute;
  transform: rotate(45deg)
    translate(${({ active }) => (active ? '-5%' : '-100%')}, 0%);
  left: 50%;
  transition: 0.7s;
  top: calc(15% + ${({ index }) => index * 30}px);
`;

interface IReflectedRay extends IRay {
  rotateAngle: number;
}

export const ReflectedRay = styled.div<IReflectedRay>`
  height: 1px;
  background: linear-gradient(to right, #ffffff03 6%, #ffd667ad 122%);
  width: ${({ active }) => (active ? '600px' : '0px')};
  filter: blur(1px);
  position: absolute;
  transform: rotate(${({ rotateAngle }) => rotateAngle}deg);
  right: 0;
  transform-origin: right;
  transition: 0.7s;
  top: calc(29.6% + ${({ index }) => index * 30}px);
`;
