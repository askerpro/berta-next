import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import SectionText from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/text';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

export const title = 'Профиль';
const id = 'profile';
const imageSrc =
  '/static/animations/presentations/silver-eco/glass/summer/data-0.jpg';

const totalDuration = 1;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  directRays = new Array(5).fill(null);

  reflectedRays = new Array(10).fill(null).map(() => Math.random() * 120 - 60);

  constructor(props) {
    super(props);

    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: 'calc(-100vh)',
          }}
          progress={progress}
          id={id}
        >
          <CE.Container>
            <E.ImageWrapper>
              <E.Image src={imageSrc} />
              <E.RaysWrapper>
                {this.directRays.map((ray, i) => (
                  <E.DirectRay active={progress > 0} index={i} />
                ))}
                {this.reflectedRays.map((angle, i) => (
                  <E.ReflectedRay
                    active={progress > 0}
                    index={i / 2}
                    rotateAngle={angle}
                  />
                ))}
              </E.RaysWrapper>
            </E.ImageWrapper>

            <CE.Content visible>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0}
                duration={1.5}
              >
                <Typography component="span" variant="h3" color="textPrimary">
                  Солнцезащитное покрытие
                </Typography>
                <Typography color="textPrimary">
                  Этот стеклопакет работает по типу теплового зеркала и отражает
                  солнечные лучи. Это снижает затраты на кондиционирование
                  помещения и обеспечивает комфортную температуру в самое жаркое
                  время года.
                </Typography>
              </SectionText>
            </CE.Content>
          </CE.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
