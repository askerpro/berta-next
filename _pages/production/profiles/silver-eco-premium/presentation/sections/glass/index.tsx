import React from 'react';
import GlassIntro, { id, title } from './intro';
import GlassWinter from './winter';
import GlassSummer from './summer';

export default () => {
  return (
    <>
      <GlassIntro />
      <GlassWinter />
      <GlassSummer />
    </>
  );
};

export const details = { id, title };
