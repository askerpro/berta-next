import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography, Hidden } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';

import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

export const title = 'Стеклопакет';
export const id = 'glass';
const videoSrc = '/static/img/presentations/silver-eco/glass/intro.mp4';

const totalDuration = 1;

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  mediaRef = React.createRef<HTMLVideoElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  shouldComponentUpdate(nextProps, nextState: State) {
    const { progress } = this.state;
    if (
      nextState.progress !== progress &&
      nextState.progress >= 0 &&
      nextState.progress <= 1
    ) {
      this.mediaRef.current.play();
    } else {
      this.mediaRef.current.pause();
    }
    return true;
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
          }}
          progress={progress}
          id={id}
        >
          <E.Container>
            <E.MediaContainer>
              <Hidden smDown implementation="css">
                <E.Video
                  style={{
                    zIndex: progress >= 0.5 ? 1 : -1,
                  }}
                  ref={this.mediaRef}
                  src={videoSrc}
                  muted
                  loop
                />
              </Hidden>
              <Hidden smUp implementation="css">
                <E.Video src={videoSrc} muted loop autoPlay />
              </Hidden>
              <E.VideoContainerCover
                style={{
                  opacity: getLocalProgress(0, 0.5, progress, 0, 1) * 0.7,
                }}
              />
            </E.MediaContainer>
            <CE.Content visible>
              <E.ST
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0}
                duration={1.5}
              >
                <Typography variant="h3" color="textPrimary">
                  Мультифункциональный стеклопакет
                </Typography>
                <Typography component="span" color="textPrimary">
                  Berta Thermo Premium изготовлен специально для компании “ОКНА
                  БЕРТА” и по своим потребительским качествам является, пожалуй,
                  одним из лучших продуктов на рынке. Этот стеклопакет среди
                  прочих выделяется высокими тепло\звуко изоляционными
                  свойствами, и солнцезащитными качествами в летние периоды
                  Так-же вы можете выбрать оттенок наружнего стекла, что
                  позволит воплатить ваши идеи по оформлению фасада.
                </Typography>
              </E.ST>
            </CE.Content>
          </E.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
