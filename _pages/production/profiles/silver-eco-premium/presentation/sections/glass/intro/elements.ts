import styled, { css } from 'layout/theme';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import SectionText from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/text';

export const MediaContainer = styled.div`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    overflow: hidden;
  }`}
`;

export const VideoContainerCover = styled.div`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    z-index: 1;
    background-color: #000000;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
   display: none;
  }`}
`;

export const Video = styled.video`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
  }`}
  width: 100%;
  height: auto;
`;

export const Img = styled.img`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
  }`}
  width: 100%;
  height: auto;
`;

export const Section = styled(CE.Section)`
  ${({ progress }) =>
    progress >= 1 &&
    css`
      z-index: 2;
    `}
`;

export const Container = styled(CE.Container)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    justify-content: center;
    max-width: 100%;
    text-align: center;
  }`}
`;

export const ST = styled(SectionText)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
   text-align: center;
  }`}

  width: 100%;
`;
