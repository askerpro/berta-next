import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import SectionText from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/text';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

export const title = 'Профиль';
const id = 'profile';
const imageSrc =
  '/static/animations/presentations/silver-eco/assembly/data.json';

const totalDuration = 2;

interface State {
  animationPlayed: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);

    this.state = {
      animationPlayed: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { animationPlayed, progress } = this.state;
    return (
      <EcoDark>
        <CE.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: 'calc(-100vh)',
          }}
          progress={progress}
          id={id}
        >
          <E.Container>
            <E.Image
              state={progress >= 0.5 ? 1 : 0}
              onAnimationPlayedEnd={() => {
                this.setState({
                  animationPlayed: true,
                });
              }}
              dataSrc={imageSrc}
            />

            <CE.Content visible={animationPlayed}>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0.5}
                duration={2}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  Автоматизированный 4х головочный робот
                </Typography>
                <Typography component="span" color="textPrimary">
                  Такая технология позволяет одновременно сваривать все 4 угла и
                  гарантирует, что окно будет собрано идеально ровно без
                  перекосов. Качественно собранные пластиковые окна являются
                  полностью герметичными, что гарантирует тишину и уют в доме.
                </Typography>
              </SectionText>
            </CE.Content>
          </E.Container>
        </CE.Section>
      </EcoDark>
    );
  }
}

export default Intro;
