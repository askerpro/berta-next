import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography, Hidden } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

const videoSrc2 = '/static/img/presentations/silver-eco/assembly/test.mp4';
const totalDuration = 1.5;

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  mediaRef1 = React.createRef<HTMLVideoElement>();

  mediaRef2 = React.createRef<HTMLVideoElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            marginTop: '-100vh',
          }}
          progress={progress}
        >
          <E.Container>
            <E.MediaContainer>
              <E.Video autoPlay src={videoSrc2} muted loop />
              <E.VideoContainerCover
                style={{
                  opacity: getLocalProgress(-0.2, 0.5, progress, 0.3, 1) * 0.7,
                }}
              />
            </E.MediaContainer>

            <CE.Content visible style={{ maxWidth: '586px' }}>
              <E.ST
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0.5}
                duration={2.5}
              >
                <Typography variant="h3" color="textPrimary">
                  Многоуровневый контроль качества
                </Typography>
                <Typography color="textPrimary">
                  Для определения качества профиля используется ксенотест. Это
                  установка, имитирующая погодные условия: все виды осадков,
                  перепады температур, колебания уровня влажности,
                  ультрафиолетовое излучение и т.д. За две недели образец
                  проходит цикл воздействий, которое пластиковое окно испытывает
                  за весь срок службы.
                </Typography>
              </E.ST>
            </CE.Content>
          </E.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
