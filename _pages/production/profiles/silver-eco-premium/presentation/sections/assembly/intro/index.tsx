import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography, Hidden } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

export const title = 'Производство';
export const id = 'assembly';
const videoSrc1 = '/static/img/presentations/silver-eco/assembly/intro.mp4';
const totalDuration = 1;

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
          }}
          progress={progress}
          id={id}
        >
          <E.Container>
            <E.MediaContainer>
              <E.Video src={videoSrc1} autoPlay muted loop />
              <E.VideoContainerCover
                style={{
                  opacity: getLocalProgress(-0.2, 0.5, progress, 0.3, 1) * 0.7,
                }}
              />
            </E.MediaContainer>

            <CE.Content visible style={{ maxWidth: '600px' }}>
              <E.ST
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0}
                duration={2}
              >
                <Typography variant="h3" color="textPrimary">
                  Высокотехнологичная автоматизированная сборка окон
                </Typography>
              </E.ST>
            </CE.Content>
          </E.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
