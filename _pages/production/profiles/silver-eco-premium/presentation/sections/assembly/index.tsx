import React from 'react';
import Intro, { id, title } from './intro';
import QualityControl from './quality-control';
import More from './more';

export default () => {
  return (
    <>
      <Intro />
      <QualityControl />
      <More />
    </>
  );
};

export const details = { id, title };
