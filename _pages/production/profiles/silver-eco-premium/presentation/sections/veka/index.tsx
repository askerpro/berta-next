import React from 'react';
import Intro, { id, title } from './intro';
import FactoryOverview from './grid';
import Transfer from './transfer';

export default () => {
  return (
    <>
      <Intro />
      <Transfer />
      <FactoryOverview />
    </>
  );
};

export const details = { id, title };
