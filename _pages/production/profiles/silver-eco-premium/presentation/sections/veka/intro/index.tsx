import React from 'react';
import { EcoDark } from 'layout/theme';
import { Typography, Hidden } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import {
  getSectionProgress,
  getLocalProgress,
} from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements/template';
import * as E from './elements';

export const title = 'Завод';
export const id = 'production';
const poster =
  '/static/img/presentations/silver-eco/privolnov/privolnov_veka_msk.jpg';

const totalDuration = 1;

interface State {
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  mediaRef = React.createRef<HTMLVideoElement>();

  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  render() {
    const { progress } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
          }}
          progress={progress}
          id={id}
        >
          <E.Container>
            <E.MediaContainer>
              <E.Img
                src={poster}
                style={{
                  zIndex: progress < 0.5 ? 1 : -1,
                }}
              />
              <E.VideoContainerCover
                style={{
                  opacity: getLocalProgress(0, 0.5, progress, 0, 1) * 0.7,
                }}
              />
            </E.MediaContainer>
            <CE.Content visible style={{ maxWidth: '600px' }}>
              <E.ST
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={0}
                duration={2}
              >
                <Typography variant="h4" color="textPrimary" component="span">
                  Почему пластиковые окна
                  <Typography variant="h3" color="textPrimary">
                    Berta Silver Eco
                  </Typography>
                  соответствуют высочайшим стандартам качества?
                </Typography>
              </E.ST>
            </CE.Content>
          </E.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
