import { Typography, Card, CardContent } from '@material-ui/core';
import React from 'react';
import * as E from './elements';
import CardMediaCarousel from './card-media-carousel';

interface Props {
  label: string;
  images?: string[];
  video?: string;
}

interface State {
  hovered: boolean;
}

export default class extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      hovered: false,
    };
  }

  render() {
    const { label, images, video } = this.props;
    const { hovered } = this.state;
    return (
      <Card
        onMouseEnter={() => {
          this.setState({
            hovered: true,
          });
        }}
        onMouseLeave={() => {
          this.setState({
            hovered: false,
          });
        }}
      >
        <E.CardMedia>
          {video ? (
            <E.Video src={video} autoPlay muted loop />
          ) : (
            <CardMediaCarousel hovered={hovered} images={images} />
          )}
        </E.CardMedia>
        <CardContent>
          <Typography variant="h6">{label}</Typography>
        </CardContent>
      </Card>
    );
  }
}
