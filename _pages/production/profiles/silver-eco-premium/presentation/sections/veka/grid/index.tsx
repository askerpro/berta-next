import React from 'react';
import { EcoDark } from 'layout/theme';
import { Grid } from '@material-ui/core';
import * as CE from '_pages/production/profiles/silver-eco-premium/presentation/sections/elements';
import GridCard from './elements/grid-card';
import * as E from './elements';

const images1 = [
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_0.jpg',
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_1.jpg',
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_2.jpg',
];

const images2 = [
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_3.jpg',
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_4.jpg',
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_5.jpg',
];

const images3 = [
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_6.jpg',
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_7.jpg',
  '/static/img/presentations/silver-eco/privolnov/veka_zavod_8.jpg',
];

const video =
  '/static/img/presentations/silver-eco/privolnov/veka_production.mp4';

export default () => (
  <EcoDark>
    <E.Section>
      <E.Container>
        <Grid container spacing={3}>
          <Grid item md={6}>
            <GridCard
              images={images1}
              label="Veka - является крупнейшим производителем профиля для пластиковых окон в мире"
            />
          </Grid>
          <Grid item md={6}>
            <GridCard
              images={images2}
              label="Завод работает на самом современном оборудовании, что непосредственно влияет на качество продукции."
            />
          </Grid>
          <Grid item md={6}>
            <GridCard
              video={video}
              label={`При производстве пластика для окон "Silver Eco" в пластик добавляются компоненты на основе серебра, что придает ему антибактериальные свойства.`}
            />
          </Grid>
          <Grid item md={6}>
            <GridCard
              images={images3}
              label="Эксперт по качеству побывал на всех производственных узлах и лично убедился, что качество соответствует заявленному."
            />
          </Grid>
        </Grid>
      </E.Container>
    </E.Section>
  </EcoDark>
);
