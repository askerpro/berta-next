import styled from 'layout/theme';

interface IImg {
  src: string;
}

export const Img = styled.div<IImg>`
  height: 100%;
  width: 100%;
  background-image: url(${({ src }) => src});
  background-size: cover;
  background-position: 50%;
`;

export const arrowsContainer = styled.div`
  position: absolute;
  left: 20px;
  top: 0;
  right: 20px;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
