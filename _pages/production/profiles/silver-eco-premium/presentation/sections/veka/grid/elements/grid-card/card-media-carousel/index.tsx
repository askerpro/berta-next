import {
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  CarouselProvider,
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import React from 'react';
import * as E from './elements';

interface Props {
  images: string[];
  hovered: boolean;
}

export default ({ images, hovered }: Props) => (
  <CarouselProvider
    naturalSlideWidth={162}
    naturalSlideHeight={100}
    totalSlides={images.length}
    visibleSlides={1}
    isPlaying={hovered}
    interval={1500}
  >
    <Slider>
      {images.map((src, i) => (
        <Slide index={i} key={i}>
          <E.Img src={src} />
        </Slide>
      ))}
    </Slider>
    <E.arrowsContainer>
      <ButtonBack>
        <KeyboardArrowLeft />
      </ButtonBack>
      <ButtonNext>
        <KeyboardArrowRight />
      </ButtonNext>
    </E.arrowsContainer>
  </CarouselProvider>
);
