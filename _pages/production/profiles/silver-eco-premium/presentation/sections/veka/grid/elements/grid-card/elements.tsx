import styled from 'layout/theme';

import { CardMedia as CardMediaT } from '@material-ui/core';

interface IImg {
  src: string;
}

export const CardMedia = styled(CardMediaT)`
  position: relative;
`;

export const Img = styled.div<IImg>`
  height: 100%;
  width: 100%;
  background-image: url(${({ src }) => src});
  background-size: cover;
  background-position: 50%;
`;

export const Video = styled.video`
  width: 100%;
`;

export const arrowsContainer = styled.div`
  position: absolute;
  left: 20px;
  top: 0;
  right: 20px;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export default {};
