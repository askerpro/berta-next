import { Typography, Card, CardMedia, CardContent } from '@material-ui/core';
import {
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  CarouselProvider,
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import React from 'react';
import * as E from './elements';

interface Props {
  label: string;
  images: string[];
}

interface State {
  hovered: boolean;
}

export default class extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      hovered: false,
    };
  }

  render() {
    const { label, images } = this.props;
    const { hovered } = this.state;
    return (
      <Card
        onMouseEnter={() => {
          this.setState({
            hovered: true,
          });
        }}
        onMouseLeave={() => {
          this.setState({
            hovered: false,
          });
        }}
      >
        <E.CardMedia>
          <CarouselProvider
            naturalSlideWidth={162}
            naturalSlideHeight={100}
            totalSlides={images.length}
            visibleSlides={1}
            isPlaying={hovered}
            interval={1500}
          >
            <Slider>
              {images.map((src, i) => (
                <Slide index={i} key={i}>
                  <E.Img src={src} />
                </Slide>
              ))}
            </Slider>
            <E.arrowsContainer>
              <ButtonBack>
                <KeyboardArrowLeft />
              </ButtonBack>
              <ButtonNext>
                <KeyboardArrowRight />
              </ButtonNext>
            </E.arrowsContainer>
          </CarouselProvider>
        </E.CardMedia>
        <CardContent>
          <Typography variant="h6">{label}</Typography>
        </CardContent>
      </Card>
    );
  }
}
