import React from 'react';
import { Heart, Ag, EasyCare } from 'layout/elements/Icon';
import { Settings } from '@material-ui/icons';
import { Typography, Hidden } from '@material-ui/core';
import { EcoDark } from 'layout/theme';
import * as SE from '../elements';
import * as E from './elements';
import Preloader from './elements/preloader';
import { getSectionProgress } from '../elements/template';
import SectionText from '../elements/text';

const content = [
  {
    icon: EasyCare,
    text: 'Экологически чистые',
  },
  {
    icon: Heart,
    text: 'Безопасные для здоровья',
  },
  {
    icon: Ag,
    text: 'Антибактериальные свойства',
  },
  {
    icon: props => <Settings {...props} />,
    text: 'Внушительные характеристики',
  },
];

export const title = 'Характеристики';
const id = 'params';
const imageSrc =
  '/static/animations/presentations/silver-eco/profile/intro/data.json';

const totalDuration = 1;

interface State {
  animationPlayed?: boolean;
  animationState: number;
  animationReady: boolean;
  preloaderAnimated: boolean;
  progress: number;
}

class Intro extends React.Component<{}, State> {
  ref = React.createRef<HTMLDivElement>();

  constructor(props) {
    super(props);
    this.state = {
      animationPlayed: false,
      animationState: 0,
      animationReady: false,
      preloaderAnimated: false,
      progress: 0,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.updateSectionProgress);
    this.updateSectionProgress();
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.updateSectionProgress);
  }

  updateSectionProgress = () => {
    const progress = getSectionProgress(this.ref);
    this.setState({
      progress,
    });
  };

  IconContent = () => {
    const { animationPlayed } = this.state;
    return (
      <E.IconContentWrapper>
        {content.map((El, i) => (
          <E.IconContent index={i} key={i} visible={animationPlayed}>
            <E.Icon as={El.icon} />
            <E.IconText>
              <Typography color="textPrimary" variant="subtitle1">
                {El.text}
              </Typography>
            </E.IconText>
          </E.IconContent>
        ))}
      </E.IconContentWrapper>
    );
  };

  render() {
    const {
      animationPlayed,
      animationState,
      animationReady,
      preloaderAnimated,
      progress,
    } = this.state;
    return (
      <EcoDark>
        <E.Section
          ref={this.ref}
          style={{
            height: `calc(${totalDuration} * 900px + 100vh - 60px)`,
            zIndex: progress > 0 && 1,
          }}
          progress={progress}
          id={id}
        >
          <Preloader
            active={!animationReady || !preloaderAnimated}
            onPlayed={() => {
              console.log('preloader animated: ');
              this.setState({
                preloaderAnimated: true,
              });
            }}
          />
          <SE.Container>
            <E.Image
              state={animationReady && preloaderAnimated ? 1 : 0}
              onReady={() => {
                console.log('animation ready: ');
                this.setState({
                  animationReady: true,
                });
              }}
              onAnimationPlayedEnd={() => {
                this.setState({
                  animationPlayed: true,
                });
              }}
              dataSrc={imageSrc}
            >
              <Hidden smDown>{this.IconContent()}</Hidden>
            </E.Image>
            <Hidden smUp>{this.IconContent()}</Hidden>
            <SE.Content visible={animationPlayed}>
              <SectionText
                sectionDuration={totalDuration}
                sectionProgress={progress}
                position={-0.5}
                duration={1.5}
              >
                <Typography variant="h3" color="textPrimary" gutterBottom>
                  BERTA SILVER
                  <Typography variant="inherit" color="textSecondary">
                    {` ECO`}
                  </Typography>
                </Typography>
                <Typography color="textPrimary">
                  это инновационный, экологически чистый и безопасный для
                  здоровья продукт с содержанием серебра по уникальной
                  запатентованной технологии Silver Eco. Единственная в России
                  оконная система с содержанием серебра, которая к тому же
                  обладает антибактериальным и антигрибковым эффектом.
                </Typography>
              </SectionText>
            </SE.Content>
          </SE.Container>
        </E.Section>
      </EcoDark>
    );
  }
}

export default Intro;
