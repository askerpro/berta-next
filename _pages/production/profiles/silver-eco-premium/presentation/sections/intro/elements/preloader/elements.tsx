import styled, { css } from 'layout/theme';

interface IWrapper {
  active: boolean;
  visible: boolean;
}

export const Wrapper = styled.div<IWrapper>`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 2;
  background-color: #000;
  transition: 2s opacity;
  ${({ active }) =>
    !active &&
    css`
      opacity: 0;
    `};

  ${({ visible }) =>
    !visible &&
    css`
      display: none;
    `};
`;

export const LogoWrapper = styled.div`
  width: 275px;
  height: auto;
  padding: 15px 30px;
  z-index: 11;
`;

export const BackgroundWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  overflow: hidden;
`;
