import React from 'react';
import * as E from './elements';
import Background from './background';
import Logo from './logo';

interface Props {
  active: boolean;
  onPlayed?(): void;
}

interface State {
  visible: boolean;
  logoAnimationPlayed: boolean;
}

export default class extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      logoAnimationPlayed: false,
    };
  }

  shouldComponentUpdate(nextProps: Props) {
    const { active } = this.props;

    if (active !== nextProps.active && !nextProps.active) {
      setTimeout(() => {
        this.setState({
          visible: false,
        });
      }, 2000);
    }
    return true;
  }

  render() {
    const { active, onPlayed } = this.props;
    const { visible, logoAnimationPlayed } = this.state;
    return (
      <E.Wrapper active={active} visible={visible}>
        <E.BackgroundWrapper>
          <Background state={logoAnimationPlayed ? 1 : 0} onPlayed={onPlayed} />
        </E.BackgroundWrapper>
        <E.LogoWrapper>
          <Logo
            state={active ? 0 : 1}
            onPlayed={() => {
              this.setState({ logoAnimationPlayed: true });
            }}
          />
        </E.LogoWrapper>
      </E.Wrapper>
    );
  }
}
