import styled, { css } from 'layout/theme';
import * as SE from '../../elements';

export const Section = styled(SE.Section)`
  z-index: 3;
`;

export const vars = {
  transitionDuration: 0.7,
};

interface IIconContent {
  index: number;
  visible: boolean;
}

export const Image = styled(SE.Image)`
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    margin-left: -22px;
    margin-top: -43px;
  }`}
`;

export const IconContent = styled.div<IIconContent>`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    transition-duration: 0s;
    transition-delay: ${({ index }) => index * 0.1}s;
    opacity: ${({ visible }) => (visible ? 1 : 0)};
    text-align: right;
    max-width: 150px;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    opacity: 1;
    max-width: 50%;
    margin-bottom: 30px;
  }`}
`;

export const IconContentWrapper = styled.div`
  display: flex;

  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    height: 100%;
    padding: 47px 0;
    right: 76%;
    top: 0;
    z-index: 1;
    position: absolute;
    flex-direction: column;
    justify-content: space-evenly;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    flex-wrap: wrap;
    margin-top: -50px;
  }`}
`;

export const Icon = styled.img`
  width: auto;
  height: 45px;
`;

export const IconText = styled.div`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    text-align: right;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    
  }`}
`;
