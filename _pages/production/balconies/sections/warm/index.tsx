import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Light } from 'layout/theme';
import SectionImage from './img/warm.jpg';

const caption = 'Остекление балконов';
const title = 'Теплое остекление балкона';
const shortTitle = 'Теплое';
const id = 'warm';
const actionText = 'Заказать теплое остекление';
const desc =
  'Установка пластиковых окон на лоджии, ее утепление и отделка — это возможность получить полноценную жилую комнату по цене балконного блока. Технология подразумевает установку ПВХ-профиля со стеклопакетами. Стоимость такого решения пропорциональна качеству. Даже зимой на уютном балконе вы сможете принимать гостей, отдыхать, играть с детьми, работать за компьютером или устраиваться поудобнее с книгой.';

const view = () => (
  <Light>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={caption}
            vertical
            reversed
          />
        </Section.Container>
      </Section.Wrapper>
    </>
  </Light>
);

export default {
  id,
  title: shortTitle,
  view,
};
