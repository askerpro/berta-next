import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { White } from 'layout/theme';
import SectionImage from './img/panoramic.jpg';

const caption = 'Остекление балконов';
const title = 'Панорамное остекление балкона';
const shortTitle = 'Панорамное';
const id = 'panoramic';
const actionText = 'Заказать панорамное остекление';
const desc =
  'Установите панорамные окна на балкон, чтобы подчеркнуть ночную красоту не беспокоясь о потере тепла, Окна Berta гарантируют максимальный уровень энергоэффективности. Если французские окна выходят на солнечную сторону, то световой поток легко регулировать при помощи специальных светоотражающих стеклопакетов, жалюзи и рулонных штор';

const view = () => (
  <White>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={caption}
            vertical
          />
        </Section.Container>
      </Section.Wrapper>
    </>
  </White>
);

export default {
  id,
  title: shortTitle,
  view,
};
