import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Dark } from 'layout/theme';
import SectionImage from './img/block.jpg';

const caption = 'Остекление балконов';
const title = 'Балконный блок';
const shortTitle = 'Балконный блок';
const id = 'block';
const actionText = 'Заказать балконный блок';
const desc =
  'Это конструкция, отделяющая комнату от балкона состоит из балконной двери и окон, в зависимости от типа и серии дома. Чаще всего балконные двери устанавливаются вместе с окном в едином балконном блоке. ';

const view = () => (
  <Dark>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={caption}
            vertical
          />
        </Section.Container>
      </Section.Wrapper>
    </>
  </Dark>
);

export default {
  id,
  title: shortTitle,
  view,
};
