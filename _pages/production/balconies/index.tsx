import React from 'react';
import { Section, PromoCard, SectionCard } from 'layout/elements';
import Page, { IHead } from 'lib/Page';
import PriceSection, { ProductType } from 'components/PriceSection';
import BlockSection from './sections/block';
import WarmSection from './sections/warm';
import PanoramicSection from './sections/panoramic';
import promoImg from './img/window-balcony-2.jpg';

const title = 'Остекление балконов';
const subtitle = 'Балконные блоки, панорамное остекление, теплое остекление';
const id = 'header';
const actionText = 'Заказать остекление балкона';
const desc =
  'Мы предлагаем своим клиентам большой спектр работ по остеклению балкона или лоджий с применением холодного и теплого метода. Монтаж конструкций осуществляется опытными мастерами, что гарантирует надёжность и продолжительный срок эксплуатации изделий.';
const url = '/production/balconies';
const view = () => (
  <>
    <Section.Wrapper id={id}>
      <Section.Container>
        <SectionCard
          title={title}
          desc={desc}
          image={promoImg}
          actionText={actionText}
          caption={subtitle}
          vertical
          reversed
          h1
        />
      </Section.Container>
    </Section.Wrapper>
    <PriceSection productType={ProductType.BALCONIES} />
  </>
);

const HeaderSection = {
  title,
  id,
  view,
};

const head: IHead = {
  title,
  description: subtitle,
};

export default () => (
  <Page
    sections={[HeaderSection, PanoramicSection, WarmSection, BlockSection]}
    head={head}
  />
);

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={promoImg} />
);
