import React from 'react';
import { Section, PromoCard } from 'layout/elements';
import { White } from 'layout/theme';
import Page, { IHead } from 'lib/Page';
import Container from '@material-ui/core/Container';
import promoImg from './sections/outside/img/outside.jpg';
import sectionImg from './img/header.jpg';
import SectionOutside from './sections/outside';
import SectionIntside from './sections/inside';
import SectionBalcony from './sections/balcony';

const title = 'Пластиковые двери';
const subtitle = 'Виды пластиковых дверей и цены.';
const id = '';
const url = '/production/doors';

const view = () => (
  <>
    <White>
      <Section.Wrapper>
        <Section.Container>
          <Section.HeaderT title={title} subtitle={subtitle} h1 center />
          <Container maxWidth="md">
            <Section.Img src={sectionImg} />
          </Container>
        </Section.Container>
      </Section.Wrapper>
    </White>
  </>
);

const HeaderSection = {
  title,
  id,
  view,
};

const PageSections = [
  HeaderSection,
  SectionOutside,
  SectionIntside,
  SectionBalcony,
];

const head: IHead = {
  title,
  description: subtitle,
};
export default () => <Page sections={PageSections} head={head} />;

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={promoImg} />
);
