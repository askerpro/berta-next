import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Dark } from 'layout/theme';
import PriceSection from 'components/PriceSection';
import { ProductType } from 'data/products';
import SectionImage from './img/outside.jpg';

const title = 'Входные пластиковые двери';
const shortTitle = 'Входные';
const id = 'doors_outside';
const desc =
  'Двери – лицо дома. Мы готовы сделать их особенными, удобными, функциональными, цветными, распашными, сложной конструкции и геометрии, безопасными от взлома. Входные группы обладают высоким уровнем противовзломности. Абсолютно герметичны, прекрасно сохраняют тепло и спасают от сквозняков. Дополнительные уплотнители позволят Вам наслаждаться тишиной в доме. Можно комплектовать различными опциями: доводчики, раскладка всех видов, ламинация поверхностей под любой цвет и фактуру.';

interface IView {
  promo?: boolean;
}
const View = ({ promo = false }: IView) => (
  <Dark>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText="Заказать входную дверь"
            caption="Пластиковые двери"
            vertical
            promo={promo}
          />
        </Section.Container>
      </Section.Wrapper>
      <PriceSection productType={ProductType.DOORS_OUTSIDE} />
    </>
  </Dark>
);

export const Promo = () => <View promo />;

export default {
  id,
  title: shortTitle,
  view: View,
};
