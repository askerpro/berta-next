import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Light } from 'layout/theme';
import PriceSection from 'components/PriceSection';
import { ProductType } from 'data/products';
import SectionImage from './img/balcony.jpg';

const title = 'Балконные двери';
const shortTitle = 'Балконные';
const id = 'doors_balcony';
const desc =
  'Если на Вашем балконе используется холодное остекление, балконная дверь должна быть более теплой. И, наоборот — при теплом остеклении балкона или лоджии, дверь может быть более «холодной». Можно сделать целиком стеклянной.';

const view = () => (
  <Light>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText="Заказать балконный блок"
            caption="Пластиковые двери"
            vertical
          />
        </Section.Container>
      </Section.Wrapper>
      <PriceSection productType={ProductType.BLOCKS} />
    </>
  </Light>
);

export default {
  id,
  title: shortTitle,
  view,
};
