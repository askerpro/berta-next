import React from 'react';
import { SectionCard, Section } from 'layout/elements';
import { Grey } from 'layout/theme';
import PriceSection from 'components/PriceSection';
import { ProductType } from 'data/products';
import SectionImage from './img/inside.jpg';

const title = 'Межкомнатные пластиковые двери';
const shortTitle = 'Межкомнатные';
const id = 'doors_intside';
const desc =
  'Просты в уходе: поверхность гладкая и устойчивая к загрязнениям. Не деформируются, не впитывают влагу, можно устанавливать в ванную. Дополнительно можно ламинировать и гармонично вписать в интерьер';

const view = () => (
  <Grey>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText="Заказать межкомнатную дверь"
            caption="Пластиковые двери"
            vertical
            reversed
          />
        </Section.Container>
      </Section.Wrapper>
      <PriceSection productType={ProductType.DOORS_INSIDE} />
    </>
  </Grey>
);

export default {
  id,
  title: shortTitle,
  view,
};
