import React from 'react';
import { SectionCard } from 'layout/elements';
import SectionImage from './img/cottage.jpg';

const title = 'В частных домах';
const desc =
  'Любуйтесь природой не выходя из дома, доверив Ваше жилище нашим технологиям безопасности.	В стеклопакетах используются специальные закаленные противоударные стекла. Высокий уровень безопасности обуславливается используемыми материалами и точными расчетами при проектировании конструкции, в которых учитываются ветровые нагрузки, площадь остекления и прочие параметры.';
const caption = 'Панорамные окна';
const price = ' ';
export default () => (
  <SectionCard
    title={title}
    desc={desc}
    image={SectionImage}
    actionDescription={price}
    caption={caption}
    promo
  />
);
