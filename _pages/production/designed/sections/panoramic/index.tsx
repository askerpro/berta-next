import React from 'react';
import Grid from '@material-ui/core/Grid';
import { SectionCard, Section, PromoCard } from 'layout/elements';
import { Grey } from 'layout/theme';
import SectionImage from './img/main.jpg';
import Variant1 from './variant1';
import Variant2 from './variant2';

const title = 'Панорамные окна';
const subtitle = 'От пола до потолка (французские)';
const shortTitle = 'Панорамные';
const actionText = 'Заказать панорамное окно';
const id = 'panoramic';
const url = `/production/designed#${id}`;
const desc =
  'У вас дизайнерский интерьер и особый архитектурный взгляд? Поддержим любые идеи и воплотим их в окнах. Пусть это будет витраж в виде павлина для привлечения достатка и богатства или декоративная раскладка в классическом стиле.';

const variants = [Variant1, Variant2];

const view = () => (
  <Grey>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText={actionText}
            caption={subtitle}
            vertical
          />
        </Section.Container>
      </Section.Wrapper>
      <Section.Wrapper>
        <Section.Container>
          <Section.HeaderT
            title="Виды панорамных окон"
            subtitle="Панорамные окна для квартир и частных домов"
          />
          <Grid container spacing={5}>
            {variants.map((Variant, i) => (
              <Grid item key={i} xs={12} sm={6}>
                <Variant />
              </Grid>
            ))}
          </Grid>
        </Section.Container>
      </Section.Wrapper>
    </>
  </Grey>
);

export default {
  id,
  title: shortTitle,
  view,
};

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={SectionImage} />
);
