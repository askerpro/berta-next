import React from 'react';
import { SectionCard } from 'layout/elements';
import SectionImage from './img/apartment.jpg';

const title = 'В квартирах';
const desc =
  'Установите панорамные окна, чтобы подчеркнуть ночную красоту не беспокоясь о потере тепла, Окна Berta гарантируют максимальный уровень энергоэффективности. Если французские окна выходят на солнечную сторону, то световой поток легко регулировать при помощи специальных светоотражающих пленок на стеклопакетах, жалюзи и рулонных штор.';
const caption = 'Панорамные окна';
const price = ' ';
export default () => (
  <SectionCard
    title={title}
    desc={desc}
    image={SectionImage}
    actionDescription={price}
    caption={caption}
    promo
  />
);
