import React from 'react';
import Grid from '@material-ui/core/Grid';
import { SectionCard, Section } from 'layout/elements';
import { Light } from 'layout/theme';
import SectionImage from './img/bg.jpg';
import Variant1 from './variants/variant1';
import Variant2 from './variants/variant2';

const title = 'Фигурные пластиковые окна';
const shortTitle = 'Фигурные';
const id = 'designed_windows';
const desc =
  'У вас дизайнерский интерьер и особый архитектурный взгляд? Поддержим любые идеи и воплотим их в окнах. Пусть это будет витраж в виде павлина для привлечения достатка и богатства или декоративная раскладка в классическом стиле.';

const variants = [Variant1, Variant2];

const view = () => (
  <Light>
    <>
      <Section.Wrapper id={id}>
        <Section.Container>
          <SectionCard
            title={title}
            desc={desc}
            image={SectionImage}
            actionText="Заказать фигурное окно"
            caption="Дизайнерские пластиковые окна"
            vertical
            h1
          />
        </Section.Container>
      </Section.Wrapper>
      <Section.Wrapper>
        <Section.Container>
          <Section.HeaderT
            title="Виды фигурных окон"
            subtitle="Возможных варианты и их изображения"
          />
          <Grid container spacing={5}>
            {variants.map((Variant, i) => (
              <Grid item key={i} xs={12} sm={6}>
                <Variant />
              </Grid>
            ))}
          </Grid>
        </Section.Container>
      </Section.Wrapper>
    </>
  </Light>
);

export default {
  id,
  title: shortTitle,
  view,
};
