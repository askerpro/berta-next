import React from 'react';
import { SectionCard } from 'layout/elements';
import SectionImage from './img/1.jpg';

const title = 'Треугольные модели';
const desc =
  'Конструкции-треугольники встречаются в проемах мансардных комнат, украшают фронтоны. В любую комнату они привносят особенную атмосферу. Впечатление от окна зависит от архитектуры объекта.';

export default () => (
  <SectionCard
    title={title}
    desc={desc}
    image={SectionImage}
    actionDescription=" "
    caption="Дизайнерские пластиковые окна"
    promo
  />
);
