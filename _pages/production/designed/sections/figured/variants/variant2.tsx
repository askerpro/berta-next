import React from 'react';
import { SectionCard } from 'layout/elements';
import SectionImage from './img/2.jpg';

const title = 'Трапециевидные модели';
const desc =
  'Идеальное решение для обустройства пространства под крышей. Окна-трапеции помогают наиболее эффективно эксплуатировать площадь проема. Также они пропускают максимум света.';

export default () => (
  <SectionCard
    title={title}
    desc={desc}
    image={SectionImage}
    actionDescription=" "
    caption="Дизайнерские пластиковые окна"
    promo
  />
);
