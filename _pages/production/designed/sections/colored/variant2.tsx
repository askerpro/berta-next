import React from 'react';
import { SectionCard } from 'layout/elements';
import SectionImage from './img/wood.jpg';

const title = 'Имитация текстуры дерева';
const desc =
  'При нанесении плёнки не имеет значения первичный цвет профиля. Он может быть классическим белым или коричневым «в массе». Второй вариант прекрасно смотрится заламинированным под натуральное дерево. На стыках не будет виден изначальный оттенок пластика';
const caption = 'Ламинированные окна';
const price = ' ';
export default () => (
  <SectionCard
    title={title}
    desc={desc}
    image={SectionImage}
    actionDescription={price}
    caption={caption}
    promo
  />
);
