import React from 'react';
import { SectionCard } from 'layout/elements';
import SectionImage from './img/inside.jpg';

const title = 'Внутренняя и внешняя ламинация';
const desc =
  'Внутренняя ламинация позволяет гармонично вписать окна в любой интерьер.	Ламинированные окна гармонично сочетаются с мебелью, цветовой гаммой интерьера. Плёнка продлевает срок эксплуатации изделия, выступает дополнительным покрытием, защищающим поверхность от выгорания на солнце и механических повреждений.';
const caption = 'Ламинированные окна';
const price = ' ';
export default () => (
  <SectionCard
    title={title}
    desc={desc}
    image={SectionImage}
    actionDescription={price}
    caption={caption}
    promo
  />
);
