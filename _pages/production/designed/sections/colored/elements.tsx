import styled from 'layout/theme';
import colorPickerBg from './img/colors/seasons.jpg';

export const ColorPicker = styled.div`
  display: flex;
  justify-content: space-between;
  position: absolute;
  width: 100%;
  bottom: 0;
  flex-shrink: 0;
  @media (max-width: 768px) {
    bottom: unset;
    top: 0;
    flex-wrap: wrap;
    position: relative;
  }
`;
export const ColorPickerImage = styled.div<{
  active: boolean;
  img: string;
}>`
    height: 100%;
    width: 100%;
    display: ${({ active }) => (active ? 'block' : 'none')};
    background-image: url("${({ img }) => img}");
    background-size: cover;
    background-repeat: no-repeat;
    @media(max-width: 768px){
        position: absolute;
        top: 0;
    }
`;
export const ColorPickerImageWrapper = styled.div`
  :after {
    background-image: url(${colorPickerBg});
    content: '';
    position: absolute;
    height: 356px;
    width: 360px;
    z-index: 1;
    bottom: 0;
    left: 0;
    background-repeat: no-repeat;
    background-size: cover;
  }
  position: relative;
  width: 72%;
  @media (max-width: 768px) {
    width: 100%;
    padding-top: 62%;
    :after {
      display: none;
    }
  }
`;

export const Picker = styled.div`
  display: block;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  flex-direction: column;
  align-items: center;
  flex-grow: 1;
  @media (max-width: 768px) {
    flex-direction: row;
  }
`;
export const PickerItem = styled.div<{
  icon: string;
  active: boolean;
  color: string;
}>`
    margin: 15px;
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-image: url("${({ icon }) => icon}");
    background-repeat: repeat-x;
    border: 1px solid #cacaca;
    position: relative;
    color: ${({ color }) => color};
    cursor: pointer;
    :after{
        display: ${({ active }) => (active ? 'block' : 'none')};
        content: '';
        position: absolute;
        width: calc(100% + 16px);
        height: calc(100% + 12px);
        left: -8px;
        top: -6px;
        border: 1px solid #cacaca;
    }
`;
