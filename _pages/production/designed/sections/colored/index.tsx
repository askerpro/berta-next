/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import React from 'react';
import { Section, Button, PromoCard } from 'layout/elements';
import Grid from '@material-ui/core/Grid';
import * as Card from 'layout/elements/SectionCard/elements';
import Typography from '@material-ui/core/Typography';
import * as E from './elements';
import Variant1 from './variant1';
import Variant2 from './variant2';
import promoImg from './img/header.jpg';

const label = 'Цветные пластиковые окна';
const title = 'Ламинация пвх окон и дверей';
const subtitle =
  'Подчеркните внешний вид вашего фасада и внутренний дизайн дома.';
const shortTitle = 'Цветные';
const desc =
  'Хотите, чтобы окна подходили к интерьеру комнаты и гармонировали с фасадом дома? Или вам просто по душе вид деревянных окон? Подчеркните внешний вид вашего фасада и внутренний дизайн дома ламинированием окон и дверей.';
const id = 'colored';
const colorsData = [
  {
    name: 'Agate Grey',
    alias: 'Агатовый серый',
    color: '#000',
  },
  {
    name: 'Anthracite Grey 167',
    alias: 'Антрацитово-серый',
    color: '#fff',
  },
  {
    name: 'Dark Oak FL-F1',
    alias: 'Темный дуб',
    color: '#fff',
  },
  {
    name: 'Golden Oak',
    alias: 'Золотой дуб',
    color: '#fff',
  },
  {
    name: 'Light Oak',
    alias: 'Светлый дуб',
    color: '#fff',
  },
  {
    name: 'Mahogany',
    alias: 'Махагон',
    color: '#fff',
  },
  {
    name: 'Rustic Oak 1',
    alias: 'Рустикальный дуб',
    color: '#fff',
  },
];
const url = `/production/designed#${id}`;
const imagePath = './img/colors/';

class ColorPicker extends React.Component<
  {},
  {
    activeColor: number;
  }
> {
  constructor(props) {
    super(props);

    this.state = {
      activeColor: 0,
    };
  }

  activateColor = (i: number) => {
    this.setState({
      activeColor: i,
    });
  };

  render() {
    const { activeColor } = this.state;
    return (
      <E.ColorPicker>
        <E.ColorPickerImageWrapper>
          {colorsData.map((data, i) => (
            <E.ColorPickerImage
              active={activeColor === i}
              img={require(`${imagePath}${data.name}.jpg`)}
              key={i}
            />
          ))}
        </E.ColorPickerImageWrapper>
        <E.Picker>
          {colorsData.map((data, i) => (
            <E.PickerItem
              title={data.alias}
              active={activeColor === i}
              onClick={() => {
                this.activateColor(i);
              }}
              icon={require(`${imagePath}icons/${data.name}.jpg`)}
              color={data.color}
              key={i}
            />
          ))}
        </E.Picker>
      </E.ColorPicker>
    );
  }
}

const variants = [Variant1, Variant2];

const view = () => (
  <>
    <Section.Wrapper id={id}>
      <Section.Container>
        <Card.Card vertical>
          <Card.CardContent>
            <Typography
              gutterBottom
              variant="overline"
              component="p"
              color="textSecondary"
            >
              {label}
            </Typography>
            <Typography gutterBottom variant="h3" component="h2">
              {title}
            </Typography>
            <Typography variant="body1" color="textSecondary" component="p">
              {desc}
            </Typography>
            <Card.CardActions display>
              <Button
                href="/services/order"
                variant="outlined"
                size="large"
                fullWidth
              >
                Заказать ламинированное окно
              </Button>
            </Card.CardActions>
          </Card.CardContent>
          <Card.CardMedia>
            <ColorPicker />
          </Card.CardMedia>
        </Card.Card>
      </Section.Container>
    </Section.Wrapper>
    <Section.Wrapper>
      <Section.Container>
        <Section.HeaderT
          title="Разновидности декора"
          subtitle=" Виды и типы ламинирования окон и дверей"
        />
        <Grid container spacing={5}>
          {variants.map((Variant, i) => (
            <Grid item key={i} xs={12} sm={6}>
              <Variant />
            </Grid>
          ))}
        </Grid>
      </Section.Container>
    </Section.Wrapper>
  </>
);

export default {
  id,
  title: shortTitle,
  view,
};

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={promoImg} />
);
