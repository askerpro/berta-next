import React from 'react';
import Page, { IHead } from 'lib/Page';
import { PromoCard } from 'layout/elements';
import SectionIntro from './sections/figured';
import SectionPanoramic from './sections/panoramic';
import SectionColored from './sections/colored';
import promoImg from './img/promo.jpg';

const title = 'Фигурные окна';
const subtitle = 'Фигурные пластиковые окна и двери';
const url = '/production/designed';
export const PageSections = [SectionIntro, SectionPanoramic, SectionColored];

const head: IHead = {
  title,
  description: subtitle,
};

export default () => <Page sections={PageSections} head={head} />;

export const Promo = () => (
  <PromoCard title={title} desc={subtitle} href={url} image={promoImg} />
);
