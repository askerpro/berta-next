import React from 'react';
import { Section, Button } from 'layout/elements';
import { UnderCnst } from 'layout/elements/img';
import PageInfo from 'lib/PageInfo';

const title = 'Продукция';
const desc = 'Продукция и комплектующие';

export default () => (
  <>
    <PageInfo title={title} description={desc} />
    <Section.Wrapper>
      <Section.Container>
        <Section.HeaderT title={title} subtitle={desc} />
        <Section.Body>
          <UnderCnst />
          <Button>ПОДВЕРДИТЬ</Button>
        </Section.Body>
      </Section.Container>
    </Section.Wrapper>
  </>
);
