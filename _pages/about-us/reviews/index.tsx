import React from 'react';
import Page from 'lib/Page';
import ReviewsSection from 'components/ReviewsSection';
import Layout from 'layout';
import { Section } from 'layout/elements';
import { White } from 'layout/theme';
import { IReview } from 'data/reviews';

import ReviewCard from 'components/ReviewsSection/ReviewCard';
import { Grid } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import * as E from './elements';

const pageTitle = 'Отзывы';
const desc = 'Настоящие отзывыо компании Окна Берта';

const url = '/api/reviews';
interface IReviewSection {
  title: string;
}
interface State {
  reviews: IReview[];
  regions: string[];
  region: string;
}
const date = new Date();

class Reviews extends React.Component<{}, State> {
  constructor(props) {
    super(props);
    this.state = {
      reviews: new Array<IReview>(),
      regions: [],
      region: '',
    };
  }

  componentDidMount() {
    fetch(url)
      .then(result => result.json())
      .then((Freviews: IReview[]) => {
        const regions: string[] = [];
        Freviews.forEach((review, i) => {
          if (regions.indexOf(review.region) === -1) {
            regions.push(review.region);
          }
        });
        this.setState({
          regions,
          reviews: Freviews.sort((review1, review2) => {
            const date1 = new Date(review1.date);
            const date2 = new Date(review2.date);
            return date1.getMilliseconds() >= date2.getMilliseconds() ? 1 : -1;
          }),
        });
      });
  }

  render() {
    const { reviews, regions, region } = this.state;
    return (
      <>
        <Page
          head={{
            title: pageTitle,
            description: desc,
          }}
        >
          <Section.Wrapper>
            <Section.Container>
              <Section.HeaderT
                h1
                title="Отзывы"
                subtitle={`Обновлено ${date.getDate() - 1}.${date.getMonth() +
                  1}.${date.getFullYear()}г.`}
              />
              <Section.Body>
                <E.SelectWrapper>
                  <Grid container spacing={2}>
                    <Grid item md={6} xs={12}>
                      <FormControl fullWidth>
                        <Select
                          value={region}
                          onChange={(
                            event: React.ChangeEvent<{ value: unknown }>,
                          ) => {
                            this.setState({
                              region: event.target.value as string,
                            });
                          }}
                          displayEmpty
                        >
                          <MenuItem value="">Все регионы</MenuItem>
                          {regions.map(cregion => (
                            <MenuItem value={cregion} key={cregion}>
                              {cregion}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                </E.SelectWrapper>
                <Grid container spacing={2}>
                  {reviews
                    .filter((review, i) => {
                      return region === review.region || region === '';
                    })
                    .map((review, i) => (
                      <Grid item md={4} xs={12} key={review.url}>
                        <ReviewCard
                          mediaSrc={review.url}
                          region={review.region}
                          location={review.location}
                          date={review.date}
                          preview={review.preview}
                        />
                      </Grid>
                    ))}
                </Grid>
              </Section.Body>
            </Section.Container>
          </Section.Wrapper>
        </Page>
      </>
    );
  }
}

export default Reviews;
