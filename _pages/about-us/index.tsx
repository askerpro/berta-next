import React from 'react';
import { Section, PromoCard } from 'layout/elements';
import { UnderCnst } from 'layout/elements/img';
import PageInfo from 'lib/PageInfo';

const title = 'О компании.';
const desc = 'И наших технологиях производства окон.';
const url = '/about-us';
const imageSrc = '/static/img/about-us/promo.jpg';

export default () => (
  <>
    <PageInfo title={title} description={desc} />
    <Section.Wrapper>
      <Section.Container>
        <Section.HeaderT title={title} subtitle={desc} h1 />
        <Section.Body>
          <UnderCnst />
        </Section.Body>
      </Section.Container>
    </Section.Wrapper>
  </>
);

export const Promo = () => (
  <PromoCard title={title} desc={desc} href={url} image={imageSrc} />
);
