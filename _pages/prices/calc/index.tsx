import React from 'react';
import { Section, PromoCard } from 'layout/elements';
import Page from 'lib/Page';
import Calc from 'components/Calc';
import { Typography } from '@material-ui/core';

const title = 'Калькулятор окон';
const pageTitle = 'Расчет стоимости';
const promoImg = '/static/img/calc/promo.jpg';
const desc =
  'Калькулятор позволяет предварительно просчитать стоимость остекления и примерно выяснить, какой бюджет на это нужно выделить.Важно понимать, размеры и цены – не все критерии, которые следует учитывать при выборе. На стоимость работ по остеклению влияет тип стеклопакета (однокамерный/двухкамерный), уровень сложности демонтажа старой оконной конструкции и необходимые работы по установке новой.';
const href = '/prices/calc';
interface Props {
  standalone?: boolean;
}

export const CalcSection = ({ standalone }: Props) => (
  <Section.Wrapper>
    <Section.Container>
      <Section.HeaderT title={pageTitle} subtitle="" h1={standalone} />
      <Section.Body>
        <Calc />
        <Section.Container>
          <Typography>{desc}</Typography>
        </Section.Container>
      </Section.Body>
    </Section.Container>
  </Section.Wrapper>
);
export default () => (
  <Page
    head={{
      title,
      description: desc,
    }}
  >
    <CalcSection standalone />
  </Page>
);
export const Promo = () => (
  <PromoCard title={title} desc="" href={href} image={promoImg} />
);
