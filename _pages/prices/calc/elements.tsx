import styled from 'layout/theme';
import { Section } from 'layout/elements';

export const Header = styled(Section.HeaderT)`
  position: absolute;
  left: 40%;
  top: 80px;
`;

export default {};
