import React from 'react';
import { LinearProgress, Typography } from '@material-ui/core';
import * as E from './elements';

export enum LineParams {
  WARM = 'warm',
  DESIGN = 'design',
  COMFORT = 'comfort',
}

export interface ILineParam {
  label: string;
  value: number;
  icon: string;
  showValue?: boolean;
  units?: string;
}

export default ({ label, value, icon, showValue }: ILineParam) => (
  <E.ProgressParam>
    <E.Content>
      <E.LabelContainer>
        <E.Icon icon={icon} />
        <Typography variant="body2">{label}</Typography>
      </E.LabelContainer>
      {showValue && <Typography variant="subtitle2">{`${value}%`}</Typography>}
    </E.Content>
    <LinearProgress
      color="secondary"
      variant="determinate"
      value={value || 0}
    />
  </E.ProgressParam>
);
