import styled from 'layout/theme';
import { Icon as IconT } from 'layout/elements';

export const ProgressParam = styled.div`
  margin-top: 30px;
  &:first-of-type {
    margin-top: 0px;
  }
`;

export const LabelContainer = styled.div`
  display: flex;
`;

export const Content = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Icon = styled(IconT)`
  height: 23px;
  margin-right: 7px;
  min-height: unset;
  min-width: unset;
  margin-bottom: 6px;
  width: auto;
`;
