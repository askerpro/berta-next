import React from 'react';
import ReactVisibilitySensor from 'react-visibility-sensor';

interface Props {
  onChange(visible: boolean): void;
}
interface State {
  viewportHeight: number;
}
export default class extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      viewportHeight: 0,
    };
  }

  componentDidMount() {
    this.setState({
      viewportHeight: document.documentElement.clientHeight,
    });
  }

  render() {
    const { viewportHeight } = this.state;
    const { onChange, children } = this.props;
    return (
      <ReactVisibilitySensor
        onChange={onChange}
        partialVisibility
        offset={{ top: viewportHeight / 2 }}
        minTopValue={viewportHeight / 2 + 50}
      >
        {children}
      </ReactVisibilitySensor>
    );
  }
}
