import React, { Component } from 'react';
import {
  Card,
  CardActionArea,
  CardActions,
  CardMedia,
  CardContent,
} from '@material-ui/core/';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { ProfileData, ProfileParams, EqupmentParam } from 'data/profiles';
import LineParam, { LineParams } from 'components/LineParam';
import Collapse from '@material-ui/core/Collapse';
import { Button } from 'layout/elements';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { isMobile } from 'lib/device-detect';

import * as E from './elements';

interface Props {
  data: ProfileData;
}

interface State {
  expanded: boolean;
  hovered: boolean;
}

class ProductCard extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      hovered: false,
      expanded: false,
    };
  }

  onMouseEnter = () => {
    this.setState({
      hovered: true,
    });
  };

  Params = () => {
    const { data } = this.props;
    return (
      <>
        <CardContent>
          <Typography variant="h6">Параметры профиля: </Typography>
        </CardContent>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                {data.profileParams[ProfileParams.MONTAGE_DEEP].name}
                <E.Tooltip
                  title={data.profileParams[ProfileParams.MONTAGE_DEEP].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.profileParams[ProfileParams.MONTAGE_DEEP].values.join(
                  ',',
                )}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.profileParams[ProfileParams.AIR_CHAMBERS].name}
                <E.Tooltip
                  title={data.profileParams[ProfileParams.AIR_CHAMBERS].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.profileParams[ProfileParams.AIR_CHAMBERS].values.join(
                  ',',
                )}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.profileParams[ProfileParams.ARM_THICKNESS].name}
                <E.Tooltip
                  title={data.profileParams[ProfileParams.ARM_THICKNESS].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.profileParams[ProfileParams.ARM_THICKNESS].values.join(
                  ',',
                )}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.profileParams[ProfileParams.PLASTIC_THICKNESS].name}
                <E.Tooltip
                  title={
                    data.profileParams[ProfileParams.PLASTIC_THICKNESS].title
                  }
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.profileParams[
                  ProfileParams.PLASTIC_THICKNESS
                ].values.join(', ')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.profileParams[ProfileParams.GLASS_WIDTH].name}
                <E.Tooltip
                  title={data.profileParams[ProfileParams.GLASS_WIDTH].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.profileParams[ProfileParams.GLASS_WIDTH].values.join(
                  ', ',
                )}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.profileParams[ProfileParams.COULD_BE_LAMINATED].name}
                <E.Tooltip
                  title={
                    data.profileParams[ProfileParams.COULD_BE_LAMINATED].title
                  }
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.profileParams[
                  ProfileParams.COULD_BE_LAMINATED
                ].values.join(', ')}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
        <CardContent>
          <Typography variant="h6">Комплектация: </Typography>
        </CardContent>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                {data.equpmentParams[EqupmentParam.FURNITURE].name}
                <E.Tooltip
                  title={data.equpmentParams[EqupmentParam.FURNITURE].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.equpmentParams[EqupmentParam.FURNITURE].values.join(', ')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.equpmentParams[EqupmentParam.HANDLES].name}
                <E.Tooltip
                  title={data.equpmentParams[EqupmentParam.HANDLES].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.equpmentParams[EqupmentParam.HANDLES].values.join(', ')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.equpmentParams[EqupmentParam.GLASS].name}
                <E.Tooltip
                  title={data.equpmentParams[EqupmentParam.GLASS].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.equpmentParams[EqupmentParam.GLASS].values.join(', ')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.equpmentParams[EqupmentParam.SEALANTS].name}
                <E.Tooltip
                  title={data.equpmentParams[EqupmentParam.SEALANTS].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.equpmentParams[EqupmentParam.SEALANTS].values.join(', ')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                {data.equpmentParams[EqupmentParam.SHTAPICKS].name}
                <E.Tooltip
                  title={data.equpmentParams[EqupmentParam.SHTAPICKS].title}
                  disableTouchListener
                >
                  <E.HelperIcon />
                </E.Tooltip>
              </TableCell>
              <TableCell align="right">
                {data.equpmentParams[EqupmentParam.SHTAPICKS].values.join(', ')}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </>
    );
  };

  render() {
    const { hovered, expanded } = this.state;
    const { data } = this.props;
    return (
      <Card>
        <E.Media>
          <E.Animation
            state={expanded ? 1 : 0}
            dataSrc={`/static/animations/profiles/${data.animationPath}/profile.json`}
          />
        </E.Media>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h5">{data.title}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>{data.desc}</Typography>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button href="/prices/calc" color="secondary">
            Рассчитать стоимость
          </Button>
          <Button
            onClick={() => {
              this.setState({
                expanded: !expanded,
              });
            }}
            color="secondary"
            style={{ marginLeft: 'auto' }}
          >
            Подробнее
            {expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
          </Button>
        </CardActions>
        <Collapse in={expanded}>{this.Params()}</Collapse>
      </Card>
    );
  }
}

export default ProductCard;

/* <Grid item xs={12}>
  <LineParam
    label={data.lineParams[LineParams.WARM].label}
    icon={data.lineParams[LineParams.WARM].icon}
    value={hovered && data.lineParams[LineParams.WARM].value}
  />
  <LineParam
    label={data.lineParams[LineParams.DESIGN].label}
    icon={data.lineParams[LineParams.DESIGN].icon}
    value={hovered && data.lineParams[LineParams.DESIGN].value}
  />
  <LineParam
    label={data.lineParams[LineParams.COMFORT].label}
    icon={data.lineParams[LineParams.COMFORT].icon}
    value={hovered && data.lineParams[LineParams.COMFORT].value}
  />
</Grid> */
