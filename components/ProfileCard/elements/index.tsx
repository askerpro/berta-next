/* eslint-disable @typescript-eslint/no-unused-vars */
import styled from 'layout/theme';
import React from 'react';

import Paper from '@material-ui/core/Paper';
import {
  Card,
  CardActionArea,
  CardActions,
  CardMedia,
  CardContent,
} from '@material-ui/core/';
import Grid from '@material-ui/core/Grid';
import AnimationT, { Props as AnimationPropsT } from 'components/Animation';
import HelpIcon from '@material-ui/icons/Help';
import TooltipT from '@material-ui/core/Tooltip';

export const Wrapper = styled(Card)``;

export const Media = styled(CardMedia)`
  padding-top: 75%;
  display: flex;
  justify-content: center;
  position: relative;
  background-color: #f0f0ee;
`;

export const MobileImage = styled.img``;

export const Animation = styled(AnimationT)`
  height: 100%;
  position: absolute;
  top: 0;
`;

export const HelperIcon = styled(HelpIcon)`
  vertical-align: middle;
  margin-left: 7px;
`;

export const Tooltip = styled(props => (
  <TooltipT classes={{ tooltip: 'tooltip' }} {...props} />
))`
  display: none;
  & .tooltip {
    font-size: 14px;
  }
`;
