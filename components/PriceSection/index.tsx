import rawProducts, { ProductTypes, ProductType } from 'data/products';
import handles, { label as handlesLabel } from 'data/handles';
import furnitures, { label as furnituresLabel } from 'data/furnitures';
import glasses, { label as glassesLabel } from 'data/glasses';
import profiles, { label as profilesLabel } from 'data/profiles';
import React from 'react';
import PriceSection, { Props as ComponentProps } from './component';

export { ProductType };

export interface Props extends Omit<ComponentProps, 'products'> {
  productType: ProductType;
}

export default ({ productType, subtitle, title, calcButton }: Props) => {
  return (
    <PriceSection
      calcButton={calcButton}
      products={rawProducts
        .filter(rawProduct => rawProduct.type === productType)
        .map(({ type, equipment, ...other }) => {
          return {
            ...other,
            profile: {
              label: profilesLabel,
              ...profiles[equipment.profile],
            },
            handle: {
              label: handlesLabel,
              ...handles[equipment.handle],
            },
            furniture: {
              label: furnituresLabel,
              ...furnitures[equipment.furniture],
            },
            glass: {
              label: glassesLabel,
              ...glasses[equipment.glass],
            },
          };
        })}
      title={title}
      subtitle={subtitle}
    />
  );
};
