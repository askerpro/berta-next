import React from 'react';
import { Section } from 'layout/elements';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import ProductCard, { Props as ProductCardProps } from 'components/ProductCard';
import Grid from '@material-ui/core/Grid';
import { Hidden } from '@material-ui/core';
import { Slider, Slide } from 'pure-react-carousel';
import * as E from './elements';

const defaultSubtitle = 'Типовые размеры и их цены. Калькулятор окон.';

export interface Props {
  products: ProductCardProps[];
  subtitle?: string;
  title?: string;
  calcButton?: boolean;
}

export default ({ products, subtitle, title, calcButton }: Props) => {
  return (
    <Section.Wrapper>
      <Section.Container>
        <Section.HeaderT
          title={title || 'Цены на продукцию'}
          subtitle={subtitle || defaultSubtitle}
          center
        />
        <Section.Body>
          <Hidden smUp>
            <E.Carousel
              naturalSlideWidth={296}
              naturalSlideHeight={476}
              totalSlides={products.length}
              visibleSlides={1}
              interval={2500}
            >
              <Slider>
                {products.map((cardData, i) => (
                  <Slide index={i} key={i}>
                    <E.SlideWrapper>
                      <ProductCard {...cardData} />
                    </E.SlideWrapper>
                  </Slide>
                ))}
              </Slider>
              <E.ArrowPrev>
                <KeyboardArrowLeft />
              </E.ArrowPrev>
              <E.ArrowNext>
                <KeyboardArrowRight />
              </E.ArrowNext>
            </E.Carousel>
          </Hidden>
          <Hidden mdDown>
            <Grid container spacing={5}>
              {products.map((cardData, i) => (
                <Grid item xs={12} lg={4} key={i}>
                  <ProductCard {...cardData} />
                </Grid>
              ))}
            </Grid>
          </Hidden>
        </Section.Body>
      </Section.Container>
    </Section.Wrapper>
  );
};
