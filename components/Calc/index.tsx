/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/no-unused-state */
import React from 'react';
import InputMask from 'react-input-mask';
import { IProductSize } from 'data/products';
import profiles, { ProfileType as ProfileTypes } from 'data/profiles';
import { LineParams as LineParamType } from 'components/LineParam';
import glasses, { GlassType as GlassTypes } from 'data/glasses';
import handles, { HandleType as HandleTypes } from 'data/handles';
import { Grey } from 'layout/theme';
import { calcSubmitReached } from 'lib/ym';
import {
  Grid,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Switch,
  Typography,
  TextField,
  Button,
  Container,
  Hidden,
  Dialog,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SendToOperator from 'lib/send-to-opearotr';
import ProfileSelect, {
  selectId as profileSelectId,
} from './parts/ProfileSelect';
import ProductTypeSelect, {
  ProductType,
  productTypes as productTypeData,
} from './parts/ProductTypeSelect';
import ProductImage from './parts/ProductImage';
import GlassSelect, { selectId as GlassSelectId } from './parts/GlassSelect';
import HandleSelect, { selectId as HandleSelectId } from './parts/HandleSelect';
import * as E from './elements';
import LineParams from './parts/LineParams';

interface State {
  activeProductType: ProductType;
  activeProductVariant: number;
  size: IProductSize;
  activeProfile: ProfileTypes;
  activeGlass: number;
  handle: number;
  isWindowSillNeeded: boolean;
  isWindowSlopeNeeded: boolean;
  isMosquitoNetNeeded: boolean;
  isInstallmentNeeded: boolean;
  userName: string;
  userPhone: string;
  stage: number;
  expandedSelect: string;
  hovered: boolean;
  isDialogOpen: boolean;
}

class Calc extends React.Component<{}, State> {
  scrollAnchor = React.createRef<HTMLDivElement>();

  phoneInputRef = React.createRef<HTMLInputElement>();

  constructor(props) {
    super(props);
    this.state = {
      activeProductType: ProductType.WINDOW,
      activeProductVariant: 1,
      activeProfile: ProfileTypes.BERTA_SILVER_ECO_CLASSIC,
      expandedSelect: '',
      size: {
        width: 1400,
        height: 1600,
      },
      activeGlass: 0,
      handle: 0,
      isWindowSillNeeded: false,
      isWindowSlopeNeeded: false,
      isMosquitoNetNeeded: false,
      isInstallmentNeeded: false,
      userName: '',
      userPhone: '',
      stage: 0,
      hovered: false,
      isDialogOpen: false,
    };
  }

  getValues = () => {
    const { activeProfile, activeGlass } = this.state;
    const values: number[] = [];
    values[0] =
      profiles[activeProfile].lineParams[LineParamType.WARM].value +
      glasses[activeGlass].lineParams[LineParamType.WARM];
    values[1] =
      profiles[activeProfile].lineParams[LineParamType.COMFORT].value +
      glasses[activeGlass].lineParams[LineParamType.COMFORT];
    values[2] =
      profiles[activeProfile].lineParams[LineParamType.DESIGN].value +
      glasses[activeGlass].lineParams[LineParamType.DESIGN];
    return values;
  };

  ProductTypeSelect = () => {
    const { activeProductType, activeProductVariant } = this.state;
    return (
      <ProductTypeSelect
        activeType={activeProductType}
        activeVariant={activeProductVariant}
        onVariantChange={index => {
          this.setState({
            activeProductVariant: index,
          });
        }}
        onTypeChange={index => {
          this.setState({
            activeProductType: index,
          });
        }}
      />
    );
  };

  ProfileSelect = () => {
    const { activeProfile, expandedSelect } = this.state;
    return (
      <ProfileSelect
        active={activeProfile}
        expanded={expandedSelect}
        onChange={newActiveProfile => {
          this.scrollToAnchor();
          this.setState({
            activeProfile: newActiveProfile,
            expandedSelect: '',
          });
        }}
        onExpandChange={(event, isExpanded) => {
          if (isExpanded) {
            this.setState({
              expandedSelect: profileSelectId,
            });
          } else if (expandedSelect === profileSelectId) {
            this.setState({
              expandedSelect: '',
            });
          }
        }}
      />
    );
  };

  HandleSelect = () => {
    const { handle, expandedSelect } = this.state;
    return (
      <HandleSelect
        active={handle}
        expanded={expandedSelect}
        onChange={newActiveProfile => {
          this.scrollToAnchor();
          this.setState({
            handle: newActiveProfile,
            expandedSelect: '',
          });
        }}
        onExpandChange={(event, isExpanded) => {
          if (isExpanded) {
            this.setState({
              expandedSelect: HandleSelectId,
            });
          } else if (expandedSelect === HandleSelectId) {
            this.setState({
              expandedSelect: '',
            });
          }
        }}
      />
    );
  };

  GlassSelect = () => {
    const { activeGlass, expandedSelect } = this.state;
    return (
      <GlassSelect
        active={activeGlass}
        expanded={expandedSelect}
        onChange={newActiveProfile => {
          this.scrollToAnchor();
          this.setState({
            activeGlass: newActiveProfile,
            expandedSelect: '',
          });
        }}
        onExpandChange={(event, isExpanded) => {
          if (isExpanded) {
            this.setState({
              expandedSelect: GlassSelectId,
            });
          } else if (expandedSelect === GlassSelectId) {
            this.setState({
              expandedSelect: '',
            });
          }
        }}
      />
    );
  };

  WindowSillSwitch = () => {
    const { isWindowSillNeeded } = this.state;
    return (
      <>
        <Switch
          checked={isWindowSillNeeded}
          onChange={(_, checked) => {
            this.setState({
              isWindowSillNeeded: checked,
            });
          }}
          edge="start"
        />
        <Typography component="span">Подоконник</Typography>
      </>
    );
  };

  WindowSlopeSwitch = () => {
    const { isWindowSlopeNeeded } = this.state;
    return (
      <>
        <Switch
          checked={isWindowSlopeNeeded}
          onChange={(_, checked) => {
            this.setState({
              isWindowSlopeNeeded: checked,
            });
          }}
          edge="start"
        />
        <Typography component="span">Отлив | Откосы</Typography>
      </>
    );
  };

  MosquitoNetSwitch = () => {
    const { isMosquitoNetNeeded } = this.state;
    return (
      <>
        <Switch
          checked={isMosquitoNetNeeded}
          onChange={(_, checked) => {
            this.setState({
              isMosquitoNetNeeded: checked,
            });
          }}
          edge="start"
        />
        <Typography component="span">Москитная сетка</Typography>
      </>
    );
  };

  InstallmentSwitch = () => {
    const { isInstallmentNeeded } = this.state;
    return (
      <>
        <Switch
          checked={isInstallmentNeeded}
          onChange={(_, checked) => {
            this.setState({
              isInstallmentNeeded: checked,
            });
          }}
          edge="start"
        />
        <Typography component="span">В рассрочку</Typography>
      </>
    );
  };

  UserNameInput = () => {
    const { userName } = this.state;
    return (
      <TextField
        required
        label="Ваше имя"
        value={userName}
        onChange={event => {
          this.setState({
            userName: event.target.value,
          });
        }}
        type="name"
        fullWidth
      />
    );
  };

  UserPhoneInput = () => {
    const { userPhone } = this.state;
    return (
      <InputMask
        mask="+7 ( 999 ) 999 99 99"
        value={userPhone}
        alwaysShowMask={false}
        onChange={event => {
          this.setState({
            userPhone: event.target.value,
          });
        }}
      >
        {() => (
          <TextField ref={this.phoneInputRef} label="Ваш номер" fullWidth />
        )}
      </InputMask>
    );
  };

  ProductImage = () => {
    const { activeProductVariant, activeProductType } = this.state;
    return (
      <ProductImage type={activeProductType} variant={activeProductVariant} />
    );
  };

  isAllInputsValid = () => {
    const { userName, userPhone } = this.state;
    return userName !== '' && userPhone.indexOf('_') === -1;
  };

  submit = () => {
    const {
      size,
      handle,
      activeProfile,
      activeGlass,
      userName,
      userPhone,
      isWindowSillNeeded,
      isInstallmentNeeded,
      isMosquitoNetNeeded,
      isWindowSlopeNeeded,
    } = this.state;
    if (this.isAllInputsValid()) {
      calcSubmitReached();
      this.setState({
        isDialogOpen: false,
      });
      const subject = 'Запрос на рассчет стоимости окна с сайта';
      const sizeText = `Ширина: ${size.width} | Высота: ${size.height}`;
      const profileText = profiles[activeProfile].title;
      const handleText = handles[handle].title;
      const glassText = glasses[activeGlass].title;
      const productText = '';
      const type = '';
      const msg = `
        <p> Тип: <strong>${productText}</strong></p>\r\n
        <p> Тип: <strong>${type}</strong></p>\r\n
        <p> Профиль: <strong>${profileText}</strong></p>\r\n
        <p> Ручка: <strong>${handleText}</strong></p>\r\n
        <p> Стеклопакет: <strong>${glassText}</strong></p>\r\n
        <p> Размеры: ${sizeText}</p>\r\n
        <p> Подоконник: ${isWindowSillNeeded ? '+' : '-'}</p>\r\n
        <p> Москитная сетка: ${isMosquitoNetNeeded ? '+' : '-'}</p>\r\n
        <p> Откосы | Отлив: ${isWindowSlopeNeeded ? '+' : '-'}</p>\r\n
        <p> В рассрочку: ${isInstallmentNeeded ? '+' : '-'}</p>\r\n
				<p> Имя: <strong>${userName}</strong></p>\r\n
        <p> Телефон: <strong>${userPhone}</strong></p>\r\n
      `;
      this.setState({
        stage: 1,
      });
      SendToOperator(subject, msg, result => {
        if (result.error) {
          alert('Ошибка при отправке запроса');
        } else {
          this.setState({
            stage: 1,
          });
        }
      });
    } else {
      alert('Пожалуйста, заполните все необходимые поля по формату');
    }
  };

  scrollToAnchor = () => {
    console.log();
    window.scrollTo({
      top:
        this.scrollAnchor.current.getBoundingClientRect().top + window.scrollY,
      behavior: 'smooth',
    });
  };

  VisualArea = () => {
    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          {this.ProductTypeSelect()}
        </Grid>
        <Grid item xs={12}>
          {this.ProductImage()}
        </Grid>
        {/* <Hidden smDown>
          <Grid item xs={12}>
            <LineParams hovered values={this.getValues()} />
          </Grid>
        </Hidden> */}
      </Grid>
    );
  };

  Extras = () => {
    return (
      <Grid container spacing={4}>
        <Grid item xs={12}>
          {this.ProfileSelect()}
          {this.GlassSelect()}
          {this.HandleSelect()}
        </Grid>
        <Grid item xs={12} sm={6}>
          {this.WindowSillSwitch()}
        </Grid>
        <Grid item xs={12} sm={6}>
          {this.WindowSlopeSwitch()}
        </Grid>
        <Grid item xs={12} sm={6}>
          {this.MosquitoNetSwitch()}
        </Grid>
        <Grid item xs={12} sm={6}>
          {this.InstallmentSwitch()}
        </Grid>
      </Grid>
    );
  };

  userInputs = () => {
    const { isDialogOpen } = this.state;
    return (
      <Dialog
        open={isDialogOpen}
        onClose={() => {
          this.setState({
            isDialogOpen: false,
          });
        }}
        maxWidth="xs"
      >
        <DialogTitle>
          <Typography>Заявка на рассчет</Typography>
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              {this.UserNameInput()}
            </Grid>
            <Grid item xs={12} sm={12}>
              {this.UserPhoneInput()}
            </Grid>
            <Grid item xs={12} sm={12}>
              <Button
                color="primary"
                variant="contained"
                fullWidth
                onClick={this.submit}
              >
                Рассчитать
              </Button>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    );
  };

  render() {
    const { stage, hovered } = this.state;
    const { VisualArea } = this;
    return (
      <Grey>
        {stage === 0 && (
          <Grid container spacing={3} alignItems="flex-start">
            <E.ImageWrapper item xs={12} sm={5}>
              <E.ImageInner>
                <VisualArea />
              </E.ImageInner>
            </E.ImageWrapper>

            <Grid item xs={12} sm={7} ref={this.scrollAnchor}>
              {this.userInputs()}
              <E.Wrapper>
                <Hidden smDown>{this.Extras()}</Hidden>
                <Hidden smUp>
                  <ExpansionPanel>
                    <ExpansionPanelSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography>Дополнительные параметры</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails style={{ padding: 0 }}>
                      {this.Extras()}
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                </Hidden>
                <Button
                  color="primary"
                  variant="contained"
                  fullWidth
                  style={{ marginTop: '20px' }}
                  onClick={() => {
                    this.setState({
                      isDialogOpen: true,
                    });
                  }}
                >
                  Рассчитать
                </Button>
              </E.Wrapper>
            </Grid>
          </Grid>
        )}
        {stage === 1 && (
          <Container maxWidth="sm">
            <Grid container spacing={5}>
              <Grid item>
                <Typography variant="h4">
                  Запрос на рассчет принят. Ждите звонка оператора.
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <E.GridInner>
                  <Button
                    color="primary"
                    variant="contained"
                    fullWidth
                    onClick={() => {
                      this.setState({
                        stage: 0,
                      });
                    }}
                  >
                    Рассчитать еще одно окно
                  </Button>
                </E.GridInner>
              </Grid>
            </Grid>
          </Container>
        )}
      </Grey>
    );
  }
}
export default Calc;
