import React from 'react';
import {
  Grid,
  CardContent,
  Typography,
  CardActionArea,
  Button,
} from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import * as E from './elements';

export enum ProductType {
  WINDOW,
  DOOR_WINDOW,
  DOOR,
}

interface ProductVariant {
  icon: string;
  title: string;
}

export interface Product {
  title: string;
  variants: ProductVariant[];
}

export const productTypes: Product[] = [
  {
    title: 'Окно',
    variants: [
      {
        title: 'Одностворчатое окно',
        icon: '/static/img/calc/types/windows/window_10001.png',
      },
      {
        title: 'Двухстворчатое окно',
        icon: '/static/img/calc/types/windows/window_20001.png',
      },
      {
        title: 'Трехстворчатое окно',
        icon: '/static/img/calc/types/windows/window_30001.png',
      },
    ],
  },
  {
    title: 'Балконный блок',
    variants: [
      {
        title: 'С одностворчатым окном',
        icon: '/static/img/calc/types/balconies/balcony_10001.png',
      },
      {
        title: 'C двухстворчатым окном',
        icon: '/static/img/calc/types/balconies/balcony_20001.png',
      },
    ],
  },
  {
    title: 'Дверь',
    variants: [
      {
        title: 'Одностворчатая дверь',
        icon: '/static/img/calc/types/doors/door_10001.png',
      },
      {
        title: 'Двухстворчатая дверь',
        icon: '/static/img/calc/types/doors/door_20001.png',
      },
    ],
  },
];

export const selectId = 'productTypeSelect';

export const label = 'Тип изделия';

export interface Props {
  activeType: number;
  activeVariant: number;
  onTypeChange(activeType: number): void;
  onVariantChange(activeVariant: number): void;
}

export default ({
  activeType,
  activeVariant,
  onTypeChange,
  onVariantChange,
}: Props) => {
  return (
    <Grid container spacing={2}>
      <Grid item md={6} xs={6}>
        <FormControl fullWidth>
          <InputLabel>Тип изделия</InputLabel>
          <Select
            value={activeType}
            onChange={event => {
              onTypeChange(event.target.value as number);
            }}
          >
            {productTypes.map((product, i) => (
              <MenuItem value={i} key={i}>
                {product.title}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item md={6} xs={6}>
        <Grid container spacing={1} wrap="nowrap" alignContent="center">
          {productTypes[activeType].variants.map((variant, i) => (
            <Grid item key={i}>
              <Button
                onClick={() => {
                  onVariantChange(i);
                }}
              >
                <E.Image src={variant.icon} />
              </Button>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};
