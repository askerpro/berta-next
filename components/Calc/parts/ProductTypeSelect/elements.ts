import React from 'react';
import styled, { css } from 'layout/theme';
import {
  Typography,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ExpansionPanel,
  Button,
  Divider,
  Grid,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
} from '@material-ui/core';

export const Item = styled(Card)`
`;

export const Image = styled.img`
  height: 50px;
  width: auto;
`;

export const Wrapper = styled.div``;
