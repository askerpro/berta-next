import React from 'react';
import styled, { css } from 'layout/theme';
import {
  Typography,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ExpansionPanel,
  Button,
  Divider,
  Grid,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
} from '@material-ui/core/';
import { ButtonProps } from '@material-ui/core/Button';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ExpansionPanelSummaryProps } from '@material-ui/core/ExpansionPanelSummary';

export const SelectWrapper = styled(ExpansionPanel)``;
interface ISelectHeader extends ExpansionPanelSummaryProps {
  hidden?: boolean;
}
export const SelectHeader = styled(({ hidden, ...props }: ISelectHeader) => (
  <ExpansionPanelSummary
    {...props}
    expandIcon={<ExpandMoreIcon />}
    classes={{
      content: 'content',
    }}
  />
))`
  padding-left: 0;

  & .content {
    margin: 0;
  }
  .Mui-expanded & {
    background-color: #eee;
  }
`;

export interface IHeaderTitle {
  label: string;
  title: string;
  opened: boolean;
}

export const HeaderIconWrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 12px;
  justify-content: center;
  max-height: 100%;
  max-width: 100%;
`;

export const HeaderIcon = styled.img`
  max-width: 100%;
  max-height: 100%;
`;

export const HeaderTitle = styled(
  ({ label, title, opened, ...other }: IHeaderTitle) => {
    return (
      <div {...other}>
        <Typography>{label}</Typography>
        {!opened && <Typography>{title}</Typography>}
      </div>
    );
  },
)``;

export const HeaderContent = styled.div`
  padding: 12px;
`;

export const HeaderLabel = styled(props => (
  <Typography {...props} variant="body2" />
))``;

export const ItemTitle = props => (
  <Typography variant="h6" color="textPrimary" {...props} />
);

export const ItemDesc = props => (
  <Typography variant="body2" color="textSecondary" {...props} />
);

export const ItemIcon = styled(CardMedia)`
  margin: 12px;
  width: 72px;
  max-height: 100px;
  min-height: 60px;
  display: block;
  flex-shrink: 0;
  background-size: contain;
`;

export const SelectBody = styled(({ children, ...props }) => (
  <ExpansionPanelDetails {...props}>
    <Grid container spacing={3}>
      {children}
    </Grid>
  </ExpansionPanelDetails>
))`
  .Mui-expanded & {
    box-shadow: grey 0px 0px 6px inset;
  }
  flex-wrap: wrap;
`;

export const ItemActionArea = styled(CardActionArea)``;

export interface IItem {
  icon: string;
  title: string;
  desc: string;
}

export const Item = styled(Card)`
  display: flex;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
   flex-direction: column;
  }`}
`;

export const ItemContent = styled(CardContent)``;

export const ItemActions = styled(props => (
  <Grid container justify="space-between" item xs={12} {...props} />
))``;

export const ItemMore = styled(props => (
  <Grid item>
    <Button {...props} size="small" />
  </Grid>
))<ButtonProps>``;

export const ItemSelect = styled(props => (
  <Grid item>
    <Button variant="contained" size="small" color="primary" {...props} />
  </Grid>
))<ButtonProps>``;

interface IItemT {
  title: string;
  desc: string;
  icon: string;
  onClick?(): void;
}

export const ItemT = ({ desc, icon, title, onClick }: IItemT) => {
  return (
    <Grid item xs={12}>
      <ItemActionArea onClick={onClick}>
        <Item>
          <ItemIcon src={icon} image={icon} />
          <ItemContent>
            <ItemTitle>{title}</ItemTitle>
            <ItemDesc>{desc}</ItemDesc>
          </ItemContent>
        </Item>
      </ItemActionArea>
    </Grid>
  );
};

export interface ISelectT {
  selectId: string;
  label: string;
  expanded: string;
  active: number;
  items: Array<Omit<IItemT, 'onClick'>>;
  onChange(active: number): void;
  onExpandChange(event: React.ChangeEvent<{}>, isExpanded: boolean): void;
}

export const SelectT = ({
  expanded,
  active,
  onExpandChange,
  onChange,
  selectId,
  label,
  items,
}: ISelectT) => {
  return (
    <SelectWrapper expanded={expanded === selectId} onChange={onExpandChange}>
      <SelectHeader hidden={expanded && expanded !== selectId}>
        <div>
          <HeaderIcon src={items[active].icon} />
        </div>
        <HeaderContent>
          <HeaderLabel>{label}</HeaderLabel>
          <ItemTitle>{items[active].title}</ItemTitle>
        </HeaderContent>
      </SelectHeader>
      <SelectBody>
        {items.map((item, i) => {
          return (
            <ItemT
              {...item}
              onClick={() => {
                onChange(i);
              }}
            />
          );
        })}
      </SelectBody>
    </SelectWrapper>
  );
};
