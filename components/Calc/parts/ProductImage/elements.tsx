import styled, { css } from 'layout/theme';
import { Button } from '@material-ui/core';
import React from 'react';

export const Helper = styled.div`
  position: relative;
`;
export const Wrapper = styled.div`
  margin-top: 30px;
  margin-left: auto;
  margin-right: auto;
  width: fit-content;
  display: flex;
  box-sizing: border-box;
  position: relative;
  align-items: flex-start;
`;
export const Img = styled.img`
  max-height: 300px;
  max-width: 100%;
`;

export const SizeWrapperT = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  justify-content: center;
`;

export const SizeVisualT = styled.div`
  background: #cacaca;
  position: absolute;
`;

export const SizeHeightWrapper = styled(SizeWrapperT)`
  height: 97%;

  border-top: 1px solid #ccc;
  border-bottom: 1px solid #ccc;
  top: 0;
  :before {
    content: '';
    background-image: url(/static/img/calc/arrow.png);
    width: 10px;
    height: 10px;
    position: absolute;
    top: 0;
    background-size: contain;
    z-index: 1;
  }
  :after {
    content: '';
    background-image: url(/static/img/calc/arrow.png);
    width: 10px;
    height: 10px;
    position: absolute;
    bottom: 0;
    background-size: contain;
    transform: rotateZ(180deg);
    z-index: 1;
  }
`;
export const SizeHeightVisual = styled(SizeVisualT)`
  width: 1px;
  height: 100%;
  left: 50%;
`;

export const SizeWidthWrapper = styled(SizeWrapperT)`
  width: 100%;
  top: -30px;
  border-left: 1px solid #ccc;
  border-right: 1px solid #ccc;
  :before {
    content: '';
    background-image: url(/static/img/calc/arrow.png);
    width: 10px;
    height: 10px;
    position: absolute;
    left: 0;
    background-size: contain;
    transform: rotateZ(-90deg);
    z-index: 1;
  }
  :after {
    content: '';
    background-image: url(/static/img/calc/arrow.png);
    width: 10px;
    height: 10px;
    position: absolute;
    right: 0;
    background-size: contain;
    transform: rotateZ(90deg);
    z-index: 1;
  }
`;

export const SizeWidthVisual = styled(SizeVisualT)`
  height: 1px;
  width: 100%;
`;

export const Input = styled.input`
  max-width: 40px;
  z-index: 1;
  background-color: ${({ theme }) => theme.palette.background.default};
`;

export const WidthT = ({ className }) => (
  <SizeWidthWrapper className={className}>
    <Input defaultValue="1200" />
    <SizeWidthVisual />
  </SizeWidthWrapper>
);

export const HeightT = ({ className }) => (
  <SizeHeightWrapper className={className}>
    <Input defaultValue="1300" />
    <SizeHeightVisual />
  </SizeHeightWrapper>
);
