import React from 'react';
import * as E from 'components/Calc/parts/ProductImage/elements';
import styled from 'layout/theme';
const img = '/static/img/calc/types/windows/window_10001.png';

const Width = styled(({ className }) => <E.WidthT className={className} />)`
    width: 66%;
    top: -2%;
    left: 17%;
`;

const Height = styled(({ className }) => <E.HeightT className={className} />)`
    top: 6%;
    height: 73%;
    left: -3%;
`;

export default () => {
    return (
        <E.Wrapper>
            <Width />
            <Height />
            <E.Img src={img} />
        </E.Wrapper>
    );
};
