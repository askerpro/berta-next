import React from 'react';
import * as E from 'components/Calc/parts/ProductImage/elements';
import styled from 'layout/theme';
const img = '/static/img/calc/types/windows/window_30001.png';

const Width = styled(({ className }) => <E.WidthT className={className} />)`
    width: 87.3%;
    top: -6%;
    left: 6%;
`;

const Height = styled(({ className }) => <E.HeightT className={className} />)`
    top: 4.6%;
    height: 64.9%;
    left: -6%;  
`;

export default () => {
    return (
        <E.Wrapper>
            <Width />
            <Height />
            <E.Img src={img} />
        </E.Wrapper>
    );
};