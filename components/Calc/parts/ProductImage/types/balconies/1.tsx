import React from 'react';
import * as E from 'components/Calc/parts/ProductImage/elements';
import styled from 'layout/theme';
const img = '/static/img/calc/types/balconies/balcony_10001.png';

const WWidth = styled(({ className }) => <E.WidthT className={className} />)`
    width: 41%;
    top: 63%;
    left: 8%;
`;

const SWidth = styled(({ className }) => <E.WidthT className={className} />)`
    width: 84%;
    top: -5%;
    left: 9%;
`;

const DHeight = styled(({ className }) => <E.HeightT className={className} />)`
    top: 4%;
    height: 91%;
    right: -10%;
`;


const WHeight = styled(({ className }) => <E.HeightT className={className} />)`
    top: 4%;
    height: 54%;
    left: -10%;
`;

export default () => {
    return (
        <E.Wrapper>
            <SWidth />
            <DHeight />
            <WWidth />
            <WHeight />
            <E.Img src={img} />
        </E.Wrapper>
    );
};
