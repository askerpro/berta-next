import React from 'react';
import * as E from 'components/Calc/parts/ProductImage/elements';
import styled from 'layout/theme';
const img = '/static/img/calc/types/balconies/balcony_20001.png';

const WWidth = styled(({ className }) => <E.WidthT className={className} />)`
    width: 59%;
    top: 64%;
    left: 7%;
`;

const SWidth = styled(({ className }) => <E.WidthT className={className} />)`
    width: 91%;
    top: -5%;
    left: 6%;
`;

const DHeight = styled(({ className }) => <E.HeightT className={className} />)`
    top: 4%;
    height: 92%;
    right: -5%;
`;


const WHeight = styled(({ className }) => <E.HeightT className={className} />)`
    top: 4%;
    height: 55%;
    left: -3%;
`;

export default () => {
    return (
        <E.Wrapper>
            <SWidth />
            <DHeight />
            <WWidth />
            <WHeight />
            <E.Img src={img} />
        </E.Wrapper>
    );
};
