import React from 'react';
import * as E from 'components/Calc/parts/ProductImage/elements';
import styled from 'layout/theme';
const img = '/static/img/calc/types/doors/door_10001.png';

const Width = styled(({ className }) => <E.WidthT className={className} />)`
    width: 84%;
    top: -5%;
    left: 9%;
`;

const Height = styled(({ className }) => <E.HeightT className={className} />)`
    top: 3%;
    height: 94%;
    left: -26%;
`;

export default () => {
    return (
        <E.Wrapper>
            <Width />
            <Height />
            <E.Img src={img} />
        </E.Wrapper>
    );
};
