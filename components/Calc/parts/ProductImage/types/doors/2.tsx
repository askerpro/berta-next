import React from 'react';
import * as E from 'components/Calc/parts/ProductImage/elements';
import styled from 'layout/theme';
const img = '/static/img/calc/types/doors/door_20001.png';

const Width = styled(({ className }) => <E.WidthT className={className} />)`
    width: 92.3%;
    top: -6%;
    left: 4%;
`;

const Height = styled(({ className }) => <E.HeightT className={className} />)`
    top: 2.6%;
    height: 93.9%;
    left: -16%;
`;

export default () => {
    return (
        <E.Wrapper>
            <Width />
            <Height />
            <E.Img src={img} />
        </E.Wrapper>
    );
};