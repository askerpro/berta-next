import React from 'react';
import { IProductSize } from 'data/products';
import { ProductType } from '../ProductTypeSelect';
import * as E from './elements';
import Windows from './types/windows';
import Doors from './types/doors';
import Balconies from './types/balconies';
interface Props {
  type: number;
  variant: number;
}

const products: React.FC[][] = [];
products[ProductType.WINDOW] = Windows;
products[ProductType.DOOR] = Doors;
products[ProductType.DOOR_WINDOW] = Balconies;

export default ({ type, variant }: Props) => {
  const R = products[type][variant];
  return <R />;
}