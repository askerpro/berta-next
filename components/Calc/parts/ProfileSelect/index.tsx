import React from 'react';
import items, { ProfileType as ProfileTypes } from 'data/profiles';
import * as E from '../elements';

export const selectId = 'profileSelect';

export const label = 'Профильная система';

export interface Props {
  expanded: string;
  active: number;
  onChange(activeProfile: ProfileTypes): void;
  onExpandChange(event: React.ChangeEvent<{}>, isExpanded: boolean): void;
}

export default ({ expanded, active, onExpandChange, onChange }: Props) => {
  return (
    <E.SelectWrapper expanded={expanded === selectId} onChange={onExpandChange}>
      <E.SelectHeader hidden={expanded && expanded !== selectId}>
        <E.ItemIcon image={items[active].icon} />
        <E.HeaderContent>
          <E.HeaderLabel>{label}</E.HeaderLabel>
          <E.ItemTitle>{items[active].title}</E.ItemTitle>
        </E.HeaderContent>
      </E.SelectHeader>
      <E.SelectBody>
        {items.map((item, i) => {
          return (
            <E.ItemT
              {...item}
              onClick={() => {
                onChange(i);
              }}
            />
          );
        })}
      </E.SelectBody>
    </E.SelectWrapper>
  );
};
