import LineParam, { ILineParam } from 'components/LineParam';
import React from 'react';

export interface Props {
  values: number[];
  hovered: boolean;
}

export default ({ values, hovered }: Props) => {
  return (
    <>
      <LineParam
        label="Тепло / Шумо изоляция"
        icon="temp"
        value={hovered && values[0]}
        showValue
      />
      <LineParam
        label="Уровень комфорта"
        icon="easy_care"
        value={hovered && values[1]}
        showValue
      />
      <LineParam
        label="Внешний вид"
        icon="design"
        value={hovered && values[2]}
        showValue
      />
    </>
  );
};
