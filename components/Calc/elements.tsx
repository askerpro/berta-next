import styled from 'layout/theme';
import { Paper, Grid } from '@material-ui/core';

export const Wrapper = styled.div``;

export const ImageWrapper = styled(Grid)`
  ${({ theme }) => `${theme.breakpoints.up('sm')} {
    top: 60px;
    position: sticky;
    z-index: 2;
  }`}
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
   position: relative;
   margin-bottom: -20%;
  }`}
`;

export const ImageInner = styled(Paper)`
  padding: ${({ theme }) => theme.spacing(3)}px;

  background-color: #f3f3f3;
`;

export const ProductImage = styled.img``;

export const GridInner = styled.div`
  height: 100%;
  display: flex;
  align-items: flex-end;
`;
