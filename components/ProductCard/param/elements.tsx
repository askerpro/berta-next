import React from 'react';
import styled, { css } from 'layout/theme';
import { Typography, Button, Divider, Grid } from '@material-ui/core/';
import { ButtonProps } from '@material-ui/core/Button';

export const ItemLabel = styled(props => (
  <Typography {...props} variant="body2" />
))``;

export const ItemTitle = props => (
  <Typography variant="h6" color="textPrimary" {...props} />
);

export const ItemDesc = props => (
  <Typography variant="body2" color="textSecondary" {...props} />
);

export const ItemIcon = styled(props => (
  <Grid item sm={2} xs={12}>
    <img {...props} alt="" />
  </Grid>
))`
  width: 50px;
  height: auto;
  display: block;
`;

export const Item = styled(props => (
  <Grid container item xs={12} {...props} />
))``;

export const ItemContent = styled(props => <Grid item xs={12} {...props} />)``;

export const ItemActions = styled(props => (
  <Grid container justify="space-between" item xs={12} {...props} />
))``;

export const ItemMore = styled(props => (
  <Grid item>
    <Button {...props} size="small" />
  </Grid>
))<ButtonProps>``;

export const ItemSelect = styled(props => (
  <Grid item>
    <Button variant="contained" size="small" color="primary" {...props} />
  </Grid>
))<ButtonProps>``;

export { Divider };
