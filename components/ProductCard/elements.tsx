import {
  Card as CardT,
  CardContent as CardContentT,
  Typography,
  DialogContent as DialogContentT,
} from '@material-ui/core';
import CardActionsT from '@material-ui/core/CardActions';

import { Calc } from 'layout/elements/Icon';
import styled from 'layout/theme';

export const Card = styled(CardT)`
  position: relative;
  overflow: visible;
`;
export const Image = styled.img`
  display: block;
  margin: 0 auto;
  width: 100%;
`;

export const CardContent = styled(CardContentT)`
  padding: ${({ theme }) => theme.spacing(2)}px
    ${({ theme }) => theme.spacing(4)}px;
`;

export const CardActions = styled(CardActionsT)`
  justify-content: space-between;
  padding: ${({ theme }) => theme.spacing(2)}px;
  padding-top: 0;
`;

export const OldPrice = styled(Typography)`
  text-decoration: line-through;
  margin-bottom: -6px;
`;

export const BestSeller = styled.div`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translate3d(-50%, -50%, 0);
  background-color: #e8be1f;
  color: #fff;
  padding: 5px 15px;
`;

export const MoreWrapper = styled.div`
  margin: ${({ theme }) => theme.spacing(2)}px;
`;

export const DialogContent = styled(DialogContentT)``;

export const CalcIcon = styled(Calc)`
  filter: brightness(1);
  height: 18px;
  margin-right: 5px;
`;

export default {};
