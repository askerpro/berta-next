import React from 'react';
import { Calc } from 'layout/elements/Icon';
import Typography from '@material-ui/core/Typography';
import { Grid, Dialog, Divider, Hidden, CardActions } from '@material-ui/core';
import { Button } from 'layout/elements';
import CardMedia from '@material-ui/core/CardMedia';
import Rating from '@material-ui/lab/Rating';
import DialogTitle from 'layout/elements/Dialog/Title';
import DialogActions from 'layout/elements/Dialog/Actions';
import { Service } from '_pages/services/order';
import * as E from './elements';
import Param, { Props as ParamProps } from './param';

export interface Props {
  isbestseller?: boolean;
  img: string;
  name?: string;
  oldprice?: string;
  price: string;
  rating?: number;
  size: {
    width: number;
    height: number;
  };
  profile: ParamProps;
  handle: ParamProps;
  furniture: ParamProps;
  glass: ParamProps;
}

interface State {
  isMoreOpen: boolean;
}

class ProductCard extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isMoreOpen: false,
    };
  }

  DialogInner = () => {
    const {
      size,
      price,
      profile,
      handle,
      furniture,
      glass,
      img,
      name,
      oldprice,
    } = this.props;
    return (
      <>
        <DialogTitle
          onClose={() => {
            this.setState({
              isMoreOpen: false,
            });
          }}
        >
          {name}
        </DialogTitle>
        <E.DialogContent dividers>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={5}>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <E.Image src={img} alt="Схематическое изображение продукта" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={7}>
              <Grid container spacing={1}>
                <Param {...profile} divider />
                <Param {...furniture} divider />
                <Param {...glass} divider />
                <Param {...handle} />
                <Grid item xs={12}>
                  <Divider />
                </Grid>

                <Grid item xs={12}>
                  <Typography>
                    В стоимость включены: замер, доставка и установка
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </E.DialogContent>
        <DialogActions>
          <Button variant="contained" color="secondary" href="/prices/calc">
            <E.CalcIcon />
            Рассчитать стоимость
          </Button>
        </DialogActions>
      </>
    );
  };

  render() {
    const {
      size,
      price,
      profile,
      img,
      name,
      oldprice,
      rating,
      isbestseller,
    } = this.props;
    const { isMoreOpen } = this.state;

    return (
      <E.Card>
        <Hidden smUp>
          <Dialog
            open={isMoreOpen}
            onClose={() => {
              this.setState({
                isMoreOpen: false,
              });
            }}
            fullScreen
            maxWidth="md"
          >
            {this.DialogInner()}
          </Dialog>
        </Hidden>
        <Hidden mdDown>
          <Dialog
            open={isMoreOpen}
            onClose={() => {
              this.setState({
                isMoreOpen: false,
              });
            }}
            maxWidth="md"
          >
            {this.DialogInner()}
          </Dialog>
        </Hidden>
        {isbestseller && (
          <E.BestSeller>
            <Typography>ХИТ ПРОДАЖ</Typography>
          </E.BestSeller>
        )}
        <CardMedia>
          <E.Image src={img} alt="Схематическое изображение продукта" />
        </CardMedia>
        <E.CardContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h6" color="textPrimary">
                {name}
              </Typography>
            </Grid>

            {rating && (
              <Grid item xs={12}>
                <Rating value={rating} size="small" precision={0.1} readOnly />
                <Typography
                  variant="body1"
                  color="textPrimary"
                  display="inline"
                >
                  {rating}
                </Typography>
              </Grid>
            )}

            <Grid item xs={12}>
              <Typography variant="body1" color="textSecondary">
                Профильная система:
                <Typography
                  style={{ fontWeight: 500 }}
                  color="textPrimary"
                  component="span"
                >
                  {' '}
                  {profile.title}
                </Typography>
              </Typography>
            </Grid>
          </Grid>
        </E.CardContent>
        <E.CardActions disableSpacing>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Button
                fullWidth
                variant="outlined"
                onClick={() => {
                  this.setState({
                    isMoreOpen: true,
                  });
                }}
              >
                Подробнее
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                href="/prices/calc"
                color="secondary"
                fullWidth
              >
                <E.CalcIcon />
                Рассчитать стоимость
              </Button>
            </Grid>
          </Grid>
        </E.CardActions>
      </E.Card>
    );
  }
}

export default ProductCard;
