import React from 'react';
import * as path from 'path';
import { Hidden } from '@material-ui/core';
import { isMobile } from 'lib/device-detect';
import * as E from './elements';
// function arrayBufferToBase64(buffer) {
//   let binary = '';
//   const bytes = [].slice.call(new Uint8Array(buffer));

//   bytes.forEach(b => (binary += String.fromCharCode(b)));

//   return window.btoa(binary);
// }

interface ITexutre {
  image: string;
  size: { w: number; h: number };
}

interface ISpriteData {
  textures: ITexutre[];
}

export interface Props extends React.HTMLAttributes<HTMLDivElement> {
  dataSrc: string;
  state: number;
  adjust?: boolean;
  className?: string;
  onReady?(): void;
  onAnimationPlayedStart?(): void;
  onAnimationPlayedEnd?(): void;
  onBeforeFrame?(frameIndex: number): void;
  onAfterFrame?(frameIndex: number): void;
  duration?: number;
  poster?: boolean;
}

interface State {
  loadedCount: number;
  dataLoaded: boolean;
  ready: boolean;
  currentFrame: number;
}

class Animation extends React.PureComponent<Props, State> {
  animationId = 0;

  fps = 24;

  frames: ITexutre[];

  texture: string;

  lastTimeStamp: number;

  onAnimationStopEventFired: boolean;

  onAnimationPlayEventFired: boolean;

  displayRef: HTMLDivElement;

  posterSrc: string;

  scale = 1;

  wrapperStyles: {
    width: string;
    height: string;
  };

  constructor(props) {
    super(props);
    this.state = {
      loadedCount: 0,
      dataLoaded: false,
      ready: false,
      currentFrame: 0,
    };
    const { dataSrc } = this.props;
    const tempPoster = dataSrc.split('.');
    this.posterSrc = `${tempPoster[0]}-0.jpg`;
  }

  componentDidMount() {
    this.loadData();
  }

  frameSize = () => {
    return this.frames[0].size;
  };

  targetFrameIndex = () => {
    const { state } = this.props;
    const targetFrameIndexT = Math.round(state * (this.frames.length - 1));
    return targetFrameIndexT;
  };

  loadData = () => {
    const { dataSrc } = this.props;

    fetch(dataSrc)
      .then(res => res.json())
      .then(
        (spriteData: ISpriteData) => {
          this.frames = spriteData.textures;
          this.setState(
            {
              dataLoaded: true,
            },
            () => {
              // this.frames = spriteData.textures.map((textureData, i) => {
              //   const frame = textureData;
              //   frame.image = path.join(
              //     path.dirname(dataSrc),
              //     textureData.image,
              //   );
              //   console.log(frame);
              //   fetch(path.join(path.dirname(dataSrc), textureData.image))
              //     .then(response => response.blob())
              //     .then(buffer => {
              //       frame.image = URL.createObjectURL(buffer);
              //       const { loadedCount } = this.state;
              //       this.setState({
              //         loadedCount: loadedCount + 1,
              //       });
              //     });
              //   return frame;
              // });
            },
          );
        },
        error => {
          console.log('error loading texture data: ', dataSrc, error);
        },
      );
  };

  getPosition = (): number => {
    const { currentFrame } = this.state;
    const { frames } = this;
    return (100 * currentFrame) / frames.length;
  };

  updatePosition = (timeStamp: number) => {
    if (!this.lastTimeStamp) {
      this.lastTimeStamp = timeStamp;
    }
    const { currentFrame } = this.state;
    const {
      onAnimationPlayedStart,
      onAnimationPlayedEnd,
      state,
      onAfterFrame,
      onBeforeFrame,
      duration,
    } = this.props;
    const frameInterval = duration
      ? (duration * 1000) / this.frames.length
      : 1000 / this.fps;

    if (currentFrame === this.targetFrameIndex()) {
      cancelAnimationFrame(this.animationId);
      this.animationId = null;
      if (state === 0) {
        onAnimationPlayedStart && onAnimationPlayedStart();
      } else {
        onAnimationPlayedEnd && onAnimationPlayedEnd();
      }
      return;
    }

    if (timeStamp - this.lastTimeStamp >= frameInterval) {
      this.lastTimeStamp = timeStamp;
      const direction = this.targetFrameIndex() > currentFrame ? 1 : -1;
      const targetFrame =
        duration === 0 ? this.targetFrameIndex() : currentFrame + direction;
      onBeforeFrame && onBeforeFrame(currentFrame);
      this.setState(
        {
          currentFrame: targetFrame,
        },
        () => {
          onAfterFrame && onAfterFrame(currentFrame - direction);
        },
      );
    }
    requestAnimationFrame(this.updatePosition);
  };

  render() {
    const { onReady, className, dataSrc, children, ...other } = this.props;
    const { currentFrame, loadedCount, dataLoaded, ready } = this.state;

    if (dataLoaded && !ready) {
      if (loadedCount === this.frames.length - 1) {
        this.setState(
          {
            ready: true,
          },
          () => {
            onReady && onReady();
          },
        );
      }
    }
    if (!this.animationId && ready) {
      if (currentFrame !== this.targetFrameIndex()) {
        this.animationId = window.requestAnimationFrame(this.updatePosition);
      }
    }

    return (
      <E.Wrapper className={className} {...other}>
        {children}
        {!isMobile() && (
          <>
            <E.Poster dataLoaded={ready} src={this.posterSrc} alt="poster" />
            {dataLoaded && (
              <E.FramesWrapper>
                <E.Frames
                  style={{
                    transform: `translateY(-${this.getPosition()}%)`,
                  }}
                >
                  {this.frames.map((el, i) => {
                    return (
                      <E.Frame
                        onLoad={() => {
                          const { loadedCount: loadedCountt } = this.state;
                          this.setState({
                            loadedCount: loadedCountt + 1,
                          });
                        }}
                        src={path.join(path.dirname(dataSrc), el.image)}
                        key={i}
                      />
                    );
                  })}
                </E.Frames>
              </E.FramesWrapper>
            )}
          </>
        )}
        {isMobile() && dataLoaded && (
          <E.MobilePoster
            src={path.join(
              path.dirname(dataSrc),
              this.frames[this.frames.length - 1].image,
            )}
            onLoad={() => {
              onReady && onReady();
            }}
            alt="poster"
          />
        )}
      </E.Wrapper>
    );
  }
}

export default Animation;
