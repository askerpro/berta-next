import React from 'react';
import { Link, Button } from 'layout/elements';
import { vars as headerVars } from 'layout/header/elements';
import { withRouter, SingletonRouter } from 'next/router';
import Hidden from '@material-ui/core/Hidden';
import * as E from './elements';
import MobilePageNav from './mobile';
import { ISection } from '..';

interface State {
  isOnTop: boolean;
}
interface Props {
  title: string;
  activeSection: number;
  sections: Array<ISection>;
  onClick(index: number): void;
}

export interface WithRouterProps extends Props {
  router: SingletonRouter;
}

class PageNav extends React.Component<WithRouterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      isOnTop: true,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    const { isOnTop } = this.state;
    const scrollTop = window.scrollY;
    if (scrollTop > headerVars.height && isOnTop === true) {
      this.setState({
        isOnTop: false,
      });
    }
    if (scrollTop <= headerVars.height && isOnTop === false) {
      this.setState({
        isOnTop: true,
      });
    }
  };

  HeaderLink = () => {
    const { title, activeSection, onClick } = this.props;
    return (
      <E.AnchorLink
        onClick={() => {
          onClick(0);
        }}
        active={activeSection === 0}
        color="textPrimary"
        variant="h6"
      >
        {title}
      </E.AnchorLink>
    );
  };

  MenuItems = () => {
    const { sections, activeSection, onClick } = this.props;
    const titles = sections.map(Section => Section.shortTitle);
    return sections
      .filter((Section, i, arr) => {
        return (
          titles.indexOf(Section.shortTitle) === i &&
          titles.indexOf(Section.shortTitle) !== 0 &&
          i !== arr.length - 1
        );
      })
      .map((Section, i, arr) => {
        return (
          <E.AnchorLink
            onClick={() => {
              onClick(titles.indexOf(Section.shortTitle));
            }}
            active={titles.indexOf(Section.shortTitle) === activeSection}
            key={i}
            color="textPrimary"
          >
            {Section.shortTitle}
          </E.AnchorLink>
        );
      });
  };

  render() {
    const { isOnTop } = this.state;
    const { sections, onClick } = this.props;
    return (
      <E.Wrapper scrolled={!isOnTop}>
        <E.Container>
          {this.HeaderLink()}
          <E.menuItems>
            {this.MenuItems()}
            <Button
              onClick={() => {
                onClick(sections.length - 1);
              }}
              variant="contained"
              color="secondary"
              size="small"
            >
              Рассчитать стоимость
            </Button>
          </E.menuItems>
        </E.Container>
        <E.BorderContainer>
          <E.Border />
        </E.BorderContainer>
      </E.Wrapper>
    );
  }
}
const PageNavWithRouter = withRouter<WithRouterProps>(PageNav);

export default (props: Props) => (
  <>
    <Hidden smUp>
      <MobilePageNav {...props} />
    </Hidden>
    <Hidden mdDown>
      <PageNavWithRouter {...props} />
    </Hidden>
  </>
);
