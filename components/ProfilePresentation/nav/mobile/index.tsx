import React from 'react';
import { Button } from 'layout/elements';

import RoutesManager from 'lib/RoutesManager';
import Typography from '@material-ui/core/Typography';
import { withRouter } from 'next/router';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { WithRouterProps } from '..';
import * as E from './elements';

interface State {
  expanded: boolean;
}

class MobilePageNav extends React.Component<WithRouterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  MenuItems = () => {
    const { sections, activeSection } = this.props;
    return sections.slice(0, sections.length - 2).map((Section, i) => {
      const active = i === activeSection;
      return (
        <E.ListItem key={i} divider>
          <E.NavItem
            active={i === activeSection}
            color={active ? 'textPrimary' : 'textSecondary'}
            variant="button"
            onClick={() => {
              this.closePanel();
            }}
            href={`#${Section.id}`}
          >
            {Section.shortTitle}
          </E.NavItem>
        </E.ListItem>
      );
    });
  };

  switchPanel = () => {
    const { expanded } = this.state;
    this.setState({
      expanded: !expanded,
    });
  };

  closePanel = () => {
    this.setState({
      expanded: false,
    });
  };

  render() {
    const { expanded } = this.state;
    const { router, sections } = this.props;
    return (
      <>
        <E.Wrapper>
          <ClickAwayListener onClickAway={this.closePanel}>
            <ExpansionPanel expanded={expanded} onChange={this.switchPanel}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography variant="button">
                  {RoutesManager.getName(router.pathname)}
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <E.List>
                  {this.MenuItems()}
                  <E.ListItem>
                    <Button
                      href={`#${sections[sections.length - 1].id}`}
                      variant="contained"
                      color="secondary"
                      fullWidth
                      onClick={() => {
                        this.closePanel();
                      }}
                    >
                      Рассчитать стоимость
                    </Button>
                  </E.ListItem>
                </E.List>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </ClickAwayListener>
        </E.Wrapper>
      </>
    );
  }
}
export default withRouter<WithRouterProps>(MobilePageNav);
