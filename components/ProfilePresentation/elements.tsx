import styled, { css } from 'layout/theme';
import { keyframes } from 'styled-components';
import { Section as S } from 'layout/elements';
import Animation from 'components/Animation';
import Grid, { GridProps } from '@material-ui/core/Grid';
import Icon from 'layout/elements/Icon';
import React from 'react';

export const vars = {
  transitionDuration: 0.7,
};

interface SectionProps {
  bgColor?: string;
  active: boolean;
  changing: boolean;
}

export const Section = styled(S.Wrapper)<SectionProps>`
  z-index: ${({ active }) => (active ? 1 : -1)};
  visibility: ${({ active }) => (active ? 'visible' : 'hidden')};
  min-height: calc(100vh - 60px);
  position: absolute;
  width: 100%;
  ${({ bgColor }) =>
    bgColor &&
    css`
      background-color: bgColor;
    `};

  ${({ changing }) =>
    changing &&
    css`
      ${Content} {
        max-height: 0px;
      }
    `};
  @media (max-width: 768px) {
    z-index: unset;
    visibility: unset;
    position: relative;
  }
`;

export const Container = styled(S.Container)`
  display: flex;
  position: relative;
  justify-content: flex-end;
  padding-top: 0;
  padding-bottom: 0;
  @media (max-width: 768px) {
    display: block;
  }
`;

export const Image = styled(Animation)`
  max-height: calc(100vh - 60px);
  height: 100%;
  position: sticky;
  flex-shrink: 0;
  top: 60px;
  @media (max-width: 768px) {
    position: relative;
    top: unset;
  }
`;

export const Content = styled((props: GridProps) => (
  <Grid {...props} container spacing={4} />
))<GridProps>`
  max-width: 50%;
  transition: 0.7s;
  overflow: hidden;
  max-height: 2000px;
  flex-shrink: 0;
  padding-top: ${({ theme }) => theme.spacing(5)}px;
  padding-bottom: ${({ theme }) => theme.spacing(5)}px;
  height: fit-content;
  @media (max-width: 768px) {
    max-width: unset;
  }
`;

export const snowContainer = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
`;

const snowFall = keyframes`
    from {
        top: 0%;
    }
    to {
        top: 100%;
    }
`;
const snowShake = keyframes`
    0%{
        transform: translateX(0px);
    }

    50% {
        transform: translateX(-30px);
    }

    100% {
        transform: translateX(0px);
    }
`;

export const snowFlake = styled.div`
  :after {
    content: '❆';
  }
  top: -10%;
  position: absolute;
  transform: translateY(0vh);
  user-select: none;
  cursor: default;
  color: #fff;
  animation-name: ${snowFall}, ${snowShake};
  animation-duration: 10s, 3s;
  animation-timing-function: linear, ease-in-out;
  animation-iteration-count: infinite, infinite;
  animation-play-state: running, running;
`;

export const IconContentWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

export const IconContent = styled.div`
  margin-right: 60px;
  margin-bottom: 40px;
  @media (max-width: 768px) {
    margin-right: 20px;
  }
`;
export const IconLabel = styled.div`
  font-size: 14px;
  font-weight: 300;
  color: #ccc;
  max-width: 85px;
  margin-right: 30px;
`;
export const IconHeroValue = styled.div`
  display: flex;
  font-size: 60px;
  font-weight: 300;
  color: #fff;
  line-height: 56px;
  height: 65px;
`;
export const IconHero = styled(Icon)`
  height: 65px;
  width: auto;
  padding: 7%;
`;

export const IconHeroUnit = styled.div`
  font-size: 24px;
  font-weight: 300;
  color: #fff;
  height: 24px;
  line-height: 90px;
`;
