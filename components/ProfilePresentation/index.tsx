/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { Theme } from '@material-ui/core/styles';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/styles';
import Hidden from '@material-ui/core/Hidden';
import PageNav from './nav';

export interface ISectionComponent {
  index: number;
  current: number;
  changing: boolean;
  animationState: 0 | 1;
  goToNext(): void;
  goToPrev(): void;
  first?: boolean;
  last?: boolean;
  onAnimationPlayedStart?(): void;
  onAnimationPlayedEnd?(): void;
  getHeight?(height: number): void;
}

export interface ISection {
  shortTitle: string;
  id: string;
  view: React.ComponentType<ISectionComponent>;
}

interface Props {
  sections: Array<ISection>;
  theme: Theme;
  title: string;
}

interface State {
  activeSection: number;
  targetActiveSection: number;
  changing: boolean;
  containerHeight: number;
  onAnimationPlayedStart?(): void;
  onAnimationPlayedEnd?(): void;
}

export const transitionDuration = 0.7;

export default class extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      containerHeight: 0,
      activeSection: 0,
      targetActiveSection: 0,
      changing: false,
      onAnimationPlayedStart: null,
    };
  }

  animationState = (i: number): 0 | 1 => {
    const { activeSection, targetActiveSection } = this.state;
    if (i < activeSection) {
      return 1;
    }
    if (i > activeSection) {
      return 0;
    }

    if (
      targetActiveSection !== activeSection &&
      targetActiveSection < activeSection
    ) {
      return 0;
    }
    return 1;
  };

  render() {
    const { sections, theme, title } = this.props;
    const {
      activeSection,
      changing,
      targetActiveSection,
      onAnimationPlayedStart,
      onAnimationPlayedEnd,
      containerHeight,
    } = this.state;

    if (activeSection !== targetActiveSection) {
      this.setState(
        {
          changing: true,
        },
        () => {
          if (targetActiveSection > activeSection) {
            setTimeout(() => {
              this.setState({
                activeSection: targetActiveSection,
                onAnimationPlayedEnd: () => {
                  this.setState({
                    changing: false,
                    onAnimationPlayedStart: null,
                  });
                },
              });
            }, transitionDuration * 1000);
          } else if (!onAnimationPlayedStart) {
            this.setState({
              onAnimationPlayedStart: () => {
                this.setState(
                  {
                    activeSection: targetActiveSection,
                    onAnimationPlayedStart: null,
                  },
                  () => {
                    setTimeout(() => {
                      this.setState({
                        changing: false,
                      });
                    }, 10);
                  },
                );
              },
            });
          }
        },
      );
    }

    return (
      <MuiThemeProvider theme={theme}>
        <StyledThemeProvider theme={theme}>
          <>
            <PageNav
              title={title}
              sections={sections}
              activeSection={activeSection}
              onClick={(i: number) => {
                this.setState({ targetActiveSection: i });
              }}
            />

            {sections.map((Section, i) => (
              <Section.view
                getHeight={height => {
                  if (i === activeSection) {
                    this.setState({
                      containerHeight: height,
                    });
                  }
                }}
                animationState={this.animationState(i)}
                changing={changing}
                key={i}
                first={i === 0}
                last={i === sections.length - 1}
                index={i}
                current={activeSection}
                goToNext={() => {
                  this.setState({ targetActiveSection: i + 1 });
                }}
                goToPrev={() => {
                  this.setState({ targetActiveSection: i - 1 });
                }}
                onAnimationPlayedStart={onAnimationPlayedStart}
                onAnimationPlayedEnd={onAnimationPlayedEnd}
              />
            ))}
            <Hidden mdDown>
              <div style={{ height: `${containerHeight.toString()}px` }} />
            </Hidden>
          </>
        </StyledThemeProvider>
      </MuiThemeProvider>
    );
  }
}
