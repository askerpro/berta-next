import { Section } from 'layout/elements';
import { IReview } from 'data/reviews';
import { Slider, Slide } from 'pure-react-carousel';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import React from 'react';
import { White } from 'layout/theme';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { Hidden, Button } from '@material-ui/core';
import ReviewCard from './ReviewCard';
import 'components/slider/styles.css';
import * as E from './elements';

const url = '/api/reviews';
interface IReviewSection {
  title: string;
}
interface State {
  reviews: IReview[];
}
const date = new Date();

class ReviewsSection extends React.Component<IReviewSection, State> {
  constructor(props) {
    super(props);
    this.state = {
      reviews: new Array<IReview>(),
    };
  }

  componentDidMount() {
    fetch(url)
      .then(result => result.json())
      .then((Freviews: IReview[]) => {
        this.setState({
          reviews: Freviews.sort((review1, review2) => {
            const date1 = new Date(review1.date);
            const date2 = new Date(review2.date);
            return date1.getMilliseconds() >= date2.getMilliseconds() ? 1 : -1;
          }),
        });
      });
  }

  render() {
    const { reviews } = this.state;
    const { title } = this.props;
    return (
      <White>
        <Section.Wrapper>
          <Section.Container>
            <Section.Header fullWidth center>
              <Section.Title variant="h3" component="h2">
                {title}
              </Section.Title>
              <Section.Subtitle>
                {`Обновлено ${date.getDate() - 1}.${date.getMonth() +
                  1}.${date.getFullYear()}г.`}
              </Section.Subtitle>
            </Section.Header>
            <Hidden smUp>
              <E.Carousel
                naturalSlideWidth={100}
                naturalSlideHeight={145}
                totalSlides={reviews.length}
                visibleSlides={1}
                isPlaying
                interval={2500}
              >
                <Slider>
                  {reviews.map((review, i) => (
                    <Slide index={i} key={i}>
                      <E.SlideWrapper>
                        <ReviewCard
                          mediaSrc={review.url}
                          region={review.region}
                          location={review.location}
                          date={review.date}
                          preview={review.preview}
                        />
                      </E.SlideWrapper>
                    </Slide>
                  ))}
                </Slider>
                <E.ArrowPrev>
                  <KeyboardArrowLeft />
                </E.ArrowPrev>
                <E.ArrowNext>
                  <KeyboardArrowRight />
                </E.ArrowNext>
              </E.Carousel>
            </Hidden>
            <Hidden smDown>
              <E.Carousel
                naturalSlideWidth={100}
                naturalSlideHeight={120}
                totalSlides={reviews.length}
                visibleSlides={3}
                isPlaying
                interval={2500}
              >
                <Slider>
                  {reviews.map((review, i) => (
                    <Slide index={i} key={i}>
                      <E.SlideWrapper>
                        <ReviewCard
                          preview={review.preview}
                          mediaSrc={review.url}
                          region={review.region}
                          location={review.location}
                          date={review.date}
                        />
                      </E.SlideWrapper>
                    </Slide>
                  ))}
                </Slider>
                <E.ArrowPrev>
                  <KeyboardArrowLeft />
                </E.ArrowPrev>
                <E.ArrowNext>
                  <KeyboardArrowRight />
                </E.ArrowNext>
              </E.Carousel>
            </Hidden>
            <Section.Body>
              <Section.Header fullWidth center>
                <Button variant="contained" href="/about-us/reviews">
                  Больше отзывов
                </Button>
              </Section.Header>
            </Section.Body>
          </Section.Container>
        </Section.Wrapper>
      </White>
    );
  }
}

export default ReviewsSection;
