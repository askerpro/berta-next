import {
  ButtonBack,
  ButtonNext,
  CarouselProvider,
  Slide,
} from 'pure-react-carousel';

import styled from 'layout/theme';

export const Carousel = styled(CarouselProvider)`
  position: relative;
`;

export const ArrowNext = styled(ButtonNext)`
  position: absolute;
  top: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translate3d(0, -50%, 0);
  right: 0;
`;

export const ArrowPrev = styled(ButtonBack)`
  position: absolute;
  top: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translate3d(0, -50%, 0);
`;

export const SlideWrapper = styled.div`
  margin: 16px;
`;
