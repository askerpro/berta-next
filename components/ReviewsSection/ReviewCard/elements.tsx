import styled from 'layout/theme';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogActions from '@material-ui/core/DialogActions';
import MuiCard from '@material-ui/core/Card';
import { PlayCircleFilled } from '@material-ui/icons';
import MuiCardMedia from '@material-ui/core/CardMedia';
import {
  CardActionArea as CardActionAreaT,
  CardContent as CardContentT,
} from '@material-ui/core';

export const CardMedia = styled(MuiCardMedia)`
  position: relative;
  height: 430px;
`;

export const PlayButton = styled(PlayCircleFilled)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate3d(-50%, -50%, 0);
  display: block;
  font-size: 60px;
  z-index: 1;
  color: #fff;
`;

export const Wrapper = styled(MuiCard)`
  margin: 2px auto;
  position: relative;
  height: 100%;
  width: 100%;
`;

export const DialogTitle = styled(MuiDialogTitle)`
  margin: 0;
  padding: ${({ theme }) => theme.spacing(2)}px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const DialogFooter = styled(MuiDialogActions)`
  margin: 0;
  padding: ${({ theme }) => theme.spacing(2)}px;
`;

export const Video = styled.video`
  width: 100%;
  height: 100%;
  max-height: 100%;
  max-width: 100%;
  object-fit: cover;
`;

export const CardActionArea = styled(CardActionAreaT)``;

export const CardContent = styled(CardContentT)`
  position: absolute;
  z-index: 1;
  bottom: 0;
  background: #fff;
  width: 100%;
`;

export default {};
