import CardContent from '@material-ui/core/CardContent';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import React from 'react';

import DialogTitle from 'layout/elements/Dialog/Title';
import * as E from './elements';

interface Props {
  mediaSrc: string;
  date: string;
  location: string;
  region: string;
  preview: string;
}

interface State {
  isPlaying: boolean;
}

class ReviewCard extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: false,
    };
  }

  handleClose = () => {
    this.setState({
      isPlaying: false,
    });
  };

  handleOpen = () => {
    this.setState({
      isPlaying: true,
    });
  };

  Media = () => {
    const { mediaSrc, preview } = this.props;
    const { isPlaying } = this.state;
    return (
      <E.Video
        autoPlay={isPlaying}
        poster={preview} //"/static/img/video/loading.gif"
        preload="none"
      >
        <source src={mediaSrc} />
        <track kind="captions" />
      </E.Video>
    );
  };

  render() {
    const { handleClose, handleOpen, Media } = this;
    const { location, date, region, preview } = this.props;
    const { isPlaying } = this.state;

    return (
      <E.Wrapper>
        <Dialog
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={isPlaying}
        >
          <DialogTitle onClose={handleClose}>
            {`${location} - ${date}`}
          </DialogTitle>
          <DialogContent dividers>
            <Media />
          </DialogContent>
          <E.DialogFooter>
            <Typography variant="body1">{region}</Typography>
          </E.DialogFooter>
        </Dialog>
        <E.CardActionArea onClick={handleOpen}>
          <E.CardMedia image={preview}>
            <Media />
          </E.CardMedia>
          <E.CardContent>
            <Typography variant="body1" color="textPrimary">
              {location}
            </Typography>
            <Typography variant="body1" color="textSecondary">
              {date}
            </Typography>
            <Typography variant="body1" color="textSecondary">
              {region}
            </Typography>
          </E.CardContent>
        </E.CardActionArea>
      </E.Wrapper>
    );
  }
}

export default ReviewCard;
