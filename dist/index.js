"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const next_1 = __importDefault(require("next"));
const maildev_1 = __importDefault(require("maildev"));
const body_parser_1 = __importDefault(require("body-parser"));
const config_1 = __importDefault(require("./config"));
const api_1 = __importDefault(require("./api"));
console.log(process.env.NODE_ENV);
const mode = process.env.NODE_ENV;
const app = next_1.default({ dev: process.env.NODE_ENV === 'development' });
const handle = app.getRequestHandler();
app.prepare().then(() => {
    const server = express_1.default();
    server.use(body_parser_1.default.json());
    if (mode === 'development') {
        const maildev = new maildev_1.default({
            smtp: config_1.default.mail.smtpPort,
            ip: config_1.default.mail.smtpHost,
        });
        maildev.listen();
    }
    api_1.default(server);
    server.get('*', (req, res) => {
        return handle(req, res);
    });
    server.listen(config_1.default.network.port, err => {
        if (err)
            throw err;
        console.log(`> Ready on http://localhost:${config_1.default.network.port}`);
    });
});
