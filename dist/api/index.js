"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sendmail_1 = __importDefault(require("./sendmail"));
const reviews_1 = __importDefault(require("./reviews"));
exports.default = (app) => {
    sendmail_1.default(app);
    reviews_1.default(app);
};
