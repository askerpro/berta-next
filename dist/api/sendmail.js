"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_1 = __importDefault(require("nodemailer"));
const config_1 = __importDefault(require("../config"));
const transporter = nodemailer_1.default.createTransport({
    host: config_1.default.mail.smtpHost,
    port: config_1.default.mail.smtpPort,
    secure: false,
    tls: {
        rejectUnauthorized: false,
    },
});
const send = async (req, res) => {
    console.log(req.body);
    const mailOptions = {
        from: '"Сайт Окна Берта" <calc@okna-berta.ru>',
        to: config_1.default.mail.to,
        subject: req.body.subject,
        html: req.body.message,
    };
    const info = await transporter.sendMail(mailOptions);
    console.log(info);
    const err = false;
    if (err) {
        res.send({ error: err });
    }
    else {
        console.log('Успешно отправлено');
        res.send({});
    }
};
exports.default = (app) => {
    app.post('/api/sendmail', send);
};
