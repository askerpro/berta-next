"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios = require('axios');
const get = async (req, res) => {
    axios.get('https://ok-crm.ru/api/site/reviews.php')
        .then(response => {
        res.send(response.data);
    })
        .catch(error => {
        console.log(error);
    });
    ;
};
exports.default = (app) => {
    app.get('/api/reviews', get);
};
