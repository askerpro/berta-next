"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const configs = {
    production: {
        network: {
            port: 8080,
            ip: 'localhost',
        },
        db: 'mongodb://berta:berta24@ds239071.mlab.com:39071/berta',
        mail: {
            to: 'support@okna-berta.ru',
            smtpPort: 25,
            smtpHost: 'localhost',
        },
    },
    development: {
        network: {
            port: 8080,
            ip: '0.0.0.0',
        },
        db: 'mongodb://berta:berta24@ds217092.mlab.com:17092/berta_test',
        mail: {
            to: 'askerpro@gmail.com',
            smtpPort: 25,
            smtpHost: 'localhost',
        },
    },
};
const mode = process.env.NODE_ENV;
exports.default = configs[mode];
