const env = process.env.NODE_ENV || 'development';

interface IConfig {
  ym: number;
}

const configs = {
  development: {
    ym: 56768515,
  },
  staging: {
    ym: 41735104,
  },
  production: {
    ym: 41735104,
  },
}[env] as IConfig;

export default configs;
