import React from 'react';
import Head from 'next/head';

export interface Props {
  title: string;
  description: string;
}

export default ({ title, description }: Props) => (
  <Head>
    <title>{`${title} - Окна Берта`}</title>
    <meta name="description" content={description} />
  </Head>
);
