/* eslint-disable react/no-unused-state */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import Head from 'next/head';
import VisibilitySensor from 'react-visibility-sensor';
import { PageNav } from 'layout/elements';

export interface IHead {
  title: string;
  description: string;
}

export interface IPageSection {
  title: string;
  id: string;
  view: React.ComponentType<any>;
}

export interface IProps {
  head: IHead;
  sections?: Array<IPageSection>;
}

export interface IState {
  activeSection: number;
  documentHeight: number;
}

export class Page extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      activeSection: 0,
      documentHeight: 0,
    };
  }

  componentDidMount() {
    this.setState({
      documentHeight: document.documentElement.clientHeight,
    });
  }

  Sections = () => {
    const { sections } = this.props;
    return sections.map((Section, i) => {
      const { documentHeight } = this.state;
      return (
        <VisibilitySensor
          onChange={isVisible => {
            const { activeSection } = this.state;
            if (isVisible && activeSection !== i)
              this.setState({
                activeSection: i,
              });
          }}
          partialVisibility
          offset={{ top: documentHeight / 2 }}
          minTopValue={documentHeight / 2 + 50}
          key={i}
        >
          <Section.view />
        </VisibilitySensor>
      );
    });
  };

  render() {
    const { head, sections, children } = this.props;
    const { title, description } = head;
    const { Sections } = this;
    const { activeSection } = this.state;
    return (
      <>
        <Head>
          <title>{`${title} - Окна Берта`}</title>
          <meta name="description" content={description} />
        </Head>
        {sections && (
          <>
            <PageNav activeSection={activeSection} sections={sections} />
            {Sections()}
          </>
        )}
        {children}
      </>
    );
  }
}

export default Page;
