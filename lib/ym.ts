import config from '../env_config';

enum goals {
  CALCSUBMIT = 'CALC_CLICK',
  CALL = 'CALL',
}

export const YMinit = () => {
  window.ym(config.ym, 'init', {
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true,
    webvisor: true,
  });
};

export const calcSubmitReached = () => {
  const { ym } = window;
  console.log('calc submit');
  ym(config.ym, 'reachGoal', goals.CALCSUBMIT);
};

export const callReached = () => {
  const { ym } = window;
  console.log('call goal reached ', ym);
  ym(config.ym, 'reachGoal', goals.CALL);
};
