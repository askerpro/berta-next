// export interface DeviceDetectHoFProps {
//   isMobile: boolean;
// }
// export function withDeviceDetect<T>(Child: React.ComponentType<T>) {
//   return class extends React.Component<DeviceDetectHoFProps & T, {}> {
//     render() {
//       const userAgent
//       return <Child {...this.props} onChange={this.onChangeHandler} />;
//     }
//   };
// }

let userAgent;
const isMobileRegExp = new RegExp(
  '/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/',
);

export default (_userAgent: string) => {
  userAgent = _userAgent;
};

export const isMobile = (): boolean => {
  if (userAgent) {
    console.log(userAgent);
    return isMobileRegExp.test(userAgent);
  }
  console.log(window.navigator.userAgent);
  return isMobileRegExp.test(window.navigator.userAgent);
};
