import { Service } from '_pages/services/order';

const routes = {
  title: 'Главная',
  children: {
    production: {
      title: 'Продукция',
      children: {
        profiles: {
          title: 'Профильные системы',
          children: {
            'silver-eco-premium': {
              title: 'Berta Silver Eco',
            },
          },
        },

        windows: {
          title: 'Пластиковые окна',
        },
        doors: {
          title: 'Пластиковые двери',
        },
        balconies: {
          title: 'Балконы',
        },
        designed: {
          title: 'Дизайнерские решения',
        },
        'how-to-order': {
          title: 'Как заказать пластиковые окна',
        },
      },
    },
    services: {
      title: 'Услуги',
      children: {
        [`order?serviceId=${Service.CALL_WORKER}`]: {
          title: 'Заказать бесплатный замер',
        },
        repair: {
          title: 'Ремонт окон',
        },
        balconies: {
          title: 'Остекление балконов',
        },
        cottages: {
          title: 'Остекление коттеджей',
        },
        order: {
          title: 'Сделать заказ',
        },

        all: {
          title: 'Список всех услуг',
        },
      },
    },
    prices: {
      title: 'Цены',
      children: {
        calc: {
          title: 'Калькулятор окон',
        },
        installments: {
          title: 'Рассрочка',
        },
      },
    },
    'about-us': {
      title: 'О нас',
    },
    contacts: {
      title: 'Контакты',
    },
  },
};

export default class RoutesManager {
  static routes = routes;

  static getName = (path: string, currentLvl = routes) => {
    const splittedRoute = path.split('/').slice(1);
    if (splittedRoute.length > 0) {
      return RoutesManager.getName(
        splittedRoute.join('/'),
        currentLvl.children[splittedRoute[0]],
      );
    }
    return currentLvl.title;
  };
}
