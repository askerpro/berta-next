import React from 'react';

export interface IPageSection {
  title: string;
  id: string;
  view: React.ComponentType<any>;
}

class PageSection {
  public title: string;

  public id: string;

  public view: React.ComponentType;

  constructor(params: IPageSection) {
    this.title = params.title;
    this.id = params.id;
    this.view = params.view;
  }
}

export default PageSection;
