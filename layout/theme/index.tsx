import React from 'react';
import {
  createMuiTheme,
  Theme,
  responsiveFontSizes,
} from '@material-ui/core/styles';
import baseStyled, {
  ThemedStyledInterface,
  ThemeProvider as StyledThemeProvider,
} from 'styled-components';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {
    iconBrightness?: number;
  }
}

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    iconBrightness: number;
  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    iconBrightness?: number;
  }
}
const commonThemeProps: ThemeOptions = {
  typography: {
    h1: {
      fontWeight: 400,
    },
    h2: {
      fontWeight: 400,
    },
  },
};
// Create a theme instance.
export const dark = responsiveFontSizes(
  createMuiTheme({
    ...commonThemeProps,
    palette: {
      primary: {
        main: '#212121',
      },
      secondary: {
        main: '#dd2c00',
      },

      type: 'dark',
    },
    iconBrightness: 1,
  }),
);

const lightThemeProps: ThemeOptions = {
  palette: {
    primary: {
      main: '#212121',
      contrastText: '#fff',
    },
    secondary: {
      main: '#fec109',
      contrastText: '#fafafa',
    },
    background: {
      default: '#f2f2f2',
    },
    type: 'light',
  },
  iconBrightness: 0.2,
};

export const light = responsiveFontSizes(
  createMuiTheme({
    ...commonThemeProps,
    ...lightThemeProps,
  }),
);
const whiteThemeProps: ThemeOptions = {
  ...lightThemeProps,
  palette: {
    background: {
      default: '#fff',
    },
  },
};

const white = responsiveFontSizes(
  createMuiTheme({
    ...commonThemeProps,
    ...whiteThemeProps,
  }),
);

export const blueGrey = responsiveFontSizes(
  createMuiTheme({
    ...commonThemeProps,
    palette: {
      primary: {
        main: '#cfd8dc',
      },
      secondary: {
        main: '#616161',
      },
      background: {
        default: '#cfd8dc',
      },
    },
    iconBrightness: 0.4,
  }),
);

export const ecoLight = responsiveFontSizes(
  createMuiTheme({
    ...commonThemeProps,
    palette: {
      primary: {
        main: '#eeeeee',
      },
      secondary: {
        main: '#8bc34a',
        contrastText: '#fafafa',
      },
    },
    iconBrightness: 0.2,
  }),
);

export const ecoDark = responsiveFontSizes(
  createMuiTheme({
    ...commonThemeProps,
    palette: {
      text: {
        secondary: '#7dba00',
        primary: '#fafafa',
      },
      primary: {
        main: '#eeeeee',
      },
      secondary: {
        main: '#8bc34a',
        contrastText: '#fff',
      },
      background: {
        default: '#141517',
      },
      type: 'dark',
    },
    iconBrightness: 1,
  }),
);

export const White = ({ children }) => (
  <MuiThemeProvider theme={white}>
    <StyledThemeProvider theme={white}>{children}</StyledThemeProvider>
  </MuiThemeProvider>
);

export const Light = ({ children }) => (
  <MuiThemeProvider theme={light}>
    <StyledThemeProvider theme={light}>{children}</StyledThemeProvider>
  </MuiThemeProvider>
);

export const Dark = ({ children }) => (
  <MuiThemeProvider theme={dark}>
    <StyledThemeProvider theme={dark}>{children}</StyledThemeProvider>
  </MuiThemeProvider>
);

export const Grey = ({ children }) => (
  <MuiThemeProvider theme={blueGrey}>
    <StyledThemeProvider theme={blueGrey}>{children}</StyledThemeProvider>
  </MuiThemeProvider>
);

export const EcoDark = ({ children }) => (
  <MuiThemeProvider theme={ecoDark}>
    <StyledThemeProvider theme={ecoDark}>{children}</StyledThemeProvider>
  </MuiThemeProvider>
);

export const EcoLight = ({ children }) => (
  <MuiThemeProvider theme={ecoLight}>
    <StyledThemeProvider theme={ecoLight}>{children}</StyledThemeProvider>
  </MuiThemeProvider>
);

export const styled = baseStyled as ThemedStyledInterface<Theme>;

export { css } from 'styled-components';

export default styled;
