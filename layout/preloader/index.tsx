import { connect } from 'react-redux';
import { AppState } from 'store';
import { doScrollable, doUnscrollable } from 'layout';
import Preloader from './component';

export const transitionTime = 0.7;

export enum Actions {
  SWITCH_STATE = 'SWITCH_STATE',
  SWITCH_CONTROL_TYPE = 'SWITCH_CT',
}

export enum OpenState {
  OPENED,
  CLOSED,
}

export enum ControlType {
  AUTO,
  MANUAL,
}

type ActionSwitchControlType = {
  type: Actions.SWITCH_CONTROL_TYPE;
  controlType: ControlType;
};
export const SwitchControlType = (type: ControlType) => dispatch => {
  dispatch({
    type: Actions.SWITCH_CONTROL_TYPE,
    controlType: type,
  });
};

type ActionSwitchState = {
  type: Actions.SWITCH_STATE;
  state: OpenState;
};

export const switchState = (state: OpenState) => dispatch => {
  if (state === OpenState.OPENED) {
    dispatch(doUnscrollable);
  } else {
    setTimeout(() => {
      dispatch(doScrollable);
    }, transitionTime * 1000);
  }
  dispatch({
    type: Actions.SWITCH_STATE,
    state,
  });
};

type Action = ActionSwitchControlType | ActionSwitchState;

interface State {
  openState: OpenState;
  controlType: ControlType;
}

export const initialState: State = {
  controlType: ControlType.AUTO,
  openState: OpenState.OPENED,
};

export const reducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case Actions.SWITCH_STATE:
      return { ...state, openState: action.state };

    case Actions.SWITCH_CONTROL_TYPE:
      return { ...state, controlType: action.controlType };

    default:
      return state;
  }
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(switchState(OpenState.CLOSED)),
  };
};

const mapStateToProps = (state: AppState) => {
  return {
    isOpen: state.preloader.openState === OpenState.OPENED,
    isManual: state.preloader.controlType === ControlType.MANUAL,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Preloader);
