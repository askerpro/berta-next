import styled, { css } from 'layout/theme';
import { keyframes } from 'styled-components';

interface PreloaderProps {
  isActive: boolean;
}
export const Preloader = styled.div<PreloaderProps>`
  position: fixed;
  height: 0px;
  top: 0;
  width: 100vw;
  overflow: hidden;
  z-index: 501;
  pointer-events: none;
  background-color: #1a1a1a;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: 0.7s;
  ${({ isActive }) =>
    isActive &&
    css`
      height: 100vh;
      ${LogoWrapper} {
        transform: scale(1) translate3d(-50%, -50%, 0);
        height: 98px;
        left: 50%;
        top: 50%;
        ${({ theme }) => `${theme.breakpoints.down('sm')} {
          
          height: 47px;
        }`}
      }
      ${Region} {
        animation: ${RegionAppear} 1s forwards;
      }
      ${RegionTitle} {
        animation: ${RegionTitleAppear} 1s forwards;
      }
    `}
`;
export const Map = styled.svg`
  max-width: 1000px;
  margin: 0 auto;
  display: block;
  height: 100%;
`;

export const LogoWrapper = styled.div`
  transform: scale(1) translate3d(calc(12px), 14px, 0);
  height: 30px;
  background: #1a1a1a;
  position: absolute;
  left: 0;
  top: 0;
  transition: 0.7s;
`;

export const Regions = styled.g`
  stroke-width: 0.5;
`;

const RegionAppear = keyframes`
  to{
    stroke: #ffffff;
  }
`;

export const Region = styled.path`
  stroke: #1a1a1a;
  fill: #1a1a1a;
`;

export const RegionTitles = styled.g`
  font-size: 9px;
`;
const RegionTitleAppear = keyframes`
  to{
    fill: #fff;
  }
`;
export const RegionTitle = styled.text`
  fill: #1a1a1a;
`;
