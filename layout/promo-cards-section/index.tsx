import React from 'react';
import styled, { Light } from 'layout/theme';
import { Section } from 'layout/elements';
import { Promo as WindowsPromo } from '_pages/production/windows';
import { Promo as DoorsPromo } from '_pages/production/doors';
import { Promo as DesignedPromo } from '_pages/production/designed';
import { Promo as BalconiesPromo } from '_pages/production/balconies';
import { Promo as PanoramicPromo } from '_pages/production/designed/sections/panoramic';
import { Promo as ColoredPromo } from '_pages/production/designed/sections/colored';
import { Promo as CottagesPromo } from '_pages/services/cottages';
import { Promo as RepairPromo } from '_pages/services/repair';
import { Promo as ProfilesPromo } from '_pages/production/profiles';
import { Promo as AboutUsPromo } from '_pages/about-us';
import { Promo as CalcPromo } from '_pages/prices/calc';
import { Promo as HowToORderPromo } from '_pages/production/how-to-order';
import Grid from '@material-ui/core/Grid';

const promos = [
  HowToORderPromo,
  ProfilesPromo,
  CalcPromo,
  RepairPromo,
  AboutUsPromo,
  WindowsPromo,  
  BalconiesPromo,
  DoorsPromo,
  PanoramicPromo,
  ColoredPromo,
  DesignedPromo,
  CottagesPromo,
];

const Wrapper = styled(Section.Wrapper)``;

export const PromoCardWrapper = styled(Grid)`
  width: 100%;
`;

export default () => (
  <Light>
    <Wrapper>
      <Section.Wrapper>
        <Section.Container style={{ maxWidth: '100%' }}>
          <Section.HeaderT title="Продукция и услуги" subtitle="" center />
          <Section.Body>
            <Grid container spacing={3}>
              {promos.map((PromoCard, index) => (
                <PromoCardWrapper item xs={12} sm={3} key={index}>
                  <PromoCard />
                </PromoCardWrapper>
              ))}
            </Grid>
          </Section.Body>
        </Section.Container>
      </Section.Wrapper>
    </Wrapper>
  </Light>
);
