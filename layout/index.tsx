import React from 'react';
import styled, { Light } from 'layout/theme';
import { connect } from 'react-redux';
import { AppState } from 'store';
import SpeedDial from 'layout/speed-dial';
import Footer from './footer';
import Header from './header';

export enum Actions {
  DO_UNSCROLLABLE = 'SET_APP_UNSCROLLABLE',
  DO_SCROLLABLE = 'SET_APP_SCROLLABLE',
}

enum scrollableState {
  UNSCROLLABLE,
  SCROLLABLE,
}

export const doScrollable = {
  type: Actions.DO_SCROLLABLE,
};

export const doUnscrollable = {
  type: Actions.DO_UNSCROLLABLE,
};

type ActionType = typeof doScrollable | typeof doUnscrollable;

export const initialState: scrollableState = scrollableState.SCROLLABLE;

export const reducer = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case Actions.DO_SCROLLABLE:
      return scrollableState.SCROLLABLE;

    case Actions.DO_UNSCROLLABLE:
      return scrollableState.UNSCROLLABLE;

    default:
      return state;
  }
};

const mapStateToProps = (state: AppState) => {
  return {
    scrollable: state.appScrollable === scrollableState.SCROLLABLE,
  };
};

interface IWrapper {
  scrollable: boolean;
}
const Wrapper = styled.div<IWrapper>`
  height: ${({ scrollable }) => (scrollable ? 'auto' : '100vh')};
  width: ${({ scrollable }) => (scrollable ? 'auto' : '100vw')};
  overflow: ${({ scrollable }) => (scrollable ? 'unset' : 'hidden')};
`;

const Layout = ({ children, scrollable }) => {
  return (
    <Wrapper scrollable={scrollable}>
      <Header border="content" />
      <Light>
        <main style={{ minHeight: 'calc(100vh - 105px)' }}>{children}</main>
      </Light>
      <Footer />
      <SpeedDial />
    </Wrapper>
  );
};

export default connect(
  mapStateToProps,
  {},
)(Layout);
