import styled from 'layout/theme';
import MuiDialogTitle from '@material-ui/core/DialogTitle';

export const DialogTitle = styled(MuiDialogTitle)`
  margin: 0;
  padding: ${({ theme }) => theme.spacing(1)}px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export default DialogTitle;
