import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import * as E from './elements';

export interface DialogTitleProps {
  onClose: () => void;
}

export const DialogTitle: React.FC<DialogTitleProps> = props => {
  const { children, onClose } = props;
  return (
    <E.DialogTitle disableTypography>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </E.DialogTitle>
  );
};

export default DialogTitle;
