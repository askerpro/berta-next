import { Button } from '@material-ui/core';
import React from 'react';
import * as E from './elements';

export interface DialogActionProps {
  onClose?: () => void;
}

export const DialogTitle: React.FC<DialogActionProps> = props => {
  const { children, onClose } = props;
  return (
    <E.DialogActions>
      {onClose ? <Button onClick={onClose}>Закрыть</Button> : null}
      {children}
    </E.DialogActions>
  );
};

export default DialogTitle;
