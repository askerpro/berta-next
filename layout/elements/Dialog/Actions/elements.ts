import styled from 'layout/theme';
import MuiDialogActions from '@material-ui/core/DialogActions';

export const DialogActions = styled(MuiDialogActions)`
  margin: 0;
  padding: ${({ theme }) => theme.spacing(1)}px;
  align-items: center;
`;

export default DialogActions;
