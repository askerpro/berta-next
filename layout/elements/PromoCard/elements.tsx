import CardMediaT, { CardMediaProps } from '@material-ui/core/CardMedia';
import CardT, { CardProps } from '@material-ui/core/Card';
import styled, { css } from 'layout/theme';
import React from 'react';

interface ICardMedia extends CardMediaProps {
  contain?: boolean;
  video?: boolean;
}
export const CardMedia = styled(({ contain, video, ...other }) => (
  <CardMediaT {...other} />
))<ICardMedia>`
  padding-top: 56%;
  background-size: ${({ contain }) => (contain ? 'contain' : 'cover')};
  object-fit: ${({ contain }) => (contain ? 'contain' : 'cover')};
  image-rendering: crisp-edges;
  image-rendering: -webkit-optimize-contrast;
  ${({ video }) =>
    video &&
    css`
      padding-top: unset;
      height: 56%;
    `}
`;
interface ICard extends CardProps {
  vertical?: boolean;
}
export const Card = styled(({ vertical, ...other }) => <CardT {...other} />)<
  ICard
>`
  display: ${({ vertical }) => (vertical ? 'flex' : 'block')};
`;

export default {};
