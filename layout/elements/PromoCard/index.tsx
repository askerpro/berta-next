import React from 'react';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link } from 'layout/elements';
import * as E from './elements';

interface Props {
  title: string;
  desc: string;
  href: string;
  image: string;
  contain?: boolean;
  vertical?: boolean;
  reversed?: boolean;
  video?: boolean;
}

export default ({
  title,
  desc,
  href,
  image,
  contain,
  vertical,
  reversed,
  video,
}: Props) => {
  const CardContentContainer = (
    <CardContent>
      <Typography gutterBottom variant="h5" component="p">
        {title}
      </Typography>
      <Typography variant="body2" color="textSecondary" component="p">
        {desc}
      </Typography>
    </CardContent>
  );
  const CardMediaContainer = (
    <E.CardMedia
      src={image}
      component={video ? 'video' : 'div'}
      image={image}
      title={title}
      contain={contain}
      autoPlay={video}
      loop={video}
      muted={video}
      video={video}
      async
    />
  );

  const ReversedCardContent = (
    <>
      {CardContentContainer}
      {CardMediaContainer}
    </>
  );

  const NotReversedCardContent = (
    <>
      {CardMediaContainer}
      {CardContentContainer}
    </>
  );

  return (
    <Link href={href} underline="none">
      <E.Card vertical={vertical}>
        <CardActionArea>
          {reversed ? ReversedCardContent : NotReversedCardContent}
        </CardActionArea>
      </E.Card>
    </Link>
  );
};
