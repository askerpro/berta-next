import styled, { css } from 'layout/theme';
import React from 'react';
import { Container as ContainerT, Typography } from '@material-ui/core';
import { TypographyProps } from '@material-ui/core/Typography';

interface WrapperProps {
  fullHeight?: boolean;
  noMargin?: boolean;
}
export const Wrapper = styled.section<WrapperProps>`
  position: relative;
  background-color: ${({ theme }) => theme.palette.background.default};
  margin-bottom: ${({ noMargin }) => (noMargin ? 0 : '30px')};
  color: ${({ theme }) => theme.palette.text.primary};
  ${({ fullHeight }) =>
    fullHeight &&
    css`
      min-height: calc(100vh - 72px - 44px);
    `};
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    margin-bottom: 0px;
  }`}
`;

export const Container = styled(ContainerT)`
  padding-top: 40px;
  padding-bottom: 40px;
  position: relative;
`;

export const Header = styled.div<{
  center?: boolean;
  fullWidth?: boolean;
}>`
  margin-bottom: ${({ theme }) => theme.spacing(4)}px;
  max-width: ${({ fullWidth }) => (fullWidth ? '100%' : '680px')};
  ${({ center }) =>
    center &&
    css`
      margin-left: auto;
      margin-right: auto;
      text-align: center;
    `}
`;

export const Caption = styled((Props: TypographyProps) => (
  <Typography variant="overline" color="textSecondary" {...Props} />
))`
  font-size: 1rem;
`;

export const Title = styled(Typography)`
  margin-bottom: 12px;
`;

export const Subtitle = styled(({ children, ...other }) => (
  <Typography variant="h6" color="textSecondary" {...other}>
    {children}
  </Typography>
))`
  font-weight: 400;
`;

export const Links = styled.div`
  margin-top: 10px;
`;

export interface IHeader {
  title: string;
  subtitle: string;
  caption?: string;
  center?: boolean;
  h1?: boolean;
  className?: string;
  maxWidth?: boolean;
}
export const HeaderT = styled(
  ({ title, subtitle, caption, center, h1, maxWidth, className }: IHeader) => (
    <Header center={center} className={className}>
      {caption && (
        <Caption align={center ? 'center' : 'left'}>{caption}</Caption>
      )}
      <Title
        align={center ? 'center' : 'left'}
        variant={h1 ? 'h2' : 'h3'}
        component={h1 ? 'h1' : 'h2'}
      >
        {title}
      </Title>
      <Subtitle align={center ? 'center' : 'left'}>{subtitle}</Subtitle>
    </Header>
  ),
)`
  ${({ maxWidth }) =>
    maxWidth &&
    css`
      max-width: 100%;
    `};
`;

export const Body = styled.div`
  margin-top: ${({ theme }) => theme.spacing(4)}px;
`;

export const Img = styled.img`
  margin: 0 auto;
  max-width: 100%;
  max-height: 100%;
  display: block;
`;
