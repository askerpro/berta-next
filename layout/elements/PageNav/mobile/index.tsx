import React from 'react';
import { Link, Button } from 'layout/elements';
import RoutesManager from 'lib/RoutesManager';
import Typography from '@material-ui/core/Typography';
import { withRouter, SingletonRouter } from 'next/router';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import * as E from './elements';
import { Props as NavProps } from '..';

interface State {
  expanded: boolean;
}
interface Props extends NavProps {
  router: SingletonRouter;
}

class MobilePageNav extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  MenuItems = () => {
    const { sections, activeSection } = this.props;
    return sections.map((Section, i) => {
      const active = i === activeSection;
      return (
        <E.ListItem key={i} divider>
          <Link
            href={`#${Section.id}`}
            color={active ? 'textPrimary' : 'textSecondary'}
            variant="button"
            onClick={this.closePanel}
          >
            {Section.title}
          </Link>
        </E.ListItem>
      );
    });
  };

  switchPanel = () => {
    const { expanded } = this.state;
    this.setState({
      expanded: !expanded,
    });
  };

  closePanel = () => {
    this.setState({
      expanded: false,
    });
  };

  render() {
    const { expanded } = this.state;
    const { router } = this.props;
    return (
      <>
        <E.Wrapper>
          <ClickAwayListener onClickAway={this.closePanel}>
            <ExpansionPanel expanded={expanded} onChange={this.switchPanel}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography variant="button">
                  {RoutesManager.getName(router.pathname)}
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <E.List>
                  {this.MenuItems()}
                  <E.ListItem>
                    <Button
                      href="/services/order"
                      variant="contained"
                      color="secondary"
                      fullWidth
                    >
                      Сделать заказ
                    </Button>
                  </E.ListItem>
                </E.List>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </ClickAwayListener>
        </E.Wrapper>
      </>
    );
  }
}
export default withRouter<Props>(MobilePageNav);
