import React from 'react';
import styled, { css } from 'layout/theme';
import BreadcrumbsT, { BreadcrumbsProps } from '@material-ui/core/Breadcrumbs';
import ListT from '@material-ui/core/List';
import ListItemT from '@material-ui/core/ListItem';
import HomeIconT from '@material-ui/icons/Home';

export const Wrapper = styled.div`
  position: sticky;
  top: 0;
  margin: 0 auto;
  z-index: 2;
  width: 100%;
  background-color: ${({ theme }) => theme.palette.background.default};
  border-bottom: 1px ${({ theme }) => theme.palette.divider} solid;
`;

export const List = styled(ListT)`
  width: 100%;
`;

export const ListItem = styled(ListItemT)`
  padding-left: 0;
  padding-right: 0;
  :last-of-type {
    padding-top: 25px;
  }
` as typeof ListItemT;

interface IMyBreadCrumb extends BreadcrumbsProps {
  expanded: boolean;
}

export const Breadcrumbs = styled(({ expanded, ...other }: IMyBreadCrumb) => (
  <BreadcrumbsT classes={{ li: 'li', separator: 'separator' }} {...other} />
))<IMyBreadCrumb>`
  & .li {
    max-width: 0;
    overflow: hidden;
    transition: 0.7s;
    display: flex;
    align-items: center;
  }
  & .li:last-of-type {
    max-width: unset;
    overflow: unset;
  }
  & .separator {
    transition: 0.7s;
    max-width: 0;
    overflow: hidden;
    margin: 0;
  }

  ${({ expanded }) =>
    expanded &&
    css`
      & .li {
        max-width: 100%;
      }

      & .separator {
        max-width: 100%;
      }
    `}
`;

export const HomeIcon = styled(HomeIconT)`
  width: 20px;
  height: 20px;
  display: block;
`;
