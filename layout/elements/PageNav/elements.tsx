import styled, { css } from 'layout/theme';
import React from 'react';
import { Link, Section } from 'layout/elements';
import ContainerT from '@material-ui/core/Container';
import { LinkProps } from '@material-ui/core/Link';
import BreadcrumbsT, { BreadcrumbsProps } from '@material-ui/core/Breadcrumbs';

export const BorderContainer = styled(ContainerT)`
  height: 1px;
`;

export const Border = styled.div`
  background-color: ${({ theme }) => theme.palette.divider};
  height: 100%;
`;

interface IWrapper {
  scrolled: boolean;
}

export const Wrapper = styled(Section.Wrapper)<IWrapper>`
  position: sticky;
  top: 0;
  margin: 0 auto;
  z-index: 3;
  ${({ scrolled }) =>
    scrolled &&
    css`
      width: 100%;
      background-color: ${({ theme }) => theme.palette.background.default};
      ${BorderContainer} {
        max-width: 100%;
        padding: 0;
      }
      ${Border} {
        max-width: 100%;
      }
    `}
`;

interface IMyBreadCrumb extends IWrapper, BreadcrumbsProps {}

export const Breadcrumbs = styled(({ scrolled, ...other }) => (
  <BreadcrumbsT classes={{ li: 'li', separator: 'separator' }} {...other} />
))<IMyBreadCrumb>`
  & .li {
    max-width: 100%;
    transition: 0.7s;
  }
  & .li:last-of-type {
    max-width: unset;
    overflow: unset;
  }
  & .separator {
    max-width: 100%;
    transition: 0.7s;
  }
  ${({ scrolled }) =>
    scrolled &&
    css`
      & .li {
        max-width: 0;
        overflow: hidden;
      }

      & .separator {
        max-width: 0;
        overflow: hidden;
        margin: 0;
      }
    `}
`;

export const Container = styled(ContainerT)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 60px;
  width: 100vw;
`;

export const menuItems = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`;

interface IAnchorLink extends LinkProps {
  active: boolean;
}

export const AnchorLink = styled(({ active, ...rest }) => <Link {...rest} />)<
  IAnchorLink
>`
  margin-right: 10px;
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 10px;
  ${({ active }) =>
    active &&
    css`
        font-weight: 500
        border-bottom: 3px solid #404040;
        box-sizing: border-box;
        
    `}
`;
