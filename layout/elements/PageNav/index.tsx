import React from 'react';
import { Link, Button } from 'layout/elements';
import { IPageSection } from 'lib/PageSection';
import { vars as headerVars } from 'layout/header/elements';
import RoutesManager from 'lib/RoutesManager';
import Typography from '@material-ui/core/Typography';
import { withRouter, SingletonRouter } from 'next/router';
import { isMobile } from 'lib/device-detect';
import * as E from './elements';
import MobilePageNav from './mobile';

interface State {
  isOnTop: boolean;
}
export interface Props {
  activeSection: number;
  sections: Array<Omit<IPageSection, 'view'>>;
  title?: any;
  action?: {
    label: string;
    href: string;
  };
}

interface WithRouterProps extends Props {
  router: SingletonRouter;
}

class PageNav extends React.Component<WithRouterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      isOnTop: true,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    const { isOnTop } = this.state;
    const scrollTop = window.scrollY;
    if (scrollTop > headerVars.height && isOnTop === true) {
      this.setState({
        isOnTop: false,
      });
    }
    if (scrollTop <= headerVars.height && isOnTop === false) {
      this.setState({
        isOnTop: true,
      });
    }
  };

  BreadCrumbLinks = () => {
    const { router } = this.props;
    const { isOnTop } = this.state;
    return router.pathname.split('/').map((_path, i, arr) => {
      const route = arr.slice(0, i + 1).join('/');
      if (i === arr.length - 1) {
        return isOnTop ? (
          <Typography key={i}>{RoutesManager.getName(route)}</Typography>
        ) : (
          <Link href={route} key={i} color="textPrimary" variant="h5">
            {RoutesManager.getName(route)}
          </Link>
        );
      }
      return (
        <Link href={route} key={i} color="textPrimary">
          {RoutesManager.getName(route)}
        </Link>
      );
    });
  };

  MenuItems = () => {
    const { sections, activeSection } = this.props;
    return sections.map((Section, i) => {
      return (
        <E.AnchorLink
          active={i === activeSection}
          href={`#${Section.id}`}
          key={i}
          color="textPrimary"
          variant="button"
        >
          {Section.title}
        </E.AnchorLink>
      );
    });
  };

  render() {
    const { isOnTop } = this.state;
    const { title, action } = this.props;
    return (
      <>
        <E.Wrapper scrolled={!isOnTop}>
          <E.Container>
            <E.Breadcrumbs scrolled={!isOnTop}>
              {title ? (
                <Link href="#" color="textPrimary" variant="h5">
                  {title}
                </Link>
              ) : (
                this.BreadCrumbLinks()
              )}
            </E.Breadcrumbs>
            <E.menuItems>
              {this.MenuItems()}
              <Button
                href={action ? action.href : '/services/order'}
                variant="contained"
                color="secondary"
                size="small"
              >
                {action ? action.label : 'Сделать заказ'}
              </Button>
            </E.menuItems>
          </E.Container>
          <E.BorderContainer>
            <E.Border> </E.Border>
          </E.BorderContainer>
        </E.Wrapper>
      </>
    );
  }
}
const PageNavWithRouter = withRouter<WithRouterProps>(PageNav);

export default (props: Props) =>
  isMobile() ? <MobilePageNav {...props} /> : <PageNavWithRouter {...props} />;
