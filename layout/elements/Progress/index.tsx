import React from 'react';
import { LinearProgress, Typography } from '@material-ui/core';
import * as E from './elements';

interface Props {
  title: string;
  value: number;
  icon?: string;
  heroValue?: string;
}
export default ({ title, value, icon, heroValue }: Props) => {
  return (
    <E.Progress>
      <E.content>
        <Typography color="textPrimary" variant="body2">
          {icon && <E.icon icon={icon} />}
          {title}
        </Typography>
        <Typography variant="h6" color="textPrimary">
          {heroValue}
        </Typography>
      </E.content>
      <LinearProgress variant="determinate" color="secondary" value={value} />
    </E.Progress>
  );
};
