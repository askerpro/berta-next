import styled from 'styled-components';

import Icon from 'layout/elements/Icon';

export const Progress = styled.div`
  margin-top: 30px;
  &:first-of-type {
    margin-top: 0px;
  }
`;
export const content = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
  align-items: flex-end;
`;

export const label = styled.div`
  font-weight: 300;
  font-size: 14px;
`;
export const icon = styled(Icon)`
  height: 23px;
  margin-right: 7px;
  min-height: unset;
  min-width: unset;
  margin-bottom: 6px;
  width: auto;
`;
export const heroValue = styled.div`
  font-size: 24px;
  font-weight: 500;
`;

export const value = styled.div`
  font-size: 40px;
`;
