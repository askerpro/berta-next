/* eslint-disable jsx-a11y/anchor-has-content */
import * as React from 'react';
import NextLink, { LinkProps as NextLinkProps } from 'next/link';
import MuiButton, { ButtonProps } from '@material-ui/core/Button';

type NextComposedProps = ButtonProps & Omit<NextLinkProps, 'href'>;

export default React.forwardRef<HTMLButtonElement, NextComposedProps>(
  (props, ref) => {
    const {
      as,
      href,
      replace,
      scroll,
      passHref,
      shallow,
      prefetch,
      ...other
    } = props;
    if (href)
      return (
        <NextLink
          href={href}
          prefetch={prefetch}
          as={as}
          replace={replace}
          scroll={scroll}
          shallow={shallow}
          passHref={passHref}
        >
          <MuiButton ref={ref} {...other} href={href} />
        </NextLink>
      );
    return <MuiButton ref={ref} {...other} />;
  },
);
