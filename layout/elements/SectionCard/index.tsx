import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Button, Link } from 'layout/elements';
import * as E from './elements';

interface Props {
  title: string;
  caption?: string;
  action?: boolean;
  actionText?: string;
  actionDescription?: string;
  actionDescriptionTitle?: string;
  actionDescriptionHref?: string;
  desc: string;
  href?: string;
  image: string;
  contain?: boolean;
  vertical?: boolean;
  reversed?: boolean;
  promo?: boolean;
  h1?: boolean;
}

export default ({
  h1 = false,
  promo,
  title,
  caption,
  desc,
  action = true,
  actionText = 'Заказать',
  actionDescription,
  actionDescriptionTitle = 'Открыть калькулятор',
  actionDescriptionHref = '/prices/calc',
  href = '/services/order',
  image,
  contain,
  vertical,
  reversed,
}: Props) => {
  const CardContentContainer = (
    <E.CardContent>
      {caption && (
        <Typography
          gutterBottom
          variant="overline"
          component="p"
          color="textSecondary"
        >
          {caption}
        </Typography>
      )}
      <Typography
        gutterBottom
        variant={promo ? 'h4' : 'h3'}
        component={promo ? 'p' : (h1 && 'h1') || 'h2'}
      >
        {title}
      </Typography>
      <Typography variant="body1" color="textSecondary" component="p">
        {desc}
      </Typography>
      <E.CardActions display={action} stretch={actionDescription}>
        {actionDescription && (
          <Link
            variant="h5"
            color="textPrimary"
            underline="always"
            href={actionDescriptionHref}
            title={actionDescriptionTitle}
          >
            {actionDescription}
          </Link>
        )}
        <Button
          href={href}
          variant="outlined"
          size="large"
          fullWidth={!actionDescription}
        >
          {actionText}
        </Button>
      </E.CardActions>
    </E.CardContent>
  );
  const CardMediaContainer = (
    <E.CardMedia image={image} title={title} contain={contain} />
  );

  const ReversedCardContent = (
    <>
      {CardContentContainer}
      {CardMediaContainer}
    </>
  );

  const NotReversedCardContent = (
    <>
      {CardMediaContainer}
      {CardContentContainer}
    </>
  );

  return (
    <E.Card vertical={vertical}>
      {reversed ? ReversedCardContent : NotReversedCardContent}
    </E.Card>
  );
};
