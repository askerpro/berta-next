import CardMediaT, { CardMediaProps } from '@material-ui/core/CardMedia';
import CardT, { CardProps } from '@material-ui/core/Card';
import CardActionsT, { CardActionsProps } from '@material-ui/core/CardActions';
import CardContentT from '@material-ui/core/CardContent';
import styled, { css } from 'layout/theme';
import React from 'react';

interface ICardMedia extends CardMediaProps {
  contain?: boolean;
}
export const CardMedia = styled(({ contain, ...other }) => (
  <CardMediaT {...other} />
))<ICardMedia>`
  position: relative;
  min-height: 200px;
  background-size: ${({ contain }) => (contain ? 'contain' : 'cover')};
  @media (min-width: 768px) {
    padding-top: 62%;
  }
`;
interface ICard extends CardProps {
  vertical?: boolean;
}
export const Card = styled(({ vertical, ...other }) => <CardT {...other} />)<
  ICard
>`
  display: ${({ vertical }) => (vertical ? 'flex' : 'block')};
  @media (max-width: 768px) {
    display: block;
  }
  ${({ vertical }) =>
    vertical &&
    css`
      ${CardMedia} {
        @media (min-width: 768px) {
          width: 62%;
          flex-shrink: 0;
          min-height: 550px;
          padding-top: unset;
        }
      }
    `}
`;
interface ICardActions extends CardActionsProps {
  display?: boolean;
  stretch?: boolean;
}
export const CardActions = styled(({ display, stretch, ...other }) => (
  <CardActionsT {...other} />
))<ICardActions>`
  display: ${({ display }) => (display ? 'flex' : 'none')};
  justify-content: ${({ stretch }) => (stretch ? 'space-between' : 'none')};
  margin-top: ${({ theme }) => theme.spacing(2)}px;
  padding: unset;
`;

export const CardContent = styled(CardContentT)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: ${({ theme }) => theme.spacing(2)}px;
  @media (min-width: 768px) {
    padding: ${({ theme }) => theme.spacing(5)}px;
  }
`;
