import React from 'react';
import styled from 'layout/theme';
import underCnstImgSrc from 'static/img/under_constr.jpg';

export const UnderCnst = styled(props => (
  <img {...props} alt="Under Reconstruction" src={underCnstImgSrc} />
))`
  margin: 0 auto;
  display: block;
`;

export default {};
