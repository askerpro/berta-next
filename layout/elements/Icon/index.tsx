/* eslint-disable global-require */
import styled from 'layout/theme';
import React from 'react';

const Icons = {
  lock: require('./img/lock.png'),
  temp: require('./img/temp.png'),
  silense: require('./img/silense.png'),
  easy_care: require('./img/easy_care.png'),
  laminat: require('./img/laminat.png'),
  no_deform: require('./img/no_deform.png'),
  more_light: require('./img/more_light.png'),
  secured: require('./img/protected.png'),
  panoramic: require('./img/panoramic.png'),
  gift: require('./img/gift.png'),
  question: require('./img/question.png'),
  info: require('./img/info.png'),
  calc: require('./img/calc.png'),
  stages: require('./img/stages.png'),
  rectangles: require('./img/rectangles.png'),
  measure: require('./img/measure.png'),
  guarantee: require('./img/guarantee.png'),
  diamond: require('./img/diamond.png'),
  plus: require('./img/plus.png'),
  sun: require('./img/sun.png'),
  sound: require('./img/sound.png'),
  design: require('./img/design.png'),
  drop: require('./img/drop.png'),
  economy: require('./img/economy.png'),
  handle: require('./img/handle.png'),
  montage: require('./img/montage.png'),
  shield: require('./img/shield.png'),
  double_arrow_down: require('./img/double_arrow_down.png'),
  ag: require('./img/ag+.png'),
  heart: require('./img/heart.png'),
};

export const IconT = styled.img`
  height: 24px;
  width: auto;
  filter: brightness(${({ theme }) => theme.iconBrightness});
`;

interface Props {
  icon: string;
  className?: string;
}

export const Icon = styled(({ icon, className }: Props) => (
  <IconT className={className} src={Icons[icon]} />
))``;

export default Icon;

export interface IIcon {
  className?: string;
}

export const Lock = props => <IconT {...props} src={Icons.lock} />;

export const Temp = props => <IconT {...props} src={Icons.temp} />;

export const silense = props => <IconT {...props} src={Icons.silense} />;

export const EasyCare = props => <IconT {...props} src={Icons.easy_care} />;

export const laminat = props => <IconT {...props} src={Icons.laminat} />;
export const NoDeform = props => <IconT {...props} src={Icons.no_deform} />;

export const MoreLight = props => <IconT {...props} src={Icons.more_light} />;

export const Secured = props => <IconT {...props} src={Icons.secured} />;
export const Panoramic = props => <IconT {...props} src={Icons.panoramic} />;

export const Gift = props => <IconT {...props} src={Icons.gift} />;

export const Question = props => <IconT {...props} src={Icons.question} />;
export const Info = props => <IconT {...props} src={Icons.info} />;

export const Calc = props => <IconT {...props} src={Icons.calc} />;

export const Stages = props => <IconT {...props} src={Icons.stages} />;
export const Rectangles = props => <IconT {...props} src={Icons.rectangles} />;

export const Measure = props => <IconT {...props} src={Icons.measure} />;

export const Guarantee = props => <IconT {...props} src={Icons.guarantee} />;
export const Diamond = props => <IconT {...props} src={Icons.diamond} />;

export const Plus = props => <IconT {...props} src={Icons.plus} />;

export const Sun = props => <IconT {...props} src={Icons.sun} />;

export const Sound = props => <IconT {...props} src={Icons.sound} />;
export const Design = props => <IconT {...props} src={Icons.design} />;

export const Dop = props => <IconT {...props} src={Icons.drop} />;

export const Economy = props => <IconT {...props} src={Icons.economy} />;
export const Handle = props => <IconT {...props} src={Icons.handle} />;

export const Montage = props => <IconT {...props} src={Icons.montage} />;

export const Shield = props => <IconT {...props} src={Icons.shield} />;

export const DoubleArrowDown = props => (
  <IconT {...props} src={Icons.double_arrow_down} />
);

export const Ag = props => <IconT {...props} src={Icons.ag} />;

export const Heart = props => <IconT {...props} src={Icons.heart} />;
