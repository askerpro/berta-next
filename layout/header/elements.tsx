import styled from 'layout/theme';
import { Button, Container } from '@material-ui/core';
import { Phone } from '@material-ui/icons';
import { HeaderState } from '.';

export const vars = {
  height: 60,
};

interface IHeader {
  state: HeaderState;
}

export const Header = styled.header<IHeader>`
  /* transform: translateY(
    ${({ state }) => (state === HeaderState.OPENED ? '0px' : '-100%')}
  ); */
  position: relative;

  transition: ${({ state }) => (state === HeaderState.OPENED ? '1' : '0')}s;
`;

export const NavWrapper = styled(Container)`
  height: 100%;
  width: 100%;
  position: relative;
  z-index: 3;
`;

export const NavContainer = styled.div`
  height: ${vars.height - 1}px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
  font-size: 17px;
`;

export const LogoWrapper = styled.div`
  position: relative;
  width: 115px;
`;

export const BackGround = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  left: 0;
  top: 0;
  z-index: -1;
`;
