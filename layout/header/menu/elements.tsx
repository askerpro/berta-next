import styled from 'layout/theme';
import { Button } from '@material-ui/core';
import React from 'react';
import { Phone } from '@material-ui/icons';

export const MenuButton = styled(props => <Button {...props} />)`
  font-weight: 400;
`;

export const NavSection = styled.div`
  justify-content: flex-end;
  display: flex;
  line-height: 30px;
  height: 40px;
  align-items: center;
`;

export const Actions = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-left: 1px solid ${({ theme }) => theme.palette.divider};
  padding-left: 30px;
  margin-left: 30px;
`;

export const PhoneIcon = styled(Phone)`
  height: 24px;
  margin-right: 7px;
`;
export const PhoneButton = styled(Button)`
  flex-shrink: 0;
  display: flex;
  align-items: center;
  font-weight: 400;
`;

export default {};
