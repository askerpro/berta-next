import React from 'react';
import RoutesManager from 'lib/RoutesManager';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'layout/elements';
import Hidden from '@material-ui/core/Hidden';
import { callReached } from 'lib/ym';
import MobileMenu from './mobile';
import * as E from './elements';

const phone = '8 800 550 30 25';

export type RoutesType = {
  title: string;
  children?: Array<RoutesType>;
};

export interface Props {
  category: RoutesType;
  path: string;
}

interface State {
  anchorEl: HTMLElement;
}

class MenuSection extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
  }

  handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null,
    });
  };

  render() {
    const { handleClose, handleClick } = this;
    const { category, path } = this.props;
    const { anchorEl } = this.state;
    if (category.children) {
      const subItems = Object.entries(category.children).map((subroute, i) => (
        // <MenuItem onClick={handleClose}>
        //   <Link href={subroute.pathname}>{subroute.title}</Link>
        // </MenuItem>
        <MenuItem
          color="inherit"
          component={Link}
          onClick={handleClose}
          href={`/${path}/${subroute[0]}`}
          key={i}
        >
          {subroute[1].title}
        </MenuItem>
      ));
      return (
        <div>
          <E.MenuButton aria-haspopup="true" onClick={handleClick}>
            {category.title}
          </E.MenuButton>
          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            {subItems}
          </Menu>
        </div>
      );
    }
    return (
      <div>
        <E.MenuButton href={`/${path}`}>{category.title}</E.MenuButton>
      </div>
    );
  }
}

export { default as MobileMenuItems } from './mobile';

export default () => (
  <>
    <Hidden implementation="css" mdDown>
      <E.NavSection>
        {Object.entries(RoutesManager.routes.children).map((category, i) => (
          <MenuSection category={category[1]} path={category[0]} key={i} />
        ))}
        <E.Actions>
          <E.PhoneButton href={`tel:${phone}`} onClick={callReached}>
            <E.PhoneIcon />
            {phone}
          </E.PhoneButton>
        </E.Actions>
      </E.NavSection>
    </Hidden>
    <Hidden smUp implementation="css">
      <MobileMenu />
    </Hidden>
  </>
);
