import React from 'react';
import RoutesManager from 'lib/RoutesManager';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import IconButton from '@material-ui/core/IconButton';
import { Menu as MenuIcon } from '@material-ui/icons';
import { ListItemLink } from './elements';
import { Props as MenuProps } from '..';

interface Props {
  isMenuOpen: boolean;
  open(): void;
  close(): void;
}
export default class extends React.Component<Props> {
  MobileMenuSection = ({ category, path }: MenuProps) => {
    const { close } = this.props;
    return (
      <div>
        <List>
          {category.children ? (
            <>
              <ListItem button>
                <Typography variant="subtitle1">{category.title}</Typography>
              </ListItem>
              {Object.entries(category.children).map((subCategory, i) => (
                <ListItem button key={i}>
                  <ListItemLink
                    href={`/${path}/${subCategory[0]}`}
                    color="inherit"
                    onClick={close}
                  >
                    {subCategory[1].title}
                  </ListItemLink>
                </ListItem>
              ))}
            </>
          ) : (
            <ListItem button>
              <ListItemLink
                href={`/${path}`}
                color="inherit"
                variant="subtitle1"
                onClick={close}
              >
                {category.title}
              </ListItemLink>
            </ListItem>
          )}
        </List>
        <Divider />
      </div>
    );
  };

  render() {
    const { isMenuOpen, open, close } = this.props;
    return (
      <>
        <SwipeableDrawer
          anchor="right"
          open={isMenuOpen}
          onClose={close}
          onOpen={open}
        >
          {Object.entries(RoutesManager.routes.children).map((category, i) => (
            <div key={i}>
              {this.MobileMenuSection({
                category: category[1],
                path: category[0],
              })}
            </div>
          ))}
        </SwipeableDrawer>
        <IconButton
          onClick={() => {
            isMenuOpen ? close() : open();
          }}
        >
          <MenuIcon />
        </IconButton>
      </>
    );
  }
}
