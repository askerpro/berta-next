import { AppState } from 'store';
import { connect } from 'react-redux';
import MobileMenu from './component';

export enum Actions {
  OPEN = 'OPEN_MOBILE_MENU',
  CLOSE = 'CLOSE_MOBILE_MENU',
}

export enum mobileMenuState {
  OPENED,
  CLOSED,
}

export const open = {
  type: Actions.OPEN,
};

export const close = {
  type: Actions.CLOSE,
};

type ActionType = typeof open | typeof close;

export const initialState: mobileMenuState = mobileMenuState.CLOSED;

export const reducer = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case Actions.OPEN:
      return mobileMenuState.OPENED;

    case Actions.CLOSE:
      return mobileMenuState.CLOSED;

    default:
      return state;
  }
};

const mapStateToProps = (state: AppState) => {
  return {
    isMenuOpen: state.mobileMenu === mobileMenuState.OPENED,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(close),
    open: () => dispatch(open),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MobileMenu);
