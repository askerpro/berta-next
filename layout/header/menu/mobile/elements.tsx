import { Link } from 'layout/elements';
import styled from 'layout/theme';
import React from 'react';

export const ListItemLink = styled(props => <Link {...props} />)`
  margin-left: ${({ theme }) => theme.spacing(1)}px;
`;

export default {};
