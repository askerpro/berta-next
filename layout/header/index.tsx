import React from 'react';
import Logo from 'layout/logo';
import { Link } from 'layout/elements';
import { Dark, Light } from 'layout/theme';
import { Divider } from '@material-ui/core';
import * as E from './elements';
import Menu from './menu';

export enum HeaderState {
  OPENED = 'OPENED',
  CLOSED = 'CLOSED',
}

interface IProps {
  state?: HeaderState;
  border?: 'wrapper' | 'content';
  theme?: 'dark' | 'light';
}

const Header = ({ state, border, theme }: IProps) => {
  const ThemeWrapper = theme === 'dark' ? Dark : Light;
  return (
    <ThemeWrapper>
      <E.Header state={state}>
        <E.BackGround />

        <E.NavWrapper>
          <E.NavContainer>
            <E.LogoWrapper>
              <Link href="/">
                <Logo theme={theme} />
              </Link>
            </E.LogoWrapper>
            <Menu />
          </E.NavContainer>
          {border === 'content' && <Divider />}
        </E.NavWrapper>
        {border === 'wrapper' && <Divider />}
      </E.Header>
    </ThemeWrapper>
  );
};
export default Header;
