import React from 'react';
import { Button } from 'layout/elements';
import Backdrop from '@material-ui/core/Backdrop';
import Router from 'next/router';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import { Calc, Rectangles } from 'layout/elements/Icon';
import { Phone, Place, Menu } from '@material-ui/icons';
import { Dark } from 'layout/theme';
import { withStyles } from '@material-ui/styles';
import { Hidden, ButtonGroup } from '@material-ui/core';
import { close, open, mobileMenuState } from 'layout/header/menu/mobile';
import { connect } from 'react-redux';
import { AppState } from 'store';
import { callReached } from 'lib/ym';
import * as E from './elements';

const actions = [
  {
    icon: <Phone />,
    name: '8 800 550 30 25',
    mobileName: 'Звонок',
    href: 'tel:88005503025',
    action: () => {
      callReached();
      window.location.href = 'tel:88005503025';
    },
  },
  {
    icon: <Calc />,
    name: 'Калькулятор',
    mobileName: 'Калькулятор',
    href: '/prices/calc',
  },
  {
    icon: <Place />,
    name: 'Контакты',
    mobileName: 'Контакты',
    href: '/contacts',
  },
  {
    icon: <Rectangles />,
    name: 'Сделать заказ',
    href: '/services/order',
  },
];

interface State {
  isOpen: boolean;
  isOnTop: boolean;
}

interface Props {
  classes: any;
  isMenuOpen: boolean;
  openMenu(): void;
  closeMenu(): void;
}
class SpeedDial extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isOnTop: true,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.onScroll);
    this.onScroll();
  }

  onScroll = () => {
    const { isOnTop } = this.state;
    if (window.scrollY > 60 && isOnTop) {
      this.setState({
        isOnTop: false,
      });
    }
    if (window.scrollY < 60 && !isOnTop) {
      this.setState({
        isOnTop: true,
      });
    }
  };

  handleOpen = () => {
    this.setState({
      isOpen: true,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  render() {
    const { isOpen, isOnTop } = this.state;
    const { classes, openMenu, closeMenu, isMenuOpen } = this.props;
    return (
      <Dark>
        <>
          <Hidden smDown implementation="css">
            <E.Wrapper>
              <E.SpeedDial
                ariaLabel="SpeedDial tooltip example"
                icon={<SpeedDialIcon />}
                onClose={this.handleClose}
                onOpen={this.handleOpen}
                open={isOpen}
              >
                {actions.map(action => (
                  <E.SpeedDialAction
                    FabProps={{
                      color: 'primary',
                      href: action.href,
                    }}
                    key={action.name}
                    icon={action.icon}
                    tooltipTitle={action.name}
                    tooltipOpen
                    classes={{
                      staticTooltipLabel: classes.action,
                    }}
                    onClick={event => {
                      event.preventDefault();
                      action.action
                        ? action.action()
                        : Router.push(action.href);

                      this.handleClose();
                    }}
                  />
                ))}
              </E.SpeedDial>
            </E.Wrapper>
          </Hidden>
          <Hidden smUp>
            <E.MobileWrapper visible={!isOnTop}>
              <E.MobileItemsWrapper
                size="small"
                aria-label="small outlined button group"
              >
                {actions.map((action, i, arr) => {
                  if (i === arr.length - 1) {
                    return (
                      <E.MobileMenuItem
                        key={i}
                        size="small"
                        onClick={() => {
                          isMenuOpen ? closeMenu() : openMenu();
                        }}
                      >
                        <Menu />
                        Меню
                      </E.MobileMenuItem>
                    );
                  }
                  return (
                    <E.MobileMenuItem
                      size="small"
                      href={action.href}
                      onClick={event => {
                        event.preventDefault();
                        action.action
                          ? action.action()
                          : Router.push(action.href);
                        this.handleClose();
                      }}
                      key={i}
                    >
                      {action.icon}
                      {action.mobileName}
                    </E.MobileMenuItem>
                  );
                })}
              </E.MobileItemsWrapper>
            </E.MobileWrapper>
          </Hidden>
        </>
      </Dark>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    closeMenu: () => dispatch(close),
    openMenu: () => dispatch(open),
  };
};

const mapStateToProps = (state: AppState) => {
  return {
    isMenuOpen: state.mobileMenu === mobileMenuState.OPENED,
  };
};
const componentWithStyles = withStyles(E.styles)(SpeedDial);
const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(componentWithStyles);

export default connected;
