import styled from 'layout/theme';
import SpeedDialT from '@material-ui/lab/SpeedDial';
import SpeedDialActionT from '@material-ui/lab/SpeedDialAction';
import { makeStyles } from '@material-ui/core/styles';
import { Button, ButtonGroup } from '@material-ui/core';
import React from 'react';

export const styles = {
  action: { width: 'max-content' },
};

export const Wrapper = styled.div``;

export const SpeedDialAction = styled(SpeedDialActionT)`
  color: ${({ theme }) => theme.palette.text.primary};
  .staticToolTipLabel {
  }
`;

export const SpeedDial = styled(SpeedDialT)`
  transform: translateZ(0px);
  position: fixed;
  bottom: ${({ theme }) => theme.spacing(6)}px;
  right: ${({ theme }) => theme.spacing(6)}px;
`;

interface IMobileWrapper {
  visible: boolean;
}

export const MobileWrapper = styled.div<IMobileWrapper>`
  position: sticky;
  bottom: 0;
  transform: translateY(${({ visible }) => (visible ? 0 : 60)}px);
  transition: 0.7s;
  background: ${({ theme }) => theme.palette.background.default};
  width: 100%;
  z-index: 5;
  display: flex;
`;

export const MobileMenuItem = styled(props => (
  <Button classes={{ label: 'label' }} {...props} />
))`
  flex-grow: 1;
  .label {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: 11px;
  }
`;

export const MobileItemsWrapper = styled(ButtonGroup)`
  width: 100%;
  justify-content: space-between;
`;

export default {};
