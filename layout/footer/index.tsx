import React from 'react';
import styled, { White } from 'layout/theme';
import { Section } from 'layout/elements';
import { Container, Typography, Divider } from '@material-ui/core';

import Grid from '@material-ui/core/Grid';

const Inner = styled(Grid)`
  padding: 10px 0;
`;

const Footer = styled(Section.Wrapper)`
  margin-bottom: 0;
`;

export default () => (
  <White>
    <Footer as="footer">
      <Container>
        <Divider />
        <Inner container justify="flex-end">
          <Typography color="textPrimary">
            © 2019 | Okna Berta - Все права защищены
          </Typography>
        </Inner>
      </Container>
    </Footer>
  </White>
);
