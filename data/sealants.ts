export enum SealantType {
  EPDM_GRAY,
  EPDM_BLACK,
}
export const sealants = [
  {
    title: 'Серый',
  },
  {
    title: 'Черный',
  },
];

export default sealants;
