import { LineParams } from 'components/LineParam';

export enum GlassType {
  STANDART,
  TERMOS,
  ENERGO,
}

export const label = 'Стеклопакет';

export const glasses = [
  {
    title: 'Стандарт',
    desc:
      'Стандартный стеклопакет. Соответствует ГОСТ стандарту. Ничего лишнего.',
    icon: '/static/img/glasses/standart.jpg',
    lineParams: {
      [LineParams.WARM]: 0,
      [LineParams.DESIGN]: 0,
      [LineParams.COMFORT]: 0,
    },
  },
   {
    title: 'Термос+',
    desc:
      'Данный стеклопакет был изготовлен специально для компании “ОКНА БЕРТА” и по своим потребительским качествам является, пожалуй, одним из лучших продуктов на рынке. Так-же вы можете выбрать оттенок наружнего стекла, что позволит воплатить ваши идеи по оформлению фасада.',
    icon: '/static/img/glasses/thermos.jpg',
    lineParams: {
      [LineParams.WARM]: 10,
      [LineParams.DESIGN]: 10,
      [LineParams.COMFORT]: 10,
    },
  },
  {
    title: 'Энергосберегающий',
    desc:
      'Обладает повышенными свойствами энергосбережения, что позволяет эффективнее сохранять тепло вашего дома.',
    icon: '/static/img/glasses/thermos.jpg',
    lineParams: {
      [LineParams.WARM]: 10,
      [LineParams.DESIGN]: 10,
      [LineParams.COMFORT]: 10,
    },
  },
];

export default glasses;
