import handles, { HandleType as HandleTypes } from 'data/handles';
import glasses, { GlassType as GlassTypes } from 'data/glasses';
import sealants, { SealantType } from 'data/sealants';
import shtapicks, { ShtapickType } from 'data/shtapicks';
import furnitures, { FurnitureType } from 'data/furnitures';
import { LineParams, ILineParam } from 'components/LineParam';

export enum ProfileType {
  BERTA_SILVER_ECO_CLASSIC,
  BERTA_SILVER_ECO_PREMIUM,
  SCHUCO_PREMIUM,
  SCHUCO_IMPERIAL,
  BERTA_LITE,
}

export enum ProfileParams {
  MONTAGE_DEEP = 'montageDeep',
  AIR_CHAMBERS = 'airChambers',
  ARM_THICKNESS = 'armThickness',
  PLASTIC_THICKNESS = 'plasticThickness',
  COULD_BE_LAMINATED = 'couldBeLaminated',
  GLASS_WIDTH = 'glassWidth',
}

export const label = 'Профиль';

interface IProfileExtraParam {
  name: string;
  title: string;
  values: string[];
}

interface IProfileParam extends IProfileExtraParam {
  units?: string;
}

interface IEquipmentParam extends IProfileExtraParam {}

export enum EqupmentParam {
  FURNITURE = 'furniture',
  HANDLES = 'handles',
  SHTAPICKS = 'shtapicks',
  SEALANTS = 'sealants',
  GLASS = 'glass',
}

export enum LaminationType {
  NONE,
  ONE_SIDE,
  BOTH,
}

const LaminationTypes: string[] = [
  'Нет',
  'Односторонняя',
  'Односторонняя, Двухсторонняя',
];

interface IProfileInitData {
  title: string;
  desc: string;
  animationPath: string;
  icon: string;
  link?: string;
  lineParams: {
    [LineParams.WARM]: number;
    [LineParams.COMFORT]: number;
    [LineParams.DESIGN]: number;
  };
  profileParams: {
    [ProfileParams.MONTAGE_DEEP]: string;
    [ProfileParams.AIR_CHAMBERS]: string[];
    [ProfileParams.ARM_THICKNESS]: string[];
    [ProfileParams.PLASTIC_THICKNESS]: string[];
    [ProfileParams.COULD_BE_LAMINATED]: LaminationType;
    [ProfileParams.GLASS_WIDTH]: string[];
  };
  equpmentParams: {
    [EqupmentParam.FURNITURE]: number;
    [EqupmentParam.HANDLES]: number[];
    [EqupmentParam.SHTAPICKS]: number[];
    [EqupmentParam.SEALANTS]: number[];
    [EqupmentParam.GLASS]: number[];
  };
}

export class ProfileData {
  title: string;

  desc: string;

  animationPath: string;

  icon: string;

  link?: string;

  lineParams: {
    [LineParams.WARM]: ILineParam;
    [LineParams.DESIGN]: ILineParam;
    [LineParams.COMFORT]: ILineParam;
  };

  profileParams: {
    [ProfileParams.MONTAGE_DEEP]: IProfileParam;
    [ProfileParams.AIR_CHAMBERS]: IProfileParam;
    [ProfileParams.ARM_THICKNESS]: IProfileParam;
    [ProfileParams.PLASTIC_THICKNESS]: IProfileParam;
    [ProfileParams.COULD_BE_LAMINATED]: IProfileParam;
    [ProfileParams.GLASS_WIDTH]: IProfileParam;
  };

  equpmentParams: {
    [EqupmentParam.FURNITURE]: IEquipmentParam;
    [EqupmentParam.HANDLES]: IEquipmentParam;
    [EqupmentParam.SHTAPICKS]: IEquipmentParam;
    [EqupmentParam.SEALANTS]: IEquipmentParam;
    [EqupmentParam.GLASS]: IEquipmentParam;
  };

  constructor(params: IProfileInitData) {
    this.title = params.title;
    this.animationPath = params.animationPath;
    this.desc = params.desc;
    this.icon = params.icon;
    this.lineParams = {
      [LineParams.WARM]: {
        label: 'Тепло\\Шумо изоляция',
        units: '',
        icon: 'temp',
        value: params.lineParams[LineParams.DESIGN],
      },
      [LineParams.DESIGN]: {
        label: 'Внешний вид',
        units: '',
        icon: 'rectangles',
        value: params.lineParams[LineParams.DESIGN],
      },
      [LineParams.COMFORT]: {
        label: 'Гарантия',
        units: '(лет)',
        icon: 'COMFORT',
        value: params.lineParams[LineParams.COMFORT],
      },
    };

    this.profileParams = {
      [ProfileParams.MONTAGE_DEEP]: {
        name: 'Монтажная глубина',
        title:
          'Ширина профиля или так называемая “монтажная глубина” отвечает за количество воздушных камер профиля. Чем шире профиль, тем больше камер там будет и соответственно, тем выше будут тепло и звукоизоляционные свойства профиля',
        units: 'mm',
        values: [params.profileParams[ProfileParams.MONTAGE_DEEP]],
      },
      [ProfileParams.AIR_CHAMBERS]: {
        name: 'Воздушные камеры',
        title:
          'В пластиковом профиле всегда есть полости — они нужны для звуко- и теплоизоляции. Минимальное количество таких камер — три, но в дорогих окнах может быть пять и более камер.',
        values: params.profileParams[ProfileParams.AIR_CHAMBERS],
      },
      [ProfileParams.ARM_THICKNESS]: {
        name: 'Толщина армирования',
        units: 'mm',
        title:
          'Армирование обеспечивает условия для противодействия эксплуатационным нагрузкам, крепежа фурнитуры и монтажа конструкции. Чем толще, тем лучше',
        values: params.profileParams[ProfileParams.ARM_THICKNESS],
      },
      [ProfileParams.PLASTIC_THICKNESS]: {
        name: 'Толщина внешних стенок',
        units: 'mm',
        title: `
      От толщины стенок зависят многие прочностные свойства окна: 
      прочность сварных углов соединений
      надежность крепления фурнитуры
      ударостойкость и прочность поверхности
      Все это влияет на долговечность изделия.
    `,
        values: params.profileParams[ProfileParams.PLASTIC_THICKNESS],
      },
      [ProfileParams.COULD_BE_LAMINATED]: {
        name: 'Возможность ламинации',
        title: 'Возможность ламинации',
        values: [
          LaminationTypes[
            params.profileParams[ProfileParams.COULD_BE_LAMINATED]
          ],
        ],
      },
      [ProfileParams.GLASS_WIDTH]: {
        name: 'Ширина стеклопакета',
        title:
          'Для теплого климата, комнат, ориентированных на юг, и кухонь, где температура почти всегда выше, чем в других местах, подойдут стандартные двухкамерные стеклопакеты.',
        values: params.profileParams[ProfileParams.GLASS_WIDTH],
      },
    };

    this.equpmentParams = {
      [EqupmentParam.FURNITURE]: {
        name: 'Фурнитура',

        title:
          'Фурнитура имеет значение не меньшее, чем сам профиль. Каким бы энергосберегающим и эстетичным не был профиль - если ручки не закрываются, механизм заедает и створка не двигается, то очень скоро вы пожалете, что сэкономили на окне.',
        values: [
          furnitures[params.equpmentParams[EqupmentParam.FURNITURE]].title,
        ],
      },
      [EqupmentParam.HANDLES]: {
        name: 'Доступные ручки',
        title: 'Перечень возможных ручек на этот профиль',
        values: params.equpmentParams[EqupmentParam.HANDLES].map(
          handle => handles[handle].title,
        ),
      },
      [EqupmentParam.SHTAPICKS]: {
        name: 'Штапики',
        title: 'Тип штапиков профиля',
        values: params.equpmentParams[EqupmentParam.SHTAPICKS].map(
          shtapick => shtapicks[shtapick].title,
        ),
      },
      [EqupmentParam.SEALANTS]: {
        name: 'Уплотнители',
        title:
          'Хорошие окна ПВХ имеют как минимум два контура уплотнения. Уплотнители делают из разных материалов, но лучшим считается натуральный каучук, стойкий к перепадам температур.',
        values: params.equpmentParams[EqupmentParam.SEALANTS].map(
          sealant => sealants[sealant].title,
        ),
      },
      [EqupmentParam.GLASS]: {
        name: 'Стеклопакеты',
        title:
          'Хорошие окна ПВХ имеют как минимум два контура уплотнения. Уплотнители делают из разных материалов, но лучшим считается натуральный каучук, стойкий к перепадам температур.',
        values: params.equpmentParams[EqupmentParam.GLASS].map(
          glass => glasses[glass].title,
        ),
      },
    };
  }
}
export const profiles: ProfileData[] = [];
profiles[ProfileType.BERTA_SILVER_ECO_CLASSIC] = new ProfileData({
  title: 'Silver Eco Classic',
  desc:
    'Это инновационный, экологически чистый и безопасный для здоровья продукт с содержанием серебра по уникальной запатентованной технологии Silver Eco.',
  animationPath: 'silver-eco-classic',
  icon: '/static/img/profiles/silver-eco-classic.jpg',
  lineParams: {
    [LineParams.WARM]: 60,
    [LineParams.DESIGN]: 55,
    [LineParams.COMFORT]: 68,
  },
  profileParams: {
    [ProfileParams.AIR_CHAMBERS]: ['4'],
    [ProfileParams.ARM_THICKNESS]: ['1.5'],
    [ProfileParams.COULD_BE_LAMINATED]: LaminationType.ONE_SIDE,
    [ProfileParams.GLASS_WIDTH]: ['24'],
    [ProfileParams.MONTAGE_DEEP]: '60',
    [ProfileParams.PLASTIC_THICKNESS]: ['2.8'],
  },
  equpmentParams: {
    [EqupmentParam.FURNITURE]: FurnitureType.INTERNIKA,
    [EqupmentParam.HANDLES]: [
      HandleTypes.HOPPE,
      HandleTypes.BERTA_STANDART,
      HandleTypes.ANTIBAKTERIAL,
      HandleTypes.BERTA_ACUSTIC,
    ],
    [EqupmentParam.SHTAPICKS]: [ShtapickType.FACETED],
    [EqupmentParam.SEALANTS]: [SealantType.EPDM_GRAY],
    [EqupmentParam.GLASS]: [
      GlassTypes.STANDART,
      GlassTypes.TERMOS,
      GlassTypes.ENERGO,
    ],
  },
});
profiles[ProfileType.BERTA_SILVER_ECO_PREMIUM] = new ProfileData({
  title: 'Silver Eco Premium',
  desc:
    'Это инновационный, экологически чистый и безопасный для здоровья продукт с содержанием серебра по уникальной запатентованной технологии Silver Eco.',
  animationPath: 'silver-eco-premium',
  icon: '/static/img/profiles/silver-eco-premium.jpg',
  lineParams: {
    [LineParams.WARM]: 60,
    [LineParams.DESIGN]: 55,
    [LineParams.COMFORT]: 68,
  },
  profileParams: {
    [ProfileParams.AIR_CHAMBERS]: ['5'],
    [ProfileParams.ARM_THICKNESS]: ['1.5'],
    [ProfileParams.COULD_BE_LAMINATED]: LaminationType.BOTH,
    [ProfileParams.GLASS_WIDTH]: ['32'],
    [ProfileParams.MONTAGE_DEEP]: '72',
    [ProfileParams.PLASTIC_THICKNESS]: ['2.8'],
  },
  equpmentParams: {
    [EqupmentParam.FURNITURE]: FurnitureType.INTERNIKA,
    [EqupmentParam.HANDLES]: [
      HandleTypes.HOPPE,
      HandleTypes.BERTA_STANDART,
      HandleTypes.ANTIBAKTERIAL,
      HandleTypes.BERTA_ACUSTIC,
    ],
    [EqupmentParam.SHTAPICKS]: [ShtapickType.SQUARED],
    [EqupmentParam.SEALANTS]: [SealantType.EPDM_GRAY],
    [EqupmentParam.GLASS]: [
      GlassTypes.STANDART,
      GlassTypes.TERMOS,
      GlassTypes.ENERGO,
    ],
  },
});

profiles[ProfileType.BERTA_LITE] = new ProfileData({
  title: 'Berta Lite 58',
  desc:
    'Профильная система российского производства. Полностью соответсвует всем требованиям ГОСТ.',
  animationPath: 'berta-lite-58',
  icon: '/static/img/profiles/berta-lite-58.jpg',
  lineParams: {
    [LineParams.WARM]: 50,
    [LineParams.DESIGN]: 53,
    [LineParams.COMFORT]: 57,
  },
  profileParams: {
    [ProfileParams.AIR_CHAMBERS]: ['3'],
    [ProfileParams.ARM_THICKNESS]: ['1.2'],
    [ProfileParams.COULD_BE_LAMINATED]: LaminationType.ONE_SIDE,
    [ProfileParams.GLASS_WIDTH]: ['24'],
    [ProfileParams.MONTAGE_DEEP]: '58',
    [ProfileParams.PLASTIC_THICKNESS]: ['2.8'],
  },
  equpmentParams: {
    [EqupmentParam.FURNITURE]: FurnitureType.INTERNIKA,
    [EqupmentParam.HANDLES]: [HandleTypes.HOPPE],
    [EqupmentParam.SHTAPICKS]: [ShtapickType.ANTIK],
    [EqupmentParam.SEALANTS]: [SealantType.EPDM_BLACK],
    [EqupmentParam.GLASS]: [
      GlassTypes.STANDART,
      GlassTypes.TERMOS,
      GlassTypes.ENERGO,
    ],
  },
});

export default profiles;
