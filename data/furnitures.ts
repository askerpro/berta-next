export enum FurnitureType {
  SCHUCO_VARIOTECK,
  INTERNIKA,
  VORNE,
}

export const label = 'Фурнитура';

export const furnitures = [
  {
    title: 'Schuco Vario Tec',
    desc: 'Фурнитура от немецкого производителя с отличными свойствами.',
    icon: '/static/img/furnitures/schuco-variotec.jpg',
  },
  {
    title: 'Internika',
    desc: 'Европейская фурнитура. Производство: Словения. С плавным ходом и повышенной износостойкостью.',
    icon: '/static/img/furnitures/maco.jpg',
  },
  {
    title: 'Vorne',
    desc: 'Проиводство: Турция. Одна из лучших фурнитур в своем классе с тройной противокорозийной защитой.',
    icon: '/static/img/furnitures/maco.jpg',
  },
];

export default furnitures;
