export enum ShtapickType {
  ANTIK,
  SQUARED,
  CIRCLE,
  FACETED,
}
export const shtapicks = [
  {
    title: 'Антик',
  },
  {
    title: 'Квадратные',
  },
  {
    title: 'Круглые',
  },
  {
    title: 'Граненный со скосом',
  },
];

export default shtapicks;
