import { ProfileType } from './profiles';
import { GlassType } from './glasses';
import { HandleType } from './handles';
import { FurnitureType } from './furnitures';

export enum ProductType {
  PROMO,
  WINDOWS,
  DOORS_OUTSIDE,
  DOORS_INSIDE,
  BLOCKS,
  BALCONIES,
}

export const extras = {
  sill: {
    title: 'Подоконник',
    desc:
      'Внутренняя часть окна не может оставаться без специализированного обрамления – подоконника, который выполняет декорирующую (эстетическую) функцию температурно-компенсационного шва с крепежом и является в большинстве случаев надежной опорной площадкой для бытовой утвари и домашних цветов.',
    icon: '/static/img/extras/sill.jpg',
  },
  slope: {
    title: 'Откос',
    desc:
      'Откос - часть оконного проема, обрамляющая окно изнутри помещения. При демонтаже старых окон неминуемо снимается слой штукатурки, окружающий оконный проем, и лучший выход из ситуации – современные пластиковые откосы.',
    icon: '/static/img/extras/slope.jpg',
  },
  tide: {
    title: 'Отлив',
    desc:
      'Отлив – элемент оконной конструкции, отвечающий за свободный отвод воды с внешней стороны окна. Благодаря отливам нижняя часть окна надежно защищена от влаги – на подоконнике и откосах не появятся загадочные темные пятна и лужицы.',
    icon: '/static/img/extras/tide.jpg',
  },
  mosquitoNet: {
    title: 'Москитная сетка',
    desc:
      'Москитная сетка - это наиболее оптимальный способ борьбы с вредными насекомыми. Помимо этого, летом она защищает окно от тополиного пуха, а осенью от листопада, позволяя держать окна полностью открытыми.',
    icon: '/static/img/extras/mosquito-net.jpg',
  },
};

export const ProductTypes = {};
ProductTypes[ProductType.WINDOWS] = 'Пластиковые окна';
ProductTypes[ProductType.DOORS_OUTSIDE] = 'Входные двери';
ProductTypes[ProductType.DOORS_INSIDE] = 'Межкомнатные двери';
ProductTypes[ProductType.BLOCKS] = 'Балконные блоки';
ProductTypes[ProductType.BALCONIES] = 'Балконны';

export interface IProductSize {
  width: number;
  height: number;
}

export interface IRawProduct {
  price: string;
  size: IProductSize;
  img: string;
  type: ProductType;
  rating?: number;
  name?: string;
  oldprice?: string;
  isbestseller?: boolean;
  equipment: {
    profile: ProfileType;
    glass: GlassType;
    handle: HandleType;
    furniture: FurnitureType;
  };
  extras: {
    sill: boolean;
    slope: boolean;
    mosqutoNet: boolean;
    tide: boolean;
  };
}

const rawProducts: IRawProduct[] = [
  {
    price: 'от 8570р.',
    img: '/static/img/schematic/windows/simple/2-2.jpg',
    type: ProductType.WINDOWS,

    size: {
      width: 1200,
      height: 1300,
    },
    equipment: {
      profile: ProfileType.BERTA_LITE,
      glass: GlassType.STANDART,
      furniture: FurnitureType.INTERNIKA,
      handle: HandleType.HOPPE,
    },
    extras: {
      sill: false,
      slope: false,
      mosqutoNet: false,
      tide: false,
    },
  },
  {
    price: 'от 8570р.',
    img: '/static/img/schematic/windows/simple/2-2.jpg',
    type: ProductType.WINDOWS,
    isbestseller: true,
    size: {
      width: 1200,
      height: 1300,
    },
    equipment: {
      profile: ProfileType.BERTA_LITE,
      glass: GlassType.STANDART,
      furniture: FurnitureType.INTERNIKA,
      handle: HandleType.HOPPE,
    },
    extras: {
      sill: false,
      slope: false,
      mosqutoNet: false,
      tide: false,
    },
  },
  {
    price: 'от 8570р.',
    img: '/static/img/schematic/windows/simple/2-2.jpg',
    type: ProductType.WINDOWS,
    size: {
      width: 1200,
      height: 1300,
    },
    equipment: {
      profile: ProfileType.BERTA_LITE,
      glass: GlassType.STANDART,
      furniture: FurnitureType.INTERNIKA,
      handle: HandleType.HOPPE,
    },
    extras: {
      sill: false,
      slope: false,
      mosqutoNet: false,
      tide: false,
    },
  },
  {
    rating: 4.9,
    name: 'Золотая середина',
    oldprice: '11 542р.',
    isbestseller: true,
    price: '9 619р.',
    img: '/static/img/schematic/promo/2.jpg',
    type: ProductType.PROMO,
    size: {
      width: 1200,
      height: 1300,
    },
    equipment: {
      profile: ProfileType.BERTA_SILVER_ECO_CLASSIC,
      glass: GlassType.ENERGO,
      furniture: FurnitureType.INTERNIKA,
      handle: HandleType.HOPPE,
    },
    extras: {
      sill: false,
      slope: false,
      tide: false,
      mosqutoNet: false,
    },
  },
  {
    rating: 4.4,
    name: 'Базовый вариант',
    oldprice: '8 941р.',
    isbestseller: false,
    price: '7 451р.',
    img: '/static/img/schematic/promo/1.jpg',
    type: ProductType.PROMO,
    size: {
      width: 1200,
      height: 1300,
    },
    equipment: {
      profile: ProfileType.BERTA_LITE,
      glass: GlassType.STANDART,
      furniture: FurnitureType.VORNE,
      handle: HandleType.HOPPE,
    },
    extras: {
      sill: false,
      slope: false,
      tide: false,
      mosqutoNet: false,
    },
  },

  {
    rating: 4.6,
    name: 'Премиум класс',
    oldprice: '13 714р.',
    isbestseller: false,
    price: '11 429р.',
    img: '/static/img/schematic/promo/3.jpg',
    type: ProductType.PROMO,
    size: {
      width: 1200,
      height: 1300,
    },
    equipment: {
      profile: ProfileType.BERTA_SILVER_ECO_PREMIUM,
      glass: GlassType.ENERGO,
      furniture: FurnitureType.INTERNIKA,
      handle: HandleType.HOPPE,
    },
    extras: {
      sill: false,
      slope: false,
      tide: false,
      mosqutoNet: false,
    },
  },
];

export default rawProducts;
