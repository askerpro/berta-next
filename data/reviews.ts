export interface IReview {
  url: string;
  region: string;
  location: string;
  date: string;
  preview: string;
}
