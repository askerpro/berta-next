import { LineParams } from 'components/LineParam';

export enum HandleType {
  HOPPE,
  BERTA_STANDART,
  HANDLE_WITH_LOCK,
  BERTA_ACUSTIC,
  ANTIBAKTERIAL,
}

export interface IRawHandle {
  title: string;
  desc: string;
  icon: string;
  lineParams: {
    [LineParams.WARM]: number;
    [LineParams.DESIGN]: number;
    [LineParams.COMFORT]: number;
  };
}

export const label = 'Ручка';

export const handles: IRawHandle[] = [
  {
    title: 'Hoppe',
    desc: 'Европейская ручка с классическим, элегантным внешним видом.',
    icon: '/static/img/handles/hoppe.jpg',
    lineParams: {
      [LineParams.WARM]: 0,
      [LineParams.DESIGN]: 0,
      [LineParams.COMFORT]: 0,
    },
  },
  {
    title: 'Berta Standart',
    desc: 'Ручка с логотипом "Берта". Производится в Германии.',
    icon: '/static/img/handles/standart.jpg',
    lineParams: {
      [LineParams.WARM]: 0,
      [LineParams.DESIGN]: 0,
      [LineParams.COMFORT]: 10,
    },
  },
  {
    title: 'Ручка с замком',
    desc: 'Ручка Берта с замком.',
    icon: '/static/img/handles/protected.jpg',
    lineParams: {
      [LineParams.WARM]: 0,
      [LineParams.DESIGN]: 0,
      [LineParams.COMFORT]: 14,
    },
  },
  {
    title: 'Berta acustic',
    desc: 'Специальная ручка с акустическим эффектом.',
    icon: '/static/img/handles/hoppe.jpg',
    lineParams: {
      [LineParams.WARM]: 0,
      [LineParams.DESIGN]: 0,
      [LineParams.COMFORT]: 20,
    },
  },
  {
    title: 'Berta Antibakterial',
    desc: 'Элегентная ручка с антибактериальным покрытием.',
    icon: '/static/img/handles/hoppe.jpg',
    lineParams: {
      [LineParams.WARM]: 0,
      [LineParams.DESIGN]: 0,
      [LineParams.COMFORT]: 20,
    },
  },
];

export default handles;
