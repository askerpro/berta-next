# SAMPLE REACT PROJECT

## How to use

### DEV

```bash
  yarn dev
```

### PRODUCTION

```bash
  yarn build && yarn start
```
