import nodemailer from 'nodemailer';
import { Express } from 'express';
import config from '../config';

const transporter = nodemailer.createTransport({
  host: config.mail.smtpHost,
  port: config.mail.smtpPort,
  secure: false,
  tls: {
    rejectUnauthorized: false,
  },
});

const send = async (req, res) => {
  console.log(req.body);
  const mailOptions = {
    from: '"Сайт Окна Берта" <calc@okna-berta.ru>', // sender address
    to: config.mail.to, // list of receivers
    subject: req.body.subject, // Subject line
    html: req.body.message, // html body
  };

  const info = await transporter.sendMail(mailOptions);
  console.log(info);
  const err = false;
  if (err) {
    res.send({ error: err });
  } else {
    console.log('Успешно отправлено');
    res.send({});
  }
};

export default (app: Express) => {
  app.post('/api/sendmail', send);
};
