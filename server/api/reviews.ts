import { Express } from 'express';
const axios = require('axios');

const get = async (req, res) => {
  axios.get('https://ok-crm.ru/api/site/reviews.php')
  .then(response => {
    res.send(response.data)
  })
  .catch(error => {
    console.log(error);
  });
  ;
};

export default (app: Express) => {
  app.get('/api/reviews', get);
};