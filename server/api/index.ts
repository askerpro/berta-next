import { Express } from 'express';
import Sendmail from './sendmail';
import Reviews from './reviews';

export default (app: Express) => {
  Sendmail(app);
  Reviews(app);
};
