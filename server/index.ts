import express from 'express';
import next from 'next';
import MailDev from 'maildev';
import bodyParser from 'body-parser';
import config, { IConfig } from './config';
import ApiRoutesInit from './api';

console.log(process.env.NODE_ENV);
const mode = process.env.NODE_ENV;

const app = next({ dev: process.env.NODE_ENV === 'development' });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  server.use(bodyParser.json());

  if (mode === 'development') {
    const maildev = new MailDev({
      smtp: config.mail.smtpPort,
      ip: config.mail.smtpHost,
    });
    maildev.listen();
  }
  ApiRoutesInit(server);
  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(config.network.port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${config.network.port}`);
  });
});
