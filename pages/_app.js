import App from 'next/app';
import React from 'react';
import withReduxStore from 'lib/with-redux-store';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import { StylesProvider } from '@material-ui/styles';
import Layout from 'layout';
import checkUserAgent from 'lib/device-detect';
import { Light } from 'layout/theme';
import { YMinit } from 'lib/ym';

import 'static/crutch.css';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};
    const userAgent = ctx.req
      ? ctx.req.headers['user-agent']
      : window.navigator.userAgent;
    checkUserAgent(userAgent);

    return { pageProps };
  }
  componentDidMount() {
    // Remove the server-side injected CSS.
    YMinit();
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;

    return (
      <Provider store={reduxStore}>
        <StylesProvider injectFirst>
          <Light>
            <Layout>
              <CssBaseline />
              <Component {...pageProps} />
            </Layout>
          </Light>
        </StylesProvider>
      </Provider>
    );
  }
}

export default withReduxStore(MyApp);
